module.exports = {
  moduleFileExtensions: ['js', 'json', 'ts'],
  rootDir: 'src',
  testRegex: '.*\\.spec-integration\\.ts$',
  transform: {
    '^.+\\.(t|j)s$': 'ts-jest'
  },
  collectCoverage: true,
  collectCoverageFrom: ['**/*.{js,ts}', '!**/node_modules/**', '!**/*.spec.{js,ts}', '!**/*.spec-integration.{js,ts}'],
  coverageDirectory: '../coverage/integration',
  coverageReporters: ['lcov', 'cobertura', 'text'],
  testEnvironment: '<rootDir>/tests/memgraph-environment-setup.ts',
  setupFiles: ['<rootDir>/tests/setTestEnvVars.js', '<rootDir>/tests/setup-jsonld-document-loader.ts'],
  globalTeardown: '<rootDir>/tests/memgraph-environment-teardown.ts',
  transformIgnorePatterns: ['/node_modules/*', '<rootDir>/tests/setTestEnvVars.js'],
  maxWorkers: '50%',
  testTimeout: 30000
}
