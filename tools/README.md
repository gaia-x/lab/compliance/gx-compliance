This contains a small jupyter notebook to sign and emit the first VC-JWT and VP containing VC-JWT

To use, put private key in PEM in `rsa_private_key.pem` and a JWK of the public key in `rsa_public_key.json`

Please also update `didValue`,`assertionMethodValue` and `domain` according to your DID