from datetime import datetime


def issuer(domain: str, did_value: str):
    return {
        "@context": [
            "https://www.w3.org/ns/credentials/v2",
            "https://w3id.org/gaia-x/development#"
        ],
        "type": ["VerifiableCredential", "Issuer"],
        "validFrom": datetime.now().isoformat(),
        "id": domain + "issuer.json",
        "issuer": did_value,
        "credentialSubject": {
            "id": domain + "/issuer.json#cs",
            "gaiaxTermsAndConditions": "4bd7554097444c960292b4726c2efa1373485e8a5565d94d41195214c5e0ceb3"
        }
    }
