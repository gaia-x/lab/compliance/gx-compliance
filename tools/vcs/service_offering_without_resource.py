from datetime import datetime


def service_offering_without_resource(domain: str, did_value: str):
    return {
        "@context": [
            "https://www.w3.org/ns/credentials/v2",
            "https://w3id.org/gaia-x/development#",
            {"xsd": "http://www.w3.org/2001/XMLSchema#"}
        ],
        "id": domain + "so.json",
        "type": [
            "VerifiableCredential",
            "ServiceOffering"
        ],
        "issuer": did_value,
        "validFrom": datetime.now().isoformat(),
        "name": "BAKEUP.IO ServiceOffering",
        "credentialSubject": {
            "id": domain + "so.json#cs",
            "providedBy": domain + "participant.json#cs",
            "serviceOfferingTermsAndConditions": {
                "type": "TermsAndConditions",
                "url": {
                    "type": "xsd:anyURI",
                    "@value": "https://domain.com/tsandcs",
                },
                "hash": "sha256-ssasaSAsa"
            },
            "servicePolicy": "allow",
            "dataAccountExport": {
                "type": "DataAccountExport",
                "requestType": "API",
                "accessType": "digital",
                "formatType": "json"
            }
        }
    }
