from datetime import datetime


def participant(domain: str, did_value: str):
    return {
        "@context": [
            "https://www.w3.org/ns/credentials/v2",
            "https://w3id.org/gaia-x/development#"
        ],
        "id": domain + "participant.json",
        "type": [
            "VerifiableCredential",
            "LegalPerson"
        ],
        "issuer": did_value,
        "validFrom": datetime.now().isoformat(),
        "name": "BAKEUP.IO LegalPerson",
        "credentialSubject": {
            "name": "BAKEUP.IO",
            "headquartersAddress": {
                "type": "gx:Address",
                "countryCode": "FR",
                "countryName": "France"
            },
            "gx:registrationNumber": {
                "id": domain + "lrn.json#cs"
            },
            "legalAddress": {
                "type": "gx:Address",
                "countryCode": "FR",
                "countryName": "France"
            },
            "id": domain + "participant.json#cs"
        }
    }
