import { NestFactory } from '@nestjs/core'
import { NestExpressApplication } from '@nestjs/platform-express'

import { AppModule } from './app.module'
import { setupSwagger } from './common/swagger'
import { HttpExceptionFilter } from './credential-offer/filter/http-exception.filter'

export const appPath = !!process.env['APP_PATH'] ? process.env['APP_PATH'] : ''

async function bootstrap() {
  const app: NestExpressApplication = await NestFactory.create(AppModule)
  app.setGlobalPrefix(`${appPath}/`)
  app.useGlobalFilters(new HttpExceptionFilter())
  app.enableCors()
  app.useBodyParser('json', { limit: '10mb' })
  app.useBodyParser('text', { limit: '10mb' })
  app.useBodyParser('raw', { limit: '10mb' })
  setupSwagger(app)

  await app.listen(process.env.PORT || 3000)
}

bootstrap()
