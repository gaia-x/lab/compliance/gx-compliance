import { BadRequestException, ConflictException, HttpException, INestApplication } from '@nestjs/common'
import { Test } from '@nestjs/testing'

import request from 'supertest'

import { VC_JWT_MIME_TYPE, VP_JWT_MIME_TYPE } from '../../common/constants'
import { InvalidVerifiablePresentationException } from '../../common/exception/invalid-verifiable-presentation.exception'
import { CredentialOfferModule } from '../credential-offer.module'
import { CredentialOfferService } from '../service/credential-offer.service'

import mocked = jest.mocked

describe('CredentialOfferController', () => {
  const credentialOfferServiceMock: CredentialOfferService = {
    checkAndIssueComplianceCredential: jest.fn()
  } as unknown as CredentialOfferService

  let app: INestApplication

  beforeAll(async () => {
    const moduleRef = await Test.createTestingModule({
      imports: [CredentialOfferModule]
    })
      .overrideProvider(CredentialOfferService)
      .useValue(credentialOfferServiceMock)
      .compile()

    app = moduleRef.createNestApplication({ bodyParser: false })
    await app.init()
  })

  afterAll(async () => {
    await app.close()
  })

  describe.each(['standard-compliance', 'label-level-1', 'label-level-2', 'label-level-3'])('POST /api/credential-offers', endpoint => {
    it(`/${endpoint} | should return a valid basic conformity`, async () => {
      mocked(credentialOfferServiceMock.checkAndIssueComplianceCredential).mockResolvedValue('Compliance VP-JWT')

      const response: request.Response = await request(app.getHttpServer())
        .post(`/api/credential-offers/${endpoint}`)
        .set('Accept', VP_JWT_MIME_TYPE)
        .send('Input VP-JWT')
        .responseType('text')
        .expect(201)

      expect(response.body.toString()).toEqual('Compliance VP-JWT')
    })

    it(`/${endpoint} | should return an invalid verifiable presentation error`, async () => {
      mocked(credentialOfferServiceMock.checkAndIssueComplianceCredential).mockRejectedValue(
        new InvalidVerifiablePresentationException('Test Exception', ['First error', 'Second error', 'Third error'])
      )

      await request(app.getHttpServer())
        .post(`/api/credential-offers/${endpoint}`)
        .set('Accept', VC_JWT_MIME_TYPE)
        .send('Input VP-JWT')
        .expect(400)
        .expect({
          message: 'Test Exception',
          error: 'Invalid verifiable presentation',
          statusCode: 400,
          errors: ['First error', 'Second error', 'Third error']
        })
    })

    it(`/${endpoint} | should return a generic conflict error`, async () => {
      mocked(credentialOfferServiceMock.checkAndIssueComplianceCredential).mockRejectedValue(
        new ConflictException('Test Exception', `Test description`)
      )

      await request(app.getHttpServer())
        .post(`/api/credential-offers/${endpoint}`)
        .set('Accept', VP_JWT_MIME_TYPE)
        .send('Input VP-JWT')
        .expect(409)
        .expect({
          message: 'Test Exception',
          error: 'Test description',
          statusCode: 409,
          errors: ['Test description']
        })
    })

    it(`/${endpoint} | should return a bad request error`, async () => {
      mocked(credentialOfferServiceMock.checkAndIssueComplianceCredential).mockRejectedValue(
        new BadRequestException('Invalid DID', 'An error occurred with something related to the DID')
      )

      await request(app.getHttpServer())
        .post(`/api/credential-offers/${endpoint}`)
        .set('Accept', VP_JWT_MIME_TYPE)
        .send('Input VP-JWT')
        .expect(400)
        .expect({
          message: 'Invalid DID',
          error: 'An error occurred with something related to the DID',
          statusCode: 400,
          errors: ['An error occurred with something related to the DID']
        })
    })

    it(`/${endpoint} | should return a generic string response error`, async () => {
      mocked(credentialOfferServiceMock.checkAndIssueComplianceCredential).mockRejectedValue(
        new HttpException("I'm a special teapot, not like the other HTTP 418 ones!", 418)
      )

      await request(app.getHttpServer())
        .post(`/api/credential-offers/${endpoint}`)
        .set('Accept', VP_JWT_MIME_TYPE)
        .send('Input VP-JWT')
        .expect(418)
        .expect({
          message: "I'm a special teapot, not like the other HTTP 418 ones!",
          error: 'HttpException',
          statusCode: 418,
          errors: ["I'm a special teapot, not like the other HTTP 418 ones!"]
        })
    })

    it(`/${endpoint} | should return a generic error`, async () => {
      mocked(credentialOfferServiceMock.checkAndIssueComplianceCredential).mockRejectedValue(new Error('Test Exception'))

      await request(app.getHttpServer())
        .post(`/api/credential-offers/${endpoint}`)
        .set('Accept', VP_JWT_MIME_TYPE)
        .send('Input VP-JWT')
        .expect(500)
        .expect({
          message: 'Internal Server Error',
          statusCode: 500,
          errors: ['Test Exception']
        })
    })
  })
})
