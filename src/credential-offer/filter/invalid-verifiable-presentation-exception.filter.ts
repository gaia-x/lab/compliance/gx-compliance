import { ArgumentsHost, Catch, ExceptionFilter, HttpStatus } from '@nestjs/common'

import { Response } from 'express'

import { InvalidVerifiablePresentationException } from '../../common/exception/invalid-verifiable-presentation.exception'

@Catch(InvalidVerifiablePresentationException)
export class InvalidVerifiablePresentationExceptionFilter implements ExceptionFilter<InvalidVerifiablePresentationException> {
  catch(exception: InvalidVerifiablePresentationException, host: ArgumentsHost) {
    const ctx = host.switchToHttp()
    const response = ctx.getResponse<Response>()
    response.header('Content-Type', 'application/json')
    response.status(HttpStatus.BAD_REQUEST).json(exception.getResponse())
  }
}
