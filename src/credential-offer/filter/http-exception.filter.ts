import { ArgumentsHost, Catch, ExceptionFilter, HttpException } from '@nestjs/common'

import { Response } from 'express'

@Catch(HttpException)
export class HttpExceptionFilter implements ExceptionFilter<HttpException> {
  catch(exception: HttpException, host: ArgumentsHost) {
    const ctx = host.switchToHttp()
    const response = ctx.getResponse<Response>()
    response.header('Content-Type', 'application/json')
    if (typeof exception.getResponse() === 'string' || exception.getResponse() instanceof String) {
      response.status(exception.getStatus()).json({
        message: exception.getResponse(),
        error: exception.name,
        statusCode: exception.getStatus(),
        errors: [exception.getResponse()]
      })
    } else {
      const errorResponse = exception.getResponse() as object

      response.status(exception.getStatus()).json({
        ...errorResponse,
        errors: [errorResponse['error']]
      })
    }
  }
}
