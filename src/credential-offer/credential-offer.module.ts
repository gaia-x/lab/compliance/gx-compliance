import { Module } from '@nestjs/common'

import { makeCounterProvider, makeSummaryProvider } from '@willsoto/nestjs-prometheus'

import { CommonModule } from '../common/common.module'
import { JwtModule } from '../jwt/jwt.module'
import { VpContentValidationModule } from '../vp-validation/vp-content-validation.module'
import { CredentialOfferController } from './controller/credential-offer.controller'
import { CredentialOfferService } from './service/credential-offer.service'

@Module({
  imports: [CommonModule, JwtModule, VpContentValidationModule],
  providers: [
    CredentialOfferService,
    makeCounterProvider({
      name: 'verified_presentations_total',
      help: 'Number of presentations verified by the engine since startup'
    }),
    makeCounterProvider({
      name: 'submitted_presentations_total',
      help: 'Number of presentations submitted to the engine since startup'
    }),
    makeSummaryProvider({
      name: 'verifiable_presentation_process_time',
      help: 'Distribution of the Verifiable Presentations verification time in seconds',
      labelNames: ['result']
    })
  ],
  controllers: [CredentialOfferController]
})
export class CredentialOfferModule {}
