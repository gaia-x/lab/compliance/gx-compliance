import { Injectable, Logger } from '@nestjs/common'

import { InjectMetric } from '@willsoto/nestjs-prometheus'
import { Counter, Summary } from 'prom-client'

import { ConformityLevelEnum } from '../../common/enum/conformity-level.enum'
import { InvalidCertificateException } from '../../common/exception/invalid-certificate.exception'
import { InvalidDidException } from '../../common/exception/invalid-did.exception'
import { InvalidVerifiablePresentationException } from '../../common/exception/invalid-verifiable-presentation.exception'
import { DIDService } from '../../common/services/did.service'
import { EvidenceService } from '../../common/services/evidence.service'
import { JwtSignatureValidationService } from '../../jwt/service/jwt-signature-validation.service'
import { JwtSignatureService } from '../../jwt/service/jwt-signature.service'
import { JwtToVpService } from '../../jwt/service/jwt-to-vp.service'
import { RegistryService } from '../../vp-validation/service/registry.service'
import { VerifiablePresentationValidationService } from '../../vp-validation/service/verifiable-presentation-validation.service'

@Injectable()
export class CredentialOfferService {
  private logger = new Logger(CredentialOfferService.name)

  constructor(
    private readonly didService: DIDService,
    private readonly evidenceService: EvidenceService,
    private readonly jwtSignatureService: JwtSignatureService,
    private readonly jwtSignatureValidationService: JwtSignatureValidationService,
    private readonly jwtToVPService: JwtToVpService,
    private readonly registryService: RegistryService,
    private readonly verifiablePresentationValidationService: VerifiablePresentationValidationService,
    @InjectMetric('submitted_presentations_total') public submittedCounter: Counter<string>,
    @InjectMetric('verified_presentations_total') public verifiedCounter: Counter<string>,
    @InjectMetric('verifiable_presentation_process_time') public processTimeSummary: Summary
  ) {}

  public async checkAndIssueComplianceCredential(
    jwtVerifiablePresentation: string,
    outputVCID: string,
    conformityLevel: ConformityLevelEnum
  ): Promise<string> {
    this.logger.debug(`Received a request for outputVC ${outputVCID}`)
    this.submittedCounter.inc()
    const end = this.processTimeSummary.startTimer()
    const { verifiableCredentials, key, JWK } = await this.jwtSignatureValidationService.validateJWTAndConvertToVCs(jwtVerifiablePresentation)
    this.logger.debug(`outputVC ${outputVCID} conversion done`)
    if (!JWK.x5u && !JWK.x5c) {
      end({ result: 'Non compliant DID' })
      throw new InvalidDidException('DID does not contain x5u nor x5c')
    }
    let certificateAndPublicKeyMatch: { conforms: boolean; cert: string }
    if (!!JWK.x5u) {
      certificateAndPublicKeyMatch = await this.didService.checkx5uMatchesPublicKey(key, JWK.x5u)
    } else if (!!JWK.x5c) {
      certificateAndPublicKeyMatch = await this.didService.checkx5cMatchesPublicKey(key, JWK.x5c)
    }
    if (!certificateAndPublicKeyMatch.conforms) {
      throw new InvalidDidException('The JWK does not match the certificate in x5u/x5c')
    }

    await this.validateCertificateIsTrusted(certificateAndPublicKeyMatch.cert)
    this.logger.debug(`output ${outputVCID} certificate validation done`)

    const verifiablePresentation = this.jwtToVPService.JWTVcsToVP(verifiableCredentials)
    this.logger.debug(`output ${outputVCID} rules validation start`)
    const results = await this.verifiablePresentationValidationService.validate(verifiablePresentation, conformityLevel)
    this.logger.debug(`output ${outputVCID} rules validation done`)
    if (results.conforms) {
      this.verifiedCounter.inc()
      end({ result: 'Compliant VP' })
      const complianceCredentialDTO = await this.evidenceService.createComplianceCredential(
        verifiablePresentation,
        outputVCID,
        results,
        conformityLevel
      )
      return await this.jwtSignatureService.signVerifiableCredential(complianceCredentialDTO)
    } else {
      end({ result: 'Non compliant VP' })

      throw new InvalidVerifiablePresentationException('Unable to validate compliance', results.results)
    }
  }

  private async validateCertificateIsTrusted(cert: string) {
    if (!(await this.registryService.isValidCertificateChain(cert))) {
      throw new InvalidCertificateException('The keypair used to sign the VerifiablePresentation is not trusted in the Gaia-X Registry')
    }
  }
}
