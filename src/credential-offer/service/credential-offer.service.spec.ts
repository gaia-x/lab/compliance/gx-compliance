import { JsonWebKey } from 'did-resolver/src/resolver'
import { GenerateKeyPairResult, generateKeyPair } from 'jose'
import { Counter, Summary } from 'prom-client'

import { ComplianceCredentialDto, CredentialSubjectDto, VerifiableCredentialDto, VerifiablePresentationDto } from '../../common/dto'
import { ConformityLevelEnum } from '../../common/enum/conformity-level.enum'
import { InvalidDidException } from '../../common/exception/invalid-did.exception'
import { DIDService } from '../../common/services/did.service'
import { EvidenceService } from '../../common/services/evidence.service'
import { validBasicConformityJWTExample } from '../../examples/jwt-self-descriptions.example'
import { JwtSignatureValidationService } from '../../jwt/service/jwt-signature-validation.service'
import { JwtSignatureService } from '../../jwt/service/jwt-signature.service'
import { JwtToVpService } from '../../jwt/service/jwt-to-vp.service'
import { RegistryService } from '../../vp-validation/service/registry.service'
import { VerifiablePresentationValidationService } from '../../vp-validation/service/verifiable-presentation-validation.service'
import { CredentialOfferService } from './credential-offer.service'

describe('CredentialOfferService', () => {
  const validVpJwt = validBasicConformityJWTExample

  const didServiceMock: DIDService = {
    checkx5uMatchesPublicKey: jest.fn(),
    checkx5cMatchesPublicKey: jest.fn()
  } as unknown as DIDService

  const jwtSignatureValidationServiceMock: JwtSignatureValidationService = {
    validateJWTAndConvertToVCs: jest.fn()
  } as unknown as JwtSignatureValidationService

  const jwtToVPServiceMock: JwtToVpService = {
    JWTVcsToVP: jest.fn()
  } as unknown as JwtToVpService

  const verifiablePresentationValidationServiceMock: VerifiablePresentationValidationService = {
    validate: jest.fn()
  } as unknown as VerifiablePresentationValidationService

  const evidenceServiceMock: EvidenceService = {
    createComplianceCredential: jest.fn()
  } as unknown as EvidenceService

  const jwtSignatureServiceMock: JwtSignatureService = {
    signVerifiableCredential: jest.fn()
  } as unknown as JwtSignatureService

  const registryServiceMock: RegistryService = {
    isValidCertificateChain: jest.fn()
  } as unknown as RegistryService

  const counterMock: Counter = {
    inc: jest.fn()
  } as unknown as Counter
  const summaryMock: Summary = {
    startTimer: () => {
      return jest.fn()
    }
  } as unknown as Summary

  const verifiableCredentialsMock: any[] = [{ id: 'test-credential-1' }, { id: 'test-credential-2' }]

  let service: CredentialOfferService
  let keyPair: GenerateKeyPairResult
  let verifiablePresentationMock: VerifiablePresentationDto<VerifiableCredentialDto<CredentialSubjectDto>>
  let complianceCredentialMock: VerifiableCredentialDto<ComplianceCredentialDto>
  beforeAll(async () => {
    service = new CredentialOfferService(
      didServiceMock,
      evidenceServiceMock,
      jwtSignatureServiceMock,
      jwtSignatureValidationServiceMock,
      jwtToVPServiceMock,
      registryServiceMock,
      verifiablePresentationValidationServiceMock,
      counterMock,
      counterMock,
      summaryMock
    )

    keyPair = await generateKeyPair('PS256')
  })
  beforeEach(async () => {
    verifiablePresentationMock = {
      '@context': undefined,
      '@type': ['VerifiablePresentation'],
      verifiableCredential: []
    }
    jest.mocked(jwtToVPServiceMock.JWTVcsToVP).mockReturnValue(verifiablePresentationMock)
    jest.mocked(jwtSignatureValidationServiceMock.validateJWTAndConvertToVCs).mockResolvedValue({
      verifiableCredentials: verifiableCredentialsMock,
      key: keyPair.publicKey,
      JWK: {
        x5u: 'certificateUrl'
      } as unknown as JsonWebKey
    })

    complianceCredentialMock = {
      '@context': undefined,
      credentialSubject: undefined,
      issuer: '',
      type: 'ComplianceCredential',
      validFrom: ''
    }
    jest.mocked(evidenceServiceMock.createComplianceCredential).mockResolvedValue(complianceCredentialMock)

    jest.mocked(jwtSignatureServiceMock.signVerifiableCredential).mockResolvedValue('jwt-compliance-credential')

    jest.mocked(verifiablePresentationValidationServiceMock.validate).mockResolvedValue({
      conforms: true,
      results: [],
      validatedCriteria: ['P1.1.1', 'P3.1.12']
    })
    jest.mocked(registryServiceMock.isValidCertificateChain).mockResolvedValue(true)
    jest.mocked(didServiceMock.checkx5cMatchesPublicKey).mockResolvedValue({ conforms: true, cert: 'cert' })
    jest.mocked(didServiceMock.checkx5uMatchesPublicKey).mockResolvedValue({ conforms: true, cert: 'cert' })
  })

  afterEach(() => {
    jest.resetAllMocks()
  })

  it('should throw a DID exception when the DID contains neither x5c nor x5u', async () => {
    // Given the DID does not contain x5c nor x5u
    jest.mocked(jwtSignatureValidationServiceMock.validateJWTAndConvertToVCs).mockResolvedValue({
      verifiableCredentials: verifiableCredentialsMock,
      key: keyPair.publicKey,
      JWK: {} as unknown as JsonWebKey
    })

    // When I check the VC for compliance
    // Then it's rejected and I get the message that x5c and x5u are missing
    await expect(
      service.checkAndIssueComplianceCredential(validVpJwt, 'https://example.org/verifiable-credential/123', ConformityLevelEnum.STANDARD_COMPLIANCE)
    ).rejects.toThrow(new InvalidDidException('DID does not contain x5u nor x5c'))
  })

  it('should check VC-JWT and issue compliance credential when the did contains a x5u', async () => {
    const result: string = await service.checkAndIssueComplianceCredential(
      validVpJwt,
      'https://example.org/verifiable-credential/123',
      ConformityLevelEnum.STANDARD_COMPLIANCE
    )

    expect(result).toEqual('jwt-compliance-credential')
    expect(evidenceServiceMock.createComplianceCredential).toHaveBeenCalledWith(
      verifiablePresentationMock,
      'https://example.org/verifiable-credential/123',
      { conforms: true, results: [], validatedCriteria: ['P1.1.1', 'P3.1.12'] },
      ConformityLevelEnum.STANDARD_COMPLIANCE
    )
    expect(jwtSignatureServiceMock.signVerifiableCredential).toHaveBeenCalledWith(complianceCredentialMock)
    expect(didServiceMock.checkx5uMatchesPublicKey).toHaveBeenCalledWith(keyPair.publicKey, 'certificateUrl')
    expect(jwtSignatureValidationServiceMock.validateJWTAndConvertToVCs).toHaveBeenCalledWith(validVpJwt)
    expect(verifiablePresentationValidationServiceMock.validate).toHaveBeenCalledWith(
      verifiablePresentationMock,
      ConformityLevelEnum.STANDARD_COMPLIANCE
    )
  })

  it('should check VC-JWT and issue compliance credential when the did contains a x5c', async () => {
    // Given the DID contains x5c
    jest.mocked(jwtSignatureValidationServiceMock.validateJWTAndConvertToVCs).mockResolvedValue({
      verifiableCredentials: verifiableCredentialsMock,
      key: keyPair.publicKey,
      JWK: {
        x5c: ['leaf', 'root']
      } as unknown as JsonWebKey
    })
    // When I check the VC for compliance
    const result: string = await service.checkAndIssueComplianceCredential(
      validVpJwt,
      'https://example.org/verifiable-credential/123',
      ConformityLevelEnum.STANDARD_COMPLIANCE
    )
    // Then it's validated and I get a compliance credential
    expect(result).toEqual('jwt-compliance-credential')
    expect(evidenceServiceMock.createComplianceCredential).toHaveBeenCalledWith(
      verifiablePresentationMock,
      'https://example.org/verifiable-credential/123',
      { conforms: true, results: [], validatedCriteria: ['P1.1.1', 'P3.1.12'] },
      ConformityLevelEnum.STANDARD_COMPLIANCE
    )
    expect(jwtSignatureServiceMock.signVerifiableCredential).toHaveBeenCalledWith(complianceCredentialMock)
    expect(didServiceMock.checkx5cMatchesPublicKey).toHaveBeenCalledWith(keyPair.publicKey, ['leaf', 'root'])
    expect(jwtSignatureValidationServiceMock.validateJWTAndConvertToVCs).toHaveBeenCalledWith(validVpJwt)
    expect(verifiablePresentationValidationServiceMock.validate).toHaveBeenCalledWith(
      verifiablePresentationMock,
      ConformityLevelEnum.STANDARD_COMPLIANCE
    )
  })

  it('should throw an exception if the verifiable presentation is not compliant', () => {
    jest.mocked(verifiablePresentationValidationServiceMock.validate).mockResolvedValue({
      conforms: false,
      results: [],
      validatedCriteria: []
    })

    expect(
      service.checkAndIssueComplianceCredential(validVpJwt, 'https://example.org/verifiable-credential/123', ConformityLevelEnum.STANDARD_COMPLIANCE)
    ).rejects.toThrow('Unable to validate compliance')

    expect(jwtSignatureServiceMock.signVerifiableCredential).not.toHaveBeenCalled()
  })

  it('should throw an exception if the certificate is not matching the JWK', () => {
    jest.mocked(didServiceMock.checkx5uMatchesPublicKey).mockResolvedValue({ conforms: false, cert: 'cert' })
    expect(
      service.checkAndIssueComplianceCredential(validVpJwt, 'https://example.org/verifiable-credential/123', ConformityLevelEnum.STANDARD_COMPLIANCE)
    ).rejects.toThrow('Invalid DID')
  })
  it('should throw an exception if the certificate is not trusted by the registry', () => {
    jest.mocked(registryServiceMock.isValidCertificateChain).mockResolvedValue(false)
    expect(
      service.checkAndIssueComplianceCredential(validVpJwt, 'https://example.org/verifiable-credential/123', ConformityLevelEnum.STANDARD_COMPLIANCE)
    ).rejects.toThrow('Invalid Certificate')
  })
})
