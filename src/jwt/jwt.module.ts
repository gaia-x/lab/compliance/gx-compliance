import { Module } from '@nestjs/common'

import { IdentityModule } from '../identity/identity.module'
import { JwtSignatureValidationService } from './service/jwt-signature-validation.service'
import { JwtSignatureService } from './service/jwt-signature.service'
import { JwtToVpService } from './service/jwt-to-vp.service'

@Module({
  imports: [IdentityModule],
  providers: [JwtSignatureService, JwtSignatureValidationService, JwtToVpService],
  exports: [JwtSignatureService, JwtSignatureValidationService, JwtToVpService]
})
export class JwtModule {}
