import { Inject, Injectable } from '@nestjs/common'

import { KeyLike, SignJWT } from 'jose'

import { CTY_VALUE, TYP_VALUE } from '../../common/constants'

@Injectable()
export class JwtSignatureService {
  constructor(
    @Inject('PRIVATE_KEY') private privateKey: KeyLike,
    @Inject('PRIVATE_KEY_ALGORITHM') private privateKeyAlgorithm: string,
    @Inject('DidWeb') private readonly didWeb: string,
    @Inject('x509VerificationMethodIdentifier') private readonly x509VerificationMethodIdentifier: string
  ) {}

  async signVerifiableCredential(verifiableCredential: any): Promise<string> {
    const issuer = this.didWeb

    const instance = new SignJWT(verifiableCredential).setProtectedHeader({
      alg: this.privateKeyAlgorithm,
      iss: issuer,
      kid: this.x509VerificationMethodIdentifier,
      iat: new Date(verifiableCredential.validFrom).getTime(),
      exp: new Date(verifiableCredential.validUntil).getTime(),
      cty: CTY_VALUE,
      typ: TYP_VALUE
    })
    return instance.sign(this.privateKey)
  }
}
