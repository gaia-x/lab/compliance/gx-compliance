import { ConfigModule, ConfigService } from '@nestjs/config'
import { Test, TestingModule } from '@nestjs/testing'

import { decodeProtectedHeader, importSPKI, jwtVerify } from 'jose'

import { CommonModule } from '../../common/common.module'
import { PrivateKeyAlgorithmProvider } from '../../common/providers/private-key-algorithm.provider'
import { PrivateKeyProvider } from '../../common/providers/private-key.provider'
import { IdentityModule } from '../../identity/identity.module'
import { ConfigServiceMock } from '../../tests/config.service.mock'
import * as complianceVC from '../../tests/fixtures/v2/compliance_vc.json'
import { JwtSignatureService } from './jwt-signature.service'

describe('JwtSignatureService', () => {
  let jwtSignatureService: JwtSignatureService

  beforeAll(async () => {
    const configServiceMock = new ConfigServiceMock({
      BASE_URL: 'https://localhost',
      PRIVATE_KEY: process.env.PRIVATE_KEY
    })
    const moduleFixture: TestingModule = await Test.createTestingModule({
      providers: [JwtSignatureService, PrivateKeyProvider.create(), PrivateKeyAlgorithmProvider.create(), ConfigService],
      imports: [CommonModule, ConfigModule, IdentityModule]
    })
      .overrideProvider(ConfigService)
      .useValue(configServiceMock)
      .compile()
    jwtSignatureService = moduleFixture.get<JwtSignatureService>(JwtSignatureService)
  })

  it('should be defined', () => {
    expect(jwtSignatureService).toBeDefined()
  })

  it('should return a JWT object from a VerifiableCredential', async () => {
    const jwt = await jwtSignatureService.signVerifiableCredential({})
    expect(jwt).toBeDefined()
    expect(jwt).not.toHaveLength(0)
  })

  it('should return JWT containing iss and kid headers', async () => {
    const jwt = await jwtSignatureService.signVerifiableCredential({})
    const headers = decodeProtectedHeader(jwt)
    expect(headers).toBeDefined()
    expect(headers.kid).not.toHaveLength(0)
    expect(headers.iss).not.toHaveLength(0)
  })
  it('should return JWT containing the VerifiableCredential as payload', async () => {
    const jwt = await jwtSignatureService.signVerifiableCredential(complianceVC)
    const publicKey = await importSPKI(process.env.publicKey, 'RS256')
    const results = await jwtVerify(jwt, publicKey)
    expect(results).toBeDefined()
    expect(results.payload).toBeDefined()
    expect(results.payload.id).toBeDefined()
    expect(results.payload.credentialSubject).toBeDefined()
    expect((<any>results.payload.credentialSubject).id).toBeDefined()
    expect((<any>results.payload.credentialSubject)['gx:evidence']).toBeDefined()
    expect(Array.isArray((<any>results.payload.credentialSubject)['gx:evidence'])).toBeTruthy()
  })
})
