import { Injectable, InternalServerErrorException } from '@nestjs/common'

import { DIDDocument } from 'did-resolver'
import { ProtectedHeaderParameters, decodeProtectedHeader, jwtVerify } from 'jose'

import { VC_JWT_MIME_TYPE } from '../../common/constants'
import { InvalidDidException } from '../../common/exception/invalid-did.exception'
import { InvalidVerifiablePresentationException } from '../../common/exception/invalid-verifiable-presentation.exception'
import { DIDService } from '../../common/services/did.service'

@Injectable()
export class JwtSignatureValidationService {
  constructor(private readonly didService: DIDService) {}

  /**
   * Takes a JWT-VP and extracts all the VCs in it.
   * @param jwt a JWT-VP
   * @throws InvalidVerifiablePresentationException if the string is not a JWT, if the headers iss & kid are missing, if the signature is invalid
   * @throws ConflictException if the JWT does not contain a VerifiablePresentation containing VerifiableCredential
   */
  async validateJWTAndConvertToVCs(jwt: string) {
    const { payload, key, JWK } = await this.validateSignatureAndDecodeJWT(jwt)

    const verifiableCredentialsEnvelopes = payload.payload['verifiableCredential']
    const verifiableCredentials = []
    if (!verifiableCredentialsEnvelopes) {
      throw new InvalidVerifiablePresentationException('The token should contain a VerifiablePresentation containing VerifiableCredentials')
    }
    if (Array.isArray(verifiableCredentialsEnvelopes)) {
      for (const element of verifiableCredentialsEnvelopes) {
        const credentialJWT = element?.id?.replace(`data:${VC_JWT_MIME_TYPE},`, '')
        const vcDecoded = await this.validateSignatureAndDecodeJWT(credentialJWT)

        verifiableCredentials.push(vcDecoded.payload.payload)
      }
    } else {
      const credentialJWT = verifiableCredentialsEnvelopes['id']?.replace(`data:${VC_JWT_MIME_TYPE},`, '')
      const vcDecoded = await this.validateSignatureAndDecodeJWT(credentialJWT)

      verifiableCredentials.push(vcDecoded.payload.payload)
    }
    return { verifiableCredentials, key, JWK }
  }

  private async validateSignatureAndDecodeJWT(jwt: string) {
    const decodedJWT: ProtectedHeaderParameters = this.decodeJWTHeaders(jwt)
    const { iss, kid } = this.getMandatoryHeadersOrFail(decodedJWT)
    const DID = await this.didService.getDIDDocumentFromDID(iss)
    const JWK = this.getJwkFromDid(DID, kid)
    const key = await this.didService.getPublicKeyFromJWK(JWK)
    return { payload: await this.decodeJWT(jwt, key), key, JWK }
  }

  /**
   * Decodes JWT headers
   * @param jwtVerifiablePresentation a jwt containing a verifiable presentation as payload
   * @throws InvalidVerifiablePresentationException if the string is not a valid JWT
   * @throws InternalServerErrorException if the token was not decoded for an unknown reason
   * @private
   */
  private decodeJWTHeaders(jwtVerifiablePresentation: string) {
    try {
      return decodeProtectedHeader(jwtVerifiablePresentation)
    } catch (error) {
      if (error?.message === 'Invalid Token or Protected Header formatting') {
        throw new InvalidVerifiablePresentationException('The payload is not a valid JWT and was not decoded')
      } else {
        throw new InternalServerErrorException(
          'An error has occurred',
          `An unexpected error has occurred on server-side and we don't know what happened. Please report it
${error.message}`
        )
      }
    }
  }

  /**
   * Retrieves the two mandatory headers iss & kid from a JWT headers. Throws a InvalidVerifiablePresentationException if one of them is not present
   * @param decodedHeaders
   * @private
   * @see https://www.w3.org/TR/vc-jose-cose/#using-header-params-claims-key-discovery
   */
  private getMandatoryHeadersOrFail(decodedHeaders: ProtectedHeaderParameters) {
    const iss = decodedHeaders['iss'] as string
    const kid = decodedHeaders['kid']
    this.checkIssIsPresent(iss)
    this.checkKidIsPresent(kid)
    return { iss, kid }
  }

  /**
   * checks whether the kid header passed as parameter is present
   * @param kid a jwt header, might be null, empty or undefined
   * @throws InvalidVerifiablePresentationException if the header is not filled
   * @private
   */
  private checkKidIsPresent(kid: string) {
    if (!kid) {
      throw new InvalidVerifiablePresentationException('The kid header referencing the verificationMethod of the DID is missing')
    }
  }

  /**
   * checks whether the iss header passed as parameter is present
   * @param iss a jwt header, might be null, empty or undefined
   * @throws InvalidVerifiablePresentationException if the header is not filled
   * @private
   */
  private checkIssIsPresent(iss) {
    if (!iss) {
      throw new InvalidVerifiablePresentationException('Invalid request', "The iss header referencing the issuer's DID is missing")
    }
  }

  /**
   * Retrieves the verificationMethod JWK from a DID based on the JWT's kid header
   * @param DID the DIDDocument
   * @param kid the verificationMethod name
   * @private
   */
  private getJwkFromDid(DID: DIDDocument, kid: string) {
    try {
      return this.didService.getJWKFromDID(DID, kid)
    } catch (error) {
      throw new InvalidDidException(error.message)
    }
  }

  private async decodeJWT(jwt, key) {
    try {
      return await jwtVerify(jwt, key)
    } catch (error) {
      throw new InvalidVerifiablePresentationException('The signature validation has failed', error?.message)
    }
  }
}
