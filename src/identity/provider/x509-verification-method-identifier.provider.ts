import { FactoryProvider, InjectionToken } from '@nestjs/common'

export const x509VerificationMethod = 'X509-JWK'

export class X509VerificationMethodIdentifierProvider implements FactoryProvider {
  provide: InjectionToken = 'x509VerificationMethodIdentifier'
  inject: Array<InjectionToken> = ['DidWeb']

  useFactory(didWeb: string): string {
    return `${didWeb}#${x509VerificationMethod}`
  }
}
