import { X509VerificationMethodIdentifierProvider } from './x509-verification-method-identifier.provider'

describe('X509VerificationMethodIdentifierProvider', () => {
  const provider = new X509VerificationMethodIdentifierProvider()

  it.each([
    ['did:web:localhost:3000:main', 'did:web:localhost:3000:main#X509-JWK'],
    ['did:web:www.gaia-x.eu', 'did:web:www.gaia-x.eu#X509-JWK'],
    ['did:web:www.gaia-x.eu:main', 'did:web:www.gaia-x.eu:main#X509-JWK'],
    ['did:web:test.gaia-x.eu:1234', 'did:web:test.gaia-x.eu:1234#X509-JWK']
  ])('should build a valid Did Web from base URL', (didWeb, verificationMethod) => {
    expect(provider.useFactory(didWeb)).toEqual(verificationMethod)
  })
})
