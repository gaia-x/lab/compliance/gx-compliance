import { CacheModule } from '@nestjs/cache-manager'
import { Module } from '@nestjs/common'
import { ConfigModule } from '@nestjs/config'

import { CertificateController } from './controller/certificate.controller'
import { DidController } from './controller/did.controller'
import { DidWebProvider } from './provider/did-web.provider'
import { X509VerificationMethodIdentifierProvider } from './provider/x509-verification-method-identifier.provider'
import { DidService } from './service/did.service'

/**
 * This module is used to identify the Gaia-X Notary by exposing a Did and an x509 certificate chain
 */
@Module({
  imports: [ConfigModule, CacheModule.register()],
  controllers: [DidController, CertificateController],
  providers: [new DidWebProvider(), new X509VerificationMethodIdentifierProvider(), DidService],
  exports: ['DidWeb', 'x509VerificationMethodIdentifier', DidService]
})
export class IdentityModule {}
