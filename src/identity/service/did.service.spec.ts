import { ConfigService } from '@nestjs/config'

import { mkdtempSync, writeFileSync } from 'fs'
import { pki } from 'node-forge'
import { tmpdir } from 'os'
import { join } from 'path'

import { X509_CERTIFICATE } from '../../common/constants'
import { CertificateBuilderUtilsSpec } from '../../tests/certificate-builder-utils.spec'
import { DidService } from './did.service'

describe('DidService', () => {
  let configService: ConfigService
  let didService: DidService
  let certificate: pki.Certificate

  beforeEach(() => {
    configService = new ConfigService()
    jest.spyOn(configService, 'get').mockImplementation((property: string) => {
      if (property === X509_CERTIFICATE) {
        return certificate?.toString()
      }

      if (property === 'BASE_URL') {
        return 'http://localhost:3000/main'
      }

      throw new Error('Invalid configuration property')
    })

    didService = new DidService('did:web:gaia-x.eu', configService)
  })

  it('should get the certificate chain from environment variables', async () => {
    certificate = CertificateBuilderUtilsSpec.createCertificate(12)

    expect(didService.getCertChain(null)).toEqual(certificate.toString())
  })

  it('should get the certificate chain from file system', async () => {
    certificate = null
    const fsCertificate: pki.Certificate = CertificateBuilderUtilsSpec.createCertificate(12)

    const certificatePath = mkdtempSync(join(tmpdir(), 'notary-certificate-')) + '/certificate.pem'
    writeFileSync(certificatePath, fsCertificate.toString())

    expect(didService.getCertChain(certificatePath)).toEqual(fsCertificate.toString())
  })
})
