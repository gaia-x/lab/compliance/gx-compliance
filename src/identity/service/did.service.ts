import { Inject, Injectable } from '@nestjs/common'
import { ConfigService } from '@nestjs/config'

import { readFileSync } from 'fs'
import * as jose from 'jose'
import { join } from 'path'

import { X509_CERTIFICATE } from '../../common/constants'

@Injectable()
export class DidService {
  static readonly FALLBACK_CERTIFICATE_PATH = join(__dirname, '../static/.well-known/x509CertificateChain.pem')
  private readonly publicKeyAlgorithm: string = 'PS256'
  private readonly x509VerificationMethod: string = 'X509-JWK'
  private readonly baseUrl: string

  constructor(@Inject('DidWeb') private readonly didWeb: string, private readonly configService: ConfigService) {
    // This will always be provided as it is checked in the AppController
    this.baseUrl = this.configService.get<string>('BASE_URL').trim()
  }

  getCertChain(fallbackPath: string): string {
    return this.configService.get<string>(X509_CERTIFICATE) || readFileSync(fallbackPath).toString()
  }

  async buildDid(): Promise<any> {
    const certChainUri = `${this.baseUrl}/.well-known/x509CertificateChain.pem`

    const certChain = await jose.importX509(this.getCertChain(DidService.FALLBACK_CERTIFICATE_PATH), this.publicKeyAlgorithm)
    const x509VerificationMethodIdentifier = `${this.didWeb}#${this.x509VerificationMethod}`

    return {
      '@context': ['https://www.w3.org/ns/did/v1', 'https://w3id.org/security/jwk/v1'],
      id: this.didWeb,
      verificationMethod: [
        {
          id: x509VerificationMethodIdentifier,
          type: 'JsonWebKey',
          controller: this.didWeb,
          publicKeyJwk: {
            ...(await jose.exportJWK(certChain)),
            kid: this.x509VerificationMethod,
            alg: this.publicKeyAlgorithm,
            x5u: certChainUri
          }
        }
      ],
      assertionMethod: [x509VerificationMethodIdentifier]
    }
  }

  getVerificationMethodIdentifier(): string {
    return this.x509VerificationMethod
  }
}
