import { EnvironmentContext, JestEnvironmentConfig } from '@jest/environment'
import { readFileSync } from 'fs'
import NodeEnvironment from 'jest-environment-node'
import jsonld from 'jsonld'
import neo4j, { Driver } from 'neo4j-driver'
import path from 'node:path'
import { GenericContainer, StartedTestContainer, Wait } from 'testcontainers'

import { RdfGraphUtils } from '../common/utils/rdf-graph.utils'

export default class MemgraphEnvironmentSetup extends NodeEnvironment {
  constructor(config: JestEnvironmentConfig, context: EnvironmentContext) {
    super(config, context)
  }

  async setup() {
    await super.setup()

    if (!this.global.memgraphContainer) {
      await new Promise(async resolve => {
        const memgraphContainer: StartedTestContainer = await new GenericContainer('memgraph/memgraph:latest')
          .withName('gx-compliance-integration-test-container')
          .withExposedPorts(7687)
          .withWaitStrategy(Wait.forLogMessage(/.*You are running Memgraph.*/))
          .withReuse()
          .start()

        const memgraphUrl = `bolt://${memgraphContainer.getHost()}:${memgraphContainer.getMappedPort(7687)}`
        const driver: Driver = neo4j.driver(memgraphUrl)

        this.global.memgraphContainer = memgraphContainer
        this.global.memgraphUrl = memgraphUrl
        this.global.memgraphDriver = driver

        this.global.ontologyTriples = readFileSync(path.resolve(__dirname, './fixtures/ontology/ontology.ttl'), 'utf8')
        this.global.ontologyShapes = readFileSync(path.resolve(__dirname, './fixtures/shapes/gaia_x_development.ttl'), 'utf8')
        this.global.parentClasses = await RdfGraphUtils.extractEntityParentClasses(this.global.ontologyTriples as string)

        resolve(this.global.memgraphDriver)
      })
    }
  }

  async teardown() {
    await super.teardown()
  }
}

export async function insertObjectInMemGraph(driver: Driver, vpUuid: string, object: any) {
  const quads = await jsonld.toRDF(object, { format: 'application/n-quads' })
  const queries: string[] = RdfGraphUtils.quadsToQueries(vpUuid, quads, global.parentClasses)

  const session = driver.session()
  for (const query of queries) {
    await session.executeWrite(tx => tx.run(query))
  }

  await session.close()
}
