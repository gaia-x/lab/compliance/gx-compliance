import { OfflineDocumentLoaderBuilder } from '@gaia-x/json-web-signature-2020'
import jsonld from 'jsonld'

import CredentialsV2 from '../contexts/credentials_v2_context.json'
import SchemaContext from '../contexts/schema_context.json'

jsonld.documentLoader = new OfflineDocumentLoaderBuilder()
  .addContext('https://schema.org/', SchemaContext)
  .addContext('http://schema.org/', SchemaContext)
  .addContext('https://www.w3.org/ns/credentials/v2', CredentialsV2)
  .addCachedContext('https://w3id.org/gaia-x/development#')
  .addCachedContext('https://w3id.org/gaia-x/main#')
  .addCachedContext('https://w3id.org/gaia-x/v1#')
  .addCachedContext('https://w3id.org/gaia-x/v2#')
  .build()
