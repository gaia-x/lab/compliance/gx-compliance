import { Container } from 'dockerode'
import { ContainerRuntimeClient, getContainerRuntimeClient } from 'testcontainers'

export default async () => {
  try {
    const containerClient: ContainerRuntimeClient = await getContainerRuntimeClient()
    const memgraphContainer: Container = containerClient.container.getById('gx-compliance-integration-test-container')
    await containerClient.container.stop(memgraphContainer)
    await containerClient.container.remove(memgraphContainer)
  } catch (e) {
    console.error(e)
  }
}
