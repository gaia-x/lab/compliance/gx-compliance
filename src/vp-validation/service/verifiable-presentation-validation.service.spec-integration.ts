import { Logger } from '@nestjs/common'
import { ConfigService } from '@nestjs/config'
import { Test } from '@nestjs/testing'

import { DateTime } from 'luxon'
import { Session } from 'neo4j-driver'
import { v4 as uuidv4 } from 'uuid'

import { VerifiablePresentation } from '../../common/constants'
import { ValidationResult } from '../../common/dto'
import { ConformityLevelEnum } from '../../common/enum/conformity-level.enum'
import { ConfigServiceMock } from '../../tests/config.service.mock'
import { VpContentValidationModule } from '../vp-content-validation.module'
import validVerifiablePresentation from './fixtures/verifiable-presentation-validation-service/valid-verifiable-presentation-basic-conformity.json'
import { RegistryService } from './registry.service'
import { VerifiablePresentationValidationService } from './verifiable-presentation-validation.service'

describe('VerifiablePresentationValidationService', () => {
  let service: VerifiablePresentationValidationService

  beforeAll(async () => {
    process.env.dburl = global.memgraphUrl

    const moduleFixture = await Test.createTestingModule({
      imports: [VpContentValidationModule]
    })
      .overrideProvider('PRIVATE_KEY')
      .useValue(null)
      .overrideProvider(ConfigService)
      .useValue(
        new ConfigServiceMock({
          BASE_URL: 'https://localhost'
        })
      )
      .overrideProvider(RegistryService)
      .useValue({
        loadOntologyFromUrl: () => global.ontologyTriples,
        loadTurtleShapesFromUrl: () => global.ontologyShapes
      })
      .setLogger(new Logger())
      .compile()

    service = await moduleFixture.resolve(VerifiablePresentationValidationService)
  })

  it('should validate a BASIC_CONFORMITY verifiable presentation', async () => {
    const validationResult: ValidationResult = await service.validate(validVerifiablePresentation, ConformityLevelEnum.STANDARD_COMPLIANCE)

    expect(validationResult.results).toStrictEqual([])
    expect(validationResult.conforms).toBe(true)
  })

  it('should cleanup memgraph after a specific vpUUID', async () => {
    let uuid = uuidv4()
    while (!isNaN(uuid[0])) {
      uuid = uuidv4()
    }

    await (service as any).insertVpInMemGraph(uuid, validVerifiablePresentation)
    await (service as any).cleanupMemGraph(uuid)

    const session: Session = global.memgraphDriver.session()
    try {
      const result = await session.executeRead(tx =>
        tx.run(
          `
          MATCH (n)
          WHERE n.vpID = $uuid
          RETURN COUNT(*) AS nodeCount`,
          { uuid }
        )
      )
      expect(result.records[0].get('nodeCount').toNumber()).toEqual(0)
    } finally {
      await session.close()
    }
  })

  it('should throw an error when verifiable presentation is invalid', async () => {
    const invalidVerifiablePresentation: VerifiablePresentation = structuredClone(validVerifiablePresentation)
    const expiredValidUntil: string = DateTime.now().minus({ day: 10 }).toISO()

    invalidVerifiablePresentation.verifiableCredential[0].validUntil = expiredValidUntil
    invalidVerifiablePresentation.verifiableCredential.find(vc => vc.type.includes('gx:Issuer')).credentialSubject.gaiaxTermsAndConditions =
      'invalidTs&Cs'

    const validationResult: ValidationResult = await service.validate(invalidVerifiablePresentation, ConformityLevelEnum.STANDARD_COMPLIANCE)

    expect(validationResult.conforms).toBe(false)
    expect(validationResult.results).toEqual(
      expect.arrayContaining([
        'The issuer: example.org has an incorrect Gaia-X terms and conditions hash. Please follow https://docs.gaia-x.eu/ontology/development/enums/GaiaXTermsAndConditions for more details.',
        `VC undefined validUntil ${expiredValidUntil} is in the past`
      ])
    )
  })
})
