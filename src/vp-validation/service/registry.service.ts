import { HttpService } from '@nestjs/axios'
import { CACHE_MANAGER } from '@nestjs/cache-manager'
import { Inject, Injectable, Logger } from '@nestjs/common'
import { ConfigService } from '@nestjs/config'

import { Cache } from 'cache-manager'
import { catchError, firstValueFrom } from 'rxjs'

import { OntologyException } from '../../common/exception/ontology.exception'
import { ShaclShapeException } from '../../common/exception/shacl-shape.exception'

@Injectable()
export class RegistryService {
  private readonly shapeCacheTtl: number = 3_600_000 // 1 hour in milliseconds
  private readonly ontologyCacheTtl: number = 600_000 // 10 minutes in milliseconds
  private readonly shapesKey: string = 'shapes'
  private readonly ontologyKey: string = 'ontology'

  private readonly registryUrl: string
  private readonly logger = new Logger(RegistryService.name)

  constructor(
    @Inject(CACHE_MANAGER) private readonly cacheManager: Cache,
    private readonly httpService: HttpService,
    private readonly configService: ConfigService
  ) {
    this.registryUrl = this.configService.get<string>('REGISTRY_URL', 'https://registry.gaia-x.eu/v2')
  }

  async isValidCertificateChain(raw: string): Promise<boolean> {
    try {
      // skip signature check against registry - NEVER ENABLE IN PRODUCTION
      if (this.configService.get('DISABLE_SIGNATURE_CHECK') === 'true') return true

      const response = await firstValueFrom(
        this.httpService.post(`${this.registryUrl}/api/trustAnchor/chain`, {
          certs: raw
        })
      )

      return response.status === 200
    } catch (error) {
      console.error(error)
      this.logger.error(error)
    }
  }

  async loadTurtleShapesFromUrl(shapesURL: string): Promise<string> {
    const cachedShapes: string = await this.cacheManager.get(shapesURL + this.shapesKey)
    if (cachedShapes) {
      return cachedShapes
    }

    const shapes: string = (
      await firstValueFrom(
        this.httpService.get(shapesURL, { headers: { Accept: 'text/turtle' } }).pipe(
          catchError(error => {
            throw new ShaclShapeException(`An error occurred while loading turtle shapes from ${shapesURL}: ${error.message}`)
          })
        )
      )
    ).data

    await this.cacheManager.set(shapesURL + this.shapesKey, shapes, this.shapeCacheTtl)

    return shapes
  }

  async loadOntologyFromUrl(ontologyURL: string): Promise<string> {
    const cachedOntology: string = await this.cacheManager.get(ontologyURL + this.ontologyKey)
    if (cachedOntology) {
      return cachedOntology
    }

    const ontology: string = (
      await firstValueFrom(
        this.httpService.get(ontologyURL, { headers: { Accept: 'application/rdf+xml' } }).pipe(
          catchError(error => {
            throw new OntologyException(`An error occurred while loading ontology from ${ontologyURL}: ${error.message}`)
          })
        )
      )
    ).data

    await this.cacheManager.set(ontologyURL + this.ontologyKey, ontology, this.ontologyCacheTtl)

    return ontology
  }
}
