import { ConfigService } from '@nestjs/config'
import { Test, TestingModule } from '@nestjs/testing'

import { readFileSync } from 'fs'
import * as path from 'node:path'

import { ShaclShapeException } from '../../common/exception/shacl-shape.exception'
import vpArrayOfCredsSingleSubject from './fixtures/shacl-service/vp-array-with-multiple-single-subject-credentials.json'
import vpArrayOfOneCredSingleSubject from './fixtures/shacl-service/vp-array-with-one-single-subject-credential.json'
import vpCredentialObjectMultipleSubjects from './fixtures/shacl-service/vp-with-multi-subject-credential.json'
import vpCredentialObject from './fixtures/shacl-service/vp-with-single-subject-credential.json'
import vpSubjectHasTypeInArray from './fixtures/shacl-service/vp-with-typed-subject-array-credential.json'
import vpSubjectHasType from './fixtures/shacl-service/vp-with-typed-subject-credential.json'
import { RegistryService } from './registry.service'
import { ShaclService } from './shacl.service'

import resetAllMocks = jest.resetAllMocks

describe('ShaclService', () => {
  const registryServiceMock: RegistryService = {
    loadTurtleShapesFromUrl: jest.fn(),
    isValidCertificateChain: jest.fn()
  } as unknown as RegistryService
  let configServiceMock: Partial<ConfigService>

  let service: ShaclService

  beforeEach(async () => {
    configServiceMock = {
      get: jest.fn().mockImplementation((key: string) => {
        if (key === 'ALLOWED_CONTEXT_VERSIONS') {
          return '["development", "main", "v1", "v2"]'
        }
      })
    }

    const moduleFixture: TestingModule = await Test.createTestingModule({
      providers: [
        ShaclService,
        { provide: 'documentLoader', useValue: jest.fn() },
        { provide: ConfigService, useValue: configServiceMock },
        { provide: RegistryService, useValue: registryServiceMock }
      ]
    }).compile()

    service = await moduleFixture.resolve<ShaclService>(ShaclService)
  })

  afterEach(() => {
    resetAllMocks()
  })

  it('should be defined', () => {
    expect(service).toBeDefined()
  })

  it('should load shapes from registry', async () => {
    jest.spyOn(registryServiceMock, 'loadTurtleShapesFromUrl').mockReturnValue(Promise.resolve('test string'))
    const shapes = await service.loadShaclFromUrl('https://example.org')
    expect(shapes).toBeDefined()
    expect(shapes).toBe('test string')
  })

  it('should throw an exception when shapes cannot be loaded from registry', async () => {
    jest.spyOn(registryServiceMock, 'loadTurtleShapesFromUrl').mockReturnValue(Promise.reject('error string'))

    try {
      await service.loadShaclFromUrl('https://example.org')
    } catch (error) {
      expect(error).toBeInstanceOf(ShaclShapeException)
      expect(error.getResponse().error).toMatch(/error string/)

      return
    }

    throw new Error()
  })

  it('should be able to parse SHACL results', () => {
    const results = service.formatJenaShacl(readFileSync(path.resolve(__dirname, './fixtures/shacl-service/shacl-validation-report.ttl'), 'utf8'))
    expect(results).toEqual([
      'An error has occurred while checking the shape of your input  focusNode:<https://trusted-issuer.com/so.json#cs>  resultMessage:Or at focusNode <https://trusted-issuer.com/data.json#cs>  sh:resultPath:gx:aggregationOfResources  sourceConstraintComponent:sh:OrConstraintComponent  sourceShape:[]   value:<https://trusted-issuer.com/data.json#cs> ',
      'An error has occurred while checking the shape of your input  focusNode:<https://trusted-issuer.com/so.json#cs>  resultMessage:Closed[https://schema.org/description, https://w3id.org/gaia-x/development#dataAccountExport, https://w3id.org/gaia-x/development#dataProtectionRegime, https://w3id.org/gaia-x/development#serviceScope, https://w3id.org/gaia-x/development#providerContactInformation, https://w3id.org/gaia-x/development#legalDocuments, https://w3id.org/gaia-x/development#possiblePersonalDataTransfers, https://w3id.org/gaia-x/development#keyword, https://w3id.org/gaia-x/development#hostedOn, https://w3id.org/gaia-x/development#endpoint, https://w3id.org/gaia-x/development#provisionType, https://w3id.org/gaia-x/development#cryptographicSecurityStandards, https://w3id.org/gaia-x/development#aggregationOfResources, https://schema.org/name, https://w3id.org/gaia-x/development#dependsOn, https://w3id.org/gaia-x/development#subContractors, https://w3id.org/gaia-x/development#requiredMeasures, https://w3id.org/gaia-x/development#serviceOfferingTermsAndConditions, https://w3id.org/gaia-x/development#providedBy, https://w3id.org/gaia-x/development#customerInstructions, https://w3id.org/gaia-x/development#dataPortability, https://w3id.org/gaia-x/development#servicePolicy][http://www.w3.org/1999/02/22-rdf-syntax-ns#type] Property = <https://w3id.org/gaia-x/development#legallyBindingActs> : Object = _:Bc7b3a2a6b1647355895988ef482c5690  sh:resultPath:gx:legallyBindingActs  sourceConstraintComponent:sh:ClosedConstraintComponent  sourceShape:gx:ServiceOfferingShape  value:[] ',
      'An error has occurred while checking the shape of your input  focusNode:<https://trusted-issuer.com/so.json#cs>  resultMessage:Closed[https://schema.org/description, https://w3id.org/gaia-x/development#dataAccountExport, https://w3id.org/gaia-x/development#dataProtectionRegime, https://w3id.org/gaia-x/development#serviceScope, https://w3id.org/gaia-x/development#providerContactInformation, https://w3id.org/gaia-x/development#legalDocuments, https://w3id.org/gaia-x/development#possiblePersonalDataTransfers, https://w3id.org/gaia-x/development#keyword, https://w3id.org/gaia-x/development#hostedOn, https://w3id.org/gaia-x/development#endpoint, https://w3id.org/gaia-x/development#provisionType, https://w3id.org/gaia-x/development#cryptographicSecurityStandards, https://w3id.org/gaia-x/development#aggregationOfResources, https://schema.org/name, https://w3id.org/gaia-x/development#dependsOn, https://w3id.org/gaia-x/development#subContractors, https://w3id.org/gaia-x/development#requiredMeasures, https://w3id.org/gaia-x/development#serviceOfferingTermsAndConditions, https://w3id.org/gaia-x/development#providedBy, https://w3id.org/gaia-x/development#customerInstructions, https://w3id.org/gaia-x/development#dataPortability, https://w3id.org/gaia-x/development#servicePolicy][http://www.w3.org/1999/02/22-rdf-syntax-ns#type] Property = <https://w3id.org/gaia-x/development#legallyBindingActs> : Object = _:B1e8fad3891b1c2ca33607f2a4bf06d18  sh:resultPath:gx:legallyBindingActs  sourceConstraintComponent:sh:ClosedConstraintComponent  sourceShape:gx:ServiceOfferingShape  value:[] '
    ])
  })

  it('should return the correct Gaia-X context URL', () => {
    const verifiablePresentation = {
      verifiableCredential: [{ '@context': ['https://example.org/', 'https://w3id.org/gaia-x/development'] }]
    }
    const result = service.getShapesUrlFromContext(verifiablePresentation)
    expect(result).toBe('https://w3id.org/gaia-x/development')
  })

  it('should return undefined if no Gaia-X context URL is found', () => {
    const verifiablePresentation = {
      verifiableCredential: [{ '@context': ['https://example.org/'] }]
    }
    const result = service.getShapesUrlFromContext(verifiablePresentation)
    expect(result).toBeUndefined()
  })

  it('should return the context version if it is allowed', () => {
    const verifiablePresentation = {
      verifiableCredential: [{ '@context': ['https://w3id.org/gaia-x/development'] }]
    }
    const result = service.getContextVersion(verifiablePresentation)
    expect(result).toBe('development')
  })

  it('should throw ShaclShapeException if the context version is not allowed', () => {
    const verifiablePresentation = {
      verifiableCredential: [{ '@context': ['https://w3id.org/gaia-x/invalidVersion'] }]
    }
    expect(() => service.getContextVersion(verifiablePresentation)).toThrow(ShaclShapeException)
  })

  it('should handle nested contexts and return the correct context version', () => {
    const verifiablePresentation = {
      verifiableCredential: [{ '@context': [['https://example.org/'], ['https://w3id.org/gaia-x/v1']] }]
    }
    const result = service.getContextVersion(verifiablePresentation)
    expect(result).toBe('v1')
  })

  it('should filter out triples where the subject is a literal', () => {
    const inputQuads = `
      "RpxmsdMz" <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://www.w3.org/2001/XMLSchema#string> .
      <http://example.org/Duty_211#cs> <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> _:B64a6d0abd8901ef3ef3b9a7925476fbe .
      <http://example.org/Duty_211#cs> <http://www.w3.org/ns/odrl/2/failure> "RpxmsdMz" .
    `

    const expectedOutput = `
      <http://example.org/Duty_211#cs> <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> _:B64a6d0abd8901ef3ef3b9a7925476fbe .
      <http://example.org/Duty_211#cs> <http://www.w3.org/ns/odrl/2/failure> "RpxmsdMz" .
    `

    const result = (service as any).filterInvalidTriples(inputQuads.trim())
    expect(result.trim()).toBe(expectedOutput.trim())
  })

  describe('CredentialSubject retrieval', () => {
    it('should retrieve credentialSubjects from VerifiablePresentation with multiple credentials', () => {
      const subjects = service.extractCredentialSubjectsFromVerifiablePresentation(vpArrayOfCredsSingleSubject)
      expect(subjects).toBeDefined()
      expect(subjects).toHaveLength(3)
      expect(subjects[0]['type']).toContain('gx:LegalPerson')
      expect(subjects[1]['type']).toContain('gx:Issuer')
      expect(subjects[2]['type']).toContain('gx:VatID')
    })

    it('should retrieve credentialSubject from VerifiablePresentation with credential array with one credential', () => {
      const subjects = service.extractCredentialSubjectsFromVerifiablePresentation(vpArrayOfOneCredSingleSubject)
      expect(subjects).toBeDefined()
      expect(subjects).toHaveLength(1)
    })

    it('should retrieve credentialSubject from VerifiablePresentation with credential being one credential', () => {
      const subjects = service.extractCredentialSubjectsFromVerifiablePresentation(vpCredentialObject)
      expect(subjects).toBeDefined()
      expect(subjects).toHaveLength(1)
    })

    it('should retrieve credentialSubjects from VerifiablePresentation with one VC with multiple subjects', () => {
      const subjects = service.extractCredentialSubjectsFromVerifiablePresentation(vpCredentialObjectMultipleSubjects)
      expect(subjects).toBeDefined()
      expect(subjects).toHaveLength(2)
    })

    it('should retrieve subjectType from VerifiablePresentation with one VC with single subject typed', () => {
      const subjects = service.extractCredentialSubjectsFromVerifiablePresentation(vpSubjectHasType)
      expect(subjects).toBeDefined()
      expect(subjects).toHaveLength(1)
      expect(subjects[0]['type']).toContain('gx:LegalPerson')
    })

    it('should retrieve subjectType from VerifiablePresentation with one VC with single subject typed in array', () => {
      const subjects = service.extractCredentialSubjectsFromVerifiablePresentation(vpSubjectHasTypeInArray)
      expect(subjects).toBeDefined()
      expect(subjects).toHaveLength(1)
      expect(subjects[0]['type']).toContain('gx:LegalPerson')
    })
  })
})
