import { Inject, Injectable, Logger } from '@nestjs/common'
import { ConfigService } from '@nestjs/config'

import { DocumentLoader } from '@gaia-x/json-web-signature-2020'
import { exec } from 'child_process'
import jsonld from 'jsonld'
import * as fs from 'node:fs'
import { mkdtemp } from 'node:fs/promises'
import { tmpdir } from 'node:os'
import * as path from 'node:path'
import { join } from 'path'
import { promisify } from 'util'

import { ValidationResult } from '../../common/dto'
import { ShaclShapeException } from '../../common/exception/shacl-shape.exception'
import { HashingUtils } from '../../common/utils/hashing.utils'
import { RegistryService } from './registry.service'

const promiseExec = promisify(exec)

@Injectable()
export class ShaclService {
  private readonly logger = new Logger(ShaclService.name)
  private readonly allowedContextVersions: string[]

  private shapesHash = ''
  private shapesDirectory = ''

  private ontologyHash = ''
  private ontologyDirectory = ''

  constructor(
    @Inject('documentLoader') private readonly documentLoader: DocumentLoader,
    private readonly registryService: RegistryService,
    private configService: ConfigService
  ) {
    this.allowedContextVersions = JSON.parse(
      this.configService.get<string>('ALLOWED_CONTEXT_VERSIONS', '["development", "main", "v1", "v2"]')
    ) as string[]
  }

  async loadShaclFromUrl(contextUrl: string): Promise<string> {
    try {
      return await this.registryService.loadTurtleShapesFromUrl(contextUrl)
    } catch (error) {
      this.logger.error(`${error}, Url used to fetch shapes: ${contextUrl}`)
      throw new ShaclShapeException(error)
    }
  }

  public async verifyShape(verifiablePresentation: any): Promise<ValidationResult> {
    try {
      const directory = await mkdtemp(join(tmpdir(), 'shaclValidation-'))
      const gaiaXContext = this.getShapesUrlFromContext(verifiablePresentation)
      const credentialSubjects = this.extractCredentialSubjectsFromVerifiablePresentation(verifiablePresentation)
      const vcs = {
        '@context': this.extractContextsFromSubjects(credentialSubjects),
        '@graph': credentialSubjects
      }

      const quads = await jsonld.canonize(vcs, {
        format: 'application/n-quads',
        documentLoader: this.documentLoader
      })

      fs.writeFileSync(directory + '/data.n3', quads)

      await promiseExec(`JVM_ARGS="-XX:+UseContainerSupport -XX:MaxRAMPercentage=80.0"
        ${path.join(__dirname, '../../../tools/jena/bin/')}riot --nocheck --rdfs=${await this.getPathOnDiskForOntology(
        gaiaXContext
      )} ${directory}/data.n3 > ${directory}/inferredData.nq`)

      const inferredData = fs.readFileSync(directory + '/inferredData.nq', 'utf8')
      const filteredData = this.filterInvalidTriples(inferredData)
      fs.writeFileSync(directory + '/inferredData.nq', filteredData)

      const res = await promiseExec(
        `JVM_ARGS="-XX:+UseContainerSupport -XX:MaxRAMPercentage=80.0" ${path.join(
          __dirname,
          '../../../tools/jena/bin/'
        )}shacl validate --data ${directory}/inferredData.nq --shapes ${await this.getPathOnDiskForShapes(gaiaXContext)}`
      )

      if (/sh:conforms *true/.test(res.stdout)) {
        return new ValidationResult()
      } else {
        return {
          conforms: false,
          results: this.formatJenaShacl(res.stdout)
        }
      }
    } catch (error) {
      this.logger.error(error)
      return {
        conforms: false,
        results: [error.message]
      }
    }
  }

  public getShapesUrlFromContext(verifiablePresentation: any): string {
    return verifiablePresentation?.verifiableCredential
      .flatMap(cred => cred['@context'])
      .flat(Infinity)
      .find(ctx => ctx.indexOf('https://w3id.org/gaia-x/') > -1)
  }

  public getContextVersion(verifiablePresentation: any): string {
    const contextVersion = new URL(this.getShapesUrlFromContext(verifiablePresentation)).pathname
      .split('/')
      .filter(Boolean)
      .reduce((_, segment) => segment, null)
    if (!this.allowedContextVersions.includes(contextVersion)) {
      throw new ShaclShapeException(`Context version ${contextVersion} is not allowed by configuration.`)
    }
    return contextVersion
  }

  /**
   * This is used to extract credential subjects from verifiable credentials contained in the verifiablePresentation
   * Can be removed as soon as the shapes are able to target CredentialSubjects directly and not specific target classes
   * @param verifiablePresentation
   */
  public extractCredentialSubjectsFromVerifiablePresentation(verifiablePresentation: any): any[] {
    if (!Array.isArray(verifiablePresentation['verifiableCredential'])) {
      return this.extractCredentialSubjectsFromVerifiableCredential(verifiablePresentation['verifiableCredential'])
    }
    const credentialSubjects = []
    for (const vc of verifiablePresentation['verifiableCredential']) {
      credentialSubjects.push(...this.extractCredentialSubjectsFromVerifiableCredential(vc))
    }
    return credentialSubjects
  }

  public formatJenaShacl(results: string) {
    const formattedShaclErrors = []
    const regex = /(sh:resultPath|focusNode|resultMessage|sourceConstraintComponent|sourceShape|value) *"?(.*?)"?[;|\n]/gm
    const shaclErrors = results.matchAll(regex)
    let countForNewError = 0
    let errorMessage = 'An error has occurred while checking the shape of your input '
    for (const error of shaclErrors) {
      if (countForNewError === 6) {
        formattedShaclErrors.push(errorMessage)
        countForNewError = 0
        errorMessage = 'An error has occurred while checking the shape of your input '
      }
      errorMessage += ` ${error[1]}:${error[2]} `
      countForNewError++
    }
    formattedShaclErrors.push(errorMessage)
    return formattedShaclErrors
  }

  // Todo: remove when this issue is fixed: https://github.com/apache/jena/issues/1720
  private filterInvalidTriples = (quads: string): string => {
    const lines = quads.split('\n')
    const validLines = lines.filter(line => {
      const subject = line.split(' ')[0]
      // Keep lines where subject is not a literal
      return !subject.startsWith('"')
    })
    return validLines.join('\n')
  }

  private extractCredentialSubjectsFromVerifiableCredential(verifiableCredential: any): any[] {
    if (!Array.isArray(verifiableCredential['credentialSubject'])) {
      return [
        this.mapCredentialSubjectIntoUsableJSONLD(
          verifiableCredential['credentialSubject'],
          verifiableCredential['@context'],
          verifiableCredential['type'] ?? verifiableCredential['@type']
        )
      ]
    }
    const mappedCS = []
    for (const cs of verifiableCredential['credentialSubject']) {
      mappedCS.push(
        this.mapCredentialSubjectIntoUsableJSONLD(cs, verifiableCredential['@context'], verifiableCredential['type'] ?? verifiableCredential['@type'])
      )
    }
    return mappedCS
  }

  private extractContextsFromSubjects(credentialSubjects: any[]): string[] {
    const contexts = new Set<string>()
    credentialSubjects
      .map((cs: Array<string>) => {
        return cs['@context']
      })
      .flat()
      .forEach(context => {
        contexts.add(context)
      })
    return Array.from(contexts)
  }

  private mapCredentialSubjectIntoUsableJSONLD(credentialSubject: any, contexts: string[], types: any[]) {
    const mappedCS = {
      ...credentialSubject,
      '@context': [...contexts],
      type: [...types]
    }
    if (!!credentialSubject['type'] && Array.isArray(credentialSubject['type'])) {
      mappedCS.type.push(...credentialSubject['type'])
    } else if (!!credentialSubject['type']) {
      mappedCS.type.push(credentialSubject['type'])
    }
    if (!!credentialSubject['@type'] && Array.isArray(credentialSubject['@type'])) {
      mappedCS.type.push(...credentialSubject['@type'])
    } else if (!!credentialSubject['@type']) {
      mappedCS.type.push(credentialSubject['@type'])
    }
    return mappedCS
  }

  private async getPathOnDiskForShapes(gaiaXContext: string): Promise<string> {
    const shapes = await this.registryService.loadTurtleShapesFromUrl(gaiaXContext)
    const latestShapesHash = HashingUtils.sha512(shapes)
    if (latestShapesHash === this.shapesHash && fs.existsSync(this.shapesDirectory)) {
      return this.shapesDirectory
    }
    this.logger.debug('Shapes not stored on disk, retrieving and writing now')
    this.shapesDirectory = (await mkdtemp(join(tmpdir(), 'shapes-'))) + '/shape.ttl'
    fs.writeFileSync(this.shapesDirectory, shapes)
    this.shapesHash = latestShapesHash
    this.logger.debug('Shapes now stored on disk')
    return this.shapesDirectory
  }

  private async getPathOnDiskForOntology(gaiaXContext: string): Promise<string> {
    const ontology = await this.registryService.loadOntologyFromUrl(gaiaXContext)

    const latestOntologyHash = HashingUtils.sha512(ontology)
    if (latestOntologyHash === this.ontologyHash && fs.existsSync(this.ontologyDirectory)) {
      return this.ontologyDirectory
    }
    this.logger.debug('Ontology not stored on disk, retrieving and writing now')
    this.ontologyDirectory = (await mkdtemp(join(tmpdir(), 'ontology-'))) + '/ontology.ttl'
    fs.writeFileSync(this.ontologyDirectory, ontology)
    this.ontologyHash = latestOntologyHash
    this.logger.debug('Ontology now stored on disk')
    return this.ontologyDirectory
  }
}
