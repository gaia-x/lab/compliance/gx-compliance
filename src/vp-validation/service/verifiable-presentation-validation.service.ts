import { Injectable, Logger, Scope } from '@nestjs/common'

import { InjectMetric } from '@willsoto/nestjs-prometheus'
import jsonld from 'jsonld'
import neo4j from 'neo4j-driver'
import * as process from 'node:process'
import { Summary } from 'prom-client'
import { StopWatch } from 'stopwatch-node'
import { v4 as uuidv4 } from 'uuid'

import { VerifiablePresentation } from '../../common/constants'
import { ConformityLevelEnum } from '../../common/enum/conformity-level.enum'
import { mergeFilterResults } from '../../common/utils/merge-results'
import { ParentClasses, RdfGraphUtils } from '../../common/utils/rdf-graph.utils'
import {
  CommunicationsAreSecureFilter,
  DataResourceIssuerMatchesProducerIssuerFilter,
  FilterValidationResult,
  IssuersHaveTermsAndConditionsFilter,
  LegalDocumentHasInvolvedParties,
  RegistrationNumbersAreFromTrustedIssuersFilter,
  ResourceHasConsentIfContainsPiiFilter,
  ServiceOfferingHasAResourceWithAddressFilter,
  ServiceOfferingHasAccessControlManagementFilter,
  ServiceOfferingHasAssetsManagementFilter,
  ServiceOfferingHasBusinessContinuityMeasuresFilter,
  ServiceOfferingHasChangeAndConfigurationManagementFilter,
  ServiceOfferingHasComplianceAssuranceFilter,
  ServiceOfferingHasCryptographicSecurityStandards,
  ServiceOfferingHasCustomerAuditingRightsFilter,
  ServiceOfferingHasCustomerDataAccessTermsFilter,
  ServiceOfferingHasCustomerInstructionsFilter,
  ServiceOfferingHasDataPortabilityFilter,
  ServiceOfferingHasDataProtectionAndServiceAgreementFilter,
  ServiceOfferingHasDataUsageRightsForPartiesFilter,
  ServiceOfferingHasDescribedDataTransfersFilter,
  ServiceOfferingHasDevelopmentCycleSecurityFilter,
  ServiceOfferingHasDocumentedChangeProceduresFilter,
  ServiceOfferingHasEmployeeResponsibilitiesFilter,
  ServiceOfferingHasGovernmentInvestigationManagementFilter,
  ServiceOfferingHasInformationSecurityOrganizationFilter,
  ServiceOfferingHasInformationSecurityPoliciesFilter,
  ServiceOfferingHasInformationSecurityRiskManagementFilter,
  ServiceOfferingHasLegallyBindingActFilter,
  ServiceOfferingHasOperationalSecurityFilter,
  ServiceOfferingHasPhysicalSecurityFilter,
  ServiceOfferingHasProcurementManagementSecurityFilter,
  ServiceOfferingHasProductSecurityFilter,
  ServiceOfferingHasProviderContactInformationFilter,
  ServiceOfferingHasRequiredMeasuresFilter,
  ServiceOfferingHasRoleAndResponsibilitiesFilter,
  ServiceOfferingHasSecurityIncidentManagementFilter,
  ServiceOfferingHasServicePolicyFilter,
  ServiceOfferingHasSubContractorDetailsFilter,
  ServiceOfferingHasUserDocumentationMaintenanceFilter,
  ServiceOfferingIsOperatedByGaiaXParticipantFilter,
  ServiceOfferingIssuerMatchesProviderIssuerFilter,
  ValidationFilter,
  VerifiableCredentialsAreNotExpiredFilter,
  VpRespectsShaclShapeFilter
} from '../filter'
import { RegistryService } from './registry.service'
import { ShaclService } from './shacl.service'
import { TrustedNotaryIssuerService } from './trusted-notary-issuer.service'

@Injectable({ scope: Scope.REQUEST })
export class VerifiablePresentationValidationService {
  private readonly serviceOfferingFilters: Record<ConformityLevelEnum, ValidationFilter[]>
  // TODO replace this by the use of ConfigService
  private readonly _driver = neo4j.driver(process.env.dburl || 'bolt://localhost:7687')
  private readonly logger: Logger = new Logger(VerifiablePresentationValidationService.name)
  private parentClasses: ParentClasses
  @InjectMetric('shacl_process_time')
  protected shaclProcessTimeSummary: Summary

  constructor(
    private readonly trustedNotaryIssuerService: TrustedNotaryIssuerService,
    private readonly shaclService: ShaclService,
    private readonly registryService: RegistryService
  ) {
    const verifiableCredentialsAreNotExpiredFilter: ValidationFilter = new VerifiableCredentialsAreNotExpiredFilter()
    const dataResourceIssuerMatchesProducerIssuerFilter: ValidationFilter = new DataResourceIssuerMatchesProducerIssuerFilter()
    const issuersHaveTermsAndConditionsFilter: ValidationFilter = new IssuersHaveTermsAndConditionsFilter()
    const registrationNumbersAreFromTrustedIssuersFilter: ValidationFilter = new RegistrationNumbersAreFromTrustedIssuersFilter(
      this.trustedNotaryIssuerService
    )
    const resourceHasConsentIfContainsPiiFilter: ValidationFilter = new ResourceHasConsentIfContainsPiiFilter()
    const serviceOfferingIssuerMatchesProviderIssuerFilter: ValidationFilter = new ServiceOfferingIssuerMatchesProviderIssuerFilter()
    const serviceOfferingHasAResourceWithAddressFilter: ValidationFilter = new ServiceOfferingHasAResourceWithAddressFilter()
    const serviceOfferingHasLegallyBindingActFilter: ValidationFilter = new ServiceOfferingHasLegallyBindingActFilter()
    const serviceOfferingHasDataUsageRightsForPartiesFilter: ValidationFilter = new ServiceOfferingHasDataUsageRightsForPartiesFilter()
    const legalDocumentHasInvolvedParties: ValidationFilter = new LegalDocumentHasInvolvedParties()
    const serviceOfferingHasRequiredMeasuresFilter: ValidationFilter = new ServiceOfferingHasRequiredMeasuresFilter()
    const serviceOfferingHasDocumentedChangeProceduresFilter: ValidationFilter = new ServiceOfferingHasDocumentedChangeProceduresFilter()
    const serviceOfferingHasRoleAndResponsibilitiesFilter: ValidationFilter = new ServiceOfferingHasRoleAndResponsibilitiesFilter()
    const serviceOfferingHasProviderContactInformationFilter: ValidationFilter = new ServiceOfferingHasProviderContactInformationFilter()
    const serviceOfferingHasCustomerInstructionsFilter: ValidationFilter = new ServiceOfferingHasCustomerInstructionsFilter()
    const serviceOfferingIsOperatedByGaiaXParticipantFilter: ValidationFilter = new ServiceOfferingIsOperatedByGaiaXParticipantFilter()
    const serviceOfferingHasDescribedDataTransfersFilter: ValidationFilter = new ServiceOfferingHasDescribedDataTransfersFilter()
    const serviceOfferingHasCustomerAuditingRightsFilter: ValidationFilter = new ServiceOfferingHasCustomerAuditingRightsFilter()
    const serviceOfferingHasAssetsManagementFilter: ValidationFilter = new ServiceOfferingHasAssetsManagementFilter()
    const serviceOfferingHasEmployeeResponsibilitiesFilter: ValidationFilter = new ServiceOfferingHasEmployeeResponsibilitiesFilter()
    const serviceOfferingHasInformationSecurityPoliciesFilter: ValidationFilter = new ServiceOfferingHasInformationSecurityPoliciesFilter()
    const serviceOfferingHasOperationalSecurityFilter: ValidationFilter = new ServiceOfferingHasOperationalSecurityFilter()
    const serviceOfferingHasAccessControlManagementFilter: ValidationFilter = new ServiceOfferingHasAccessControlManagementFilter()
    const serviceOfferingHasInformationSecurityOrganizationFilter: ValidationFilter = new ServiceOfferingHasInformationSecurityOrganizationFilter()
    const serviceOfferingHasInformationSecurityRiskManagementFilter: ValidationFilter =
      new ServiceOfferingHasInformationSecurityRiskManagementFilter()
    const serviceOfferingHasPhysicalSecurityFilter: ValidationFilter = new ServiceOfferingHasPhysicalSecurityFilter()
    const serviceOfferingHasCryptographicSecurityStandards: ValidationFilter = new ServiceOfferingHasCryptographicSecurityStandards()
    const serviceOfferingHasDataPortabilityFilter: ValidationFilter = new ServiceOfferingHasDataPortabilityFilter()
    const serviceOfferingHasDevelopmentCycleSecurityFilter: ValidationFilter = new ServiceOfferingHasDevelopmentCycleSecurityFilter()
    const serviceOfferingHasProcurementManagementSecurityFilter: ValidationFilter = new ServiceOfferingHasProcurementManagementSecurityFilter()
    const serviceOfferingHasSecurityIncidentManagementFilter: ValidationFilter = new ServiceOfferingHasSecurityIncidentManagementFilter()
    const serviceOfferingHasBusinessContinuityMeasuresFilter: ValidationFilter = new ServiceOfferingHasBusinessContinuityMeasuresFilter()
    const serviceOfferingHasComplianceAssuranceFilter: ValidationFilter = new ServiceOfferingHasComplianceAssuranceFilter()
    const serviceOfferingHasUserDocumentationMaintenanceFilter: ValidationFilter = new ServiceOfferingHasUserDocumentationMaintenanceFilter()
    const serviceOfferingHasGovernmentInvestigationManagementFilter: ValidationFilter =
      new ServiceOfferingHasGovernmentInvestigationManagementFilter()
    const serviceOfferingHasProductSecurityFilter: ValidationFilter = new ServiceOfferingHasProductSecurityFilter()
    const serviceOfferingHasChangeAndConfigurationManagementFilter: ValidationFilter = new ServiceOfferingHasChangeAndConfigurationManagementFilter()
    const serviceOfferingHasCustomerDataAccessTermsFilter: ValidationFilter = new ServiceOfferingHasCustomerDataAccessTermsFilter()
    const serviceOfferingHaveDataProtectionAndServiceAgreementFilter: ValidationFilter =
      new ServiceOfferingHasDataProtectionAndServiceAgreementFilter()
    const serviceOfferingHasSubContractorDetailsFilter: ValidationFilter = new ServiceOfferingHasSubContractorDetailsFilter()
    const serviceOfferingHasServicePolicyFilter: ValidationFilter = new ServiceOfferingHasServicePolicyFilter()
    const communicationsAreSecureFilter: ValidationFilter = new CommunicationsAreSecureFilter()

    this.serviceOfferingFilters = {
      [ConformityLevelEnum.STANDARD_COMPLIANCE]: [
        verifiableCredentialsAreNotExpiredFilter,
        dataResourceIssuerMatchesProducerIssuerFilter,
        issuersHaveTermsAndConditionsFilter,
        registrationNumbersAreFromTrustedIssuersFilter,
        resourceHasConsentIfContainsPiiFilter,
        serviceOfferingIssuerMatchesProviderIssuerFilter,
        serviceOfferingHasAResourceWithAddressFilter,
        serviceOfferingHasLegallyBindingActFilter,
        serviceOfferingHasDataUsageRightsForPartiesFilter,
        serviceOfferingHasDocumentedChangeProceduresFilter,
        legalDocumentHasInvolvedParties,
        serviceOfferingHasRequiredMeasuresFilter,
        serviceOfferingHasProviderContactInformationFilter,
        serviceOfferingHasRoleAndResponsibilitiesFilter,
        serviceOfferingHasAccessControlManagementFilter,
        serviceOfferingHasCustomerInstructionsFilter,
        serviceOfferingHasOperationalSecurityFilter,
        serviceOfferingIsOperatedByGaiaXParticipantFilter,
        serviceOfferingHasCustomerAuditingRightsFilter,
        serviceOfferingHasAssetsManagementFilter,
        serviceOfferingHasInformationSecurityOrganizationFilter,
        serviceOfferingHasEmployeeResponsibilitiesFilter,
        serviceOfferingHasInformationSecurityRiskManagementFilter,
        serviceOfferingHasInformationSecurityPoliciesFilter,
        serviceOfferingHasPhysicalSecurityFilter,
        serviceOfferingHasCryptographicSecurityStandards,
        serviceOfferingHasDataPortabilityFilter,
        serviceOfferingHasDevelopmentCycleSecurityFilter,
        serviceOfferingHasProcurementManagementSecurityFilter,
        serviceOfferingHasSecurityIncidentManagementFilter,
        serviceOfferingHasBusinessContinuityMeasuresFilter,
        serviceOfferingHasComplianceAssuranceFilter,
        serviceOfferingHasUserDocumentationMaintenanceFilter,
        serviceOfferingHasGovernmentInvestigationManagementFilter,
        serviceOfferingHasProductSecurityFilter,
        serviceOfferingHasChangeAndConfigurationManagementFilter,
        serviceOfferingHasCustomerDataAccessTermsFilter,
        serviceOfferingHasSubContractorDetailsFilter,
        communicationsAreSecureFilter
      ],
      [ConformityLevelEnum.LABEL_LEVEL_1]: [
        verifiableCredentialsAreNotExpiredFilter,
        dataResourceIssuerMatchesProducerIssuerFilter,
        issuersHaveTermsAndConditionsFilter,
        registrationNumbersAreFromTrustedIssuersFilter,
        resourceHasConsentIfContainsPiiFilter,
        serviceOfferingIssuerMatchesProviderIssuerFilter,
        serviceOfferingHasAResourceWithAddressFilter,
        serviceOfferingHasLegallyBindingActFilter,
        serviceOfferingHasDataUsageRightsForPartiesFilter,
        legalDocumentHasInvolvedParties,
        serviceOfferingHasRequiredMeasuresFilter,
        serviceOfferingHasDocumentedChangeProceduresFilter,
        serviceOfferingHasProviderContactInformationFilter,
        serviceOfferingHasRoleAndResponsibilitiesFilter,
        serviceOfferingHasAccessControlManagementFilter,
        serviceOfferingHasCustomerInstructionsFilter,
        serviceOfferingHasOperationalSecurityFilter,
        serviceOfferingIsOperatedByGaiaXParticipantFilter,
        serviceOfferingHasCustomerAuditingRightsFilter,
        serviceOfferingHasAssetsManagementFilter,
        serviceOfferingHasInformationSecurityOrganizationFilter,
        serviceOfferingHasEmployeeResponsibilitiesFilter,
        serviceOfferingHasInformationSecurityRiskManagementFilter,
        serviceOfferingHasDescribedDataTransfersFilter,
        serviceOfferingHasInformationSecurityPoliciesFilter,
        serviceOfferingHasPhysicalSecurityFilter,
        serviceOfferingHasCryptographicSecurityStandards,
        serviceOfferingHasDataPortabilityFilter,
        serviceOfferingHasDevelopmentCycleSecurityFilter,
        serviceOfferingHasProcurementManagementSecurityFilter,
        serviceOfferingHasSecurityIncidentManagementFilter,
        serviceOfferingHasBusinessContinuityMeasuresFilter,
        serviceOfferingHasComplianceAssuranceFilter,
        serviceOfferingHasUserDocumentationMaintenanceFilter,
        serviceOfferingHasGovernmentInvestigationManagementFilter,
        serviceOfferingHasProductSecurityFilter,
        serviceOfferingHasChangeAndConfigurationManagementFilter,
        serviceOfferingHasCustomerDataAccessTermsFilter,
        serviceOfferingHaveDataProtectionAndServiceAgreementFilter,
        serviceOfferingHasSubContractorDetailsFilter,
        serviceOfferingHasServicePolicyFilter,
        communicationsAreSecureFilter
      ],
      [ConformityLevelEnum.LABEL_LEVEL_2]: [
        verifiableCredentialsAreNotExpiredFilter,
        dataResourceIssuerMatchesProducerIssuerFilter,
        issuersHaveTermsAndConditionsFilter,
        registrationNumbersAreFromTrustedIssuersFilter,
        resourceHasConsentIfContainsPiiFilter,
        serviceOfferingIssuerMatchesProviderIssuerFilter,
        serviceOfferingHasAResourceWithAddressFilter,
        serviceOfferingHasLegallyBindingActFilter,
        serviceOfferingHasDataUsageRightsForPartiesFilter,
        legalDocumentHasInvolvedParties,
        serviceOfferingHasDataPortabilityFilter,
        serviceOfferingHasRequiredMeasuresFilter,
        serviceOfferingHasDocumentedChangeProceduresFilter,
        serviceOfferingHasProviderContactInformationFilter,
        serviceOfferingIsOperatedByGaiaXParticipantFilter,
        serviceOfferingHasSubContractorDetailsFilter,
        serviceOfferingHasServicePolicyFilter
      ],
      [ConformityLevelEnum.LABEL_LEVEL_3]: [
        verifiableCredentialsAreNotExpiredFilter,
        dataResourceIssuerMatchesProducerIssuerFilter,
        issuersHaveTermsAndConditionsFilter,
        registrationNumbersAreFromTrustedIssuersFilter,
        resourceHasConsentIfContainsPiiFilter,
        serviceOfferingIssuerMatchesProviderIssuerFilter,
        serviceOfferingHasAResourceWithAddressFilter,
        serviceOfferingHasLegallyBindingActFilter,
        serviceOfferingHasDataUsageRightsForPartiesFilter,
        legalDocumentHasInvolvedParties,
        serviceOfferingHasRequiredMeasuresFilter,
        serviceOfferingHasDocumentedChangeProceduresFilter,
        serviceOfferingHasProviderContactInformationFilter,
        serviceOfferingIsOperatedByGaiaXParticipantFilter,
        serviceOfferingHasSubContractorDetailsFilter,
        serviceOfferingHasServicePolicyFilter
      ]
    }
  }

  static getUUIDStartingWithALetter() {
    let uuid = uuidv4()
    while (!isNaN(uuid[0])) {
      uuid = uuidv4()
    }
    return uuid
  }

  async validate(verifiablePresentation: VerifiablePresentation, conformityLevel: ConformityLevelEnum): Promise<FilterValidationResult> {
    const contextVersion = this.shaclService.getContextVersion(verifiablePresentation)
    const vpUUID: string = VerifiablePresentationValidationService.getUUIDStartingWithALetter()

    this.logger.debug(`Starting asynchronous shacl validation`)
    const vpRespectsShaclShapeFilter: ValidationFilter = new VpRespectsShaclShapeFilter(this.shaclService, this.shaclProcessTimeSummary)
    const vpRespectsShaclShapeFilterPromise = vpRespectsShaclShapeFilter.doFilter(vpUUID, verifiablePresentation, this._driver, contextVersion)

    this.logger.debug(`Inserting in memgraph ${vpUUID}`)
    await this.insertVpInMemGraph(vpUUID, verifiablePresentation)
    this.logger.debug(`${vpUUID} Inserted in memgraph `)
    const filtersPromise = this.serviceOfferingFilters[conformityLevel].map(filter =>
      filter.doFilter(vpUUID, verifiablePresentation, this._driver, contextVersion)
    )
    const [shaclResult, filterResults] = await Promise.all([vpRespectsShaclShapeFilterPromise, Promise.all(filtersPromise)])
    await this.cleanupMemGraph(vpUUID)
    return mergeFilterResults(shaclResult, ...filterResults)
  }

  private async insertVpInMemGraph(vpUUID: string, verifiablePresentation: VerifiablePresentation): Promise<void> {
    const sw = new StopWatch('sw')
    sw.start('jsonld.toRDF')
    this.logger.debug(`Getting ${vpUUID} quads from jsonld for memgraph`)
    const quads: string = await jsonld.toRDF(verifiablePresentation, { format: 'application/n-quads' })
    const sanitizedQuads = quads.replace(/'/g, "\\'")
    sw.stop()
    this.logger.debug(`Got ${vpUUID} quads from jsonld for memgraph`)
    sw.start('getShapesURLFromContext')
    const ontologyUrl: string = this.shaclService.getShapesUrlFromContext(verifiablePresentation)
    sw.stop()
    sw.start('loadOntologyFromUrl')
    const ontologyTriples: string = await this.registryService.loadOntologyFromUrl(ontologyUrl)
    sw.stop()
    sw.start('extractEntityParentClasses')
    this.parentClasses = await RdfGraphUtils.extractEntityParentClasses(ontologyTriples)
    sw.stop()
    sw.start('quadsToQueries')
    const queries: string[] = RdfGraphUtils.quadsToQueries(vpUUID, sanitizedQuads, this.parentClasses)
    sw.stop()
    this.logger.debug(`Sending ${vpUUID} quads to memgraph with ${queries.length} queries ${new Date().getTime()}`)
    sw.start('Open session & begin Transaction')
    const session = this._driver.session()
    const transaction = await session.beginTransaction()
    sw.stop()
    sw.start('transaction run')
    try {
      const batchSize = 100
      for (let i = 0; i < queries.length; i += batchSize) {
        const batch = queries.slice(i, i + batchSize)
        try {
          Promise.all(batch.map(query => transaction.run(query)))
        } catch (error) {
          this.logger.error(`Error executing batch at index ${i}: ${error}`)
          throw error
        }
      }
      sw.stop()
      sw.start('transaction commit')
      await transaction.commit()
      sw.stop()
      this.logger.debug(`Successfully committed ${vpUUID} quads to memgraph`)
    } catch (error) {
      this.logger.error(`An error occurred while inserting the verifiable presentation in MemGraph: ${error}`)
      await transaction.rollback()
    } finally {
      sw.start('Closing transaction & session')
      await transaction.close()
      await session.close()
      sw.stop()
      sw.prettyPrint()
      this.logger.debug(`Sent ${vpUUID} quads to memgraph and closed the session ${new Date().getTime()}`)
    }
  }

  private async cleanupMemGraph(VPUUID: any) {
    const session = this._driver.session()
    try {
      await session.executeWrite(tx => tx.run(`MATCH (n) WHERE n.vpID="${VPUUID}" DETACH DELETE n`))
    } catch (e) {
      this.logger.warn(`An error occurred while removing VP ${VPUUID} from DB`, e)
    } finally {
      await session.close()
    }
  }
}
