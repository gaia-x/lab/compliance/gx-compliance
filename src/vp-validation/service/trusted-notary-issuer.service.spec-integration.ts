import { HttpModule, HttpService } from '@nestjs/axios'
import { ConfigService } from '@nestjs/config'
import { Test } from '@nestjs/testing'

import { AxiosResponse } from 'axios'
import { of, throwError } from 'rxjs'

import { ConfigServiceMock } from '../../tests/config.service.mock'
import { TrustedNotaryIssuerService } from './trusted-notary-issuer.service'

const responseIssuers: string[] = ['notary.trusted-issuer.com', 'notary.good-issuer.com']
let service: TrustedNotaryIssuerService
let httpService: HttpService

describe('TrustedNotaryIssuerService', () => {
  beforeAll(async () => {
    const moduleRef = await Test.createTestingModule({
      imports: [HttpModule],
      providers: [TrustedNotaryIssuerService, ConfigService]
    })
      .overrideProvider(ConfigService)
      .useValue(new ConfigServiceMock({ production: 'true', REGISTRY_URL: 'https://test-registry.eu' }))
      .compile()

    service = moduleRef.get<TrustedNotaryIssuerService>(TrustedNotaryIssuerService)
    httpService = moduleRef.get<HttpService>(HttpService)
  })

  beforeEach(() => {
    jest.resetAllMocks()
    service.clearTrustedNotaryIssuersCache()

    jest.spyOn(httpService, 'get').mockReturnValue(
      of({
        data: responseIssuers
      } as unknown as AxiosResponse)
    )
  })

  it('should retrieve trusted notary issuers from a registry', async () => {
    expect(await service.retrieveTrustedNotaryIssuers()).toEqual(responseIssuers)
    expect(httpService.get).toHaveBeenCalledWith('https://test-registry.eu/api/trusted-issuers/registration-notary')
  })

  it('should cache retrieved trusted notary issuers', async () => {
    for (let i = 0; i < 3; i++) {
      expect(await service.retrieveTrustedNotaryIssuers()).toEqual(responseIssuers)
      expect(httpService.get).toHaveBeenCalledTimes(1)
    }
  })

  it('should throw an exception when trusted notary issuer retrieval fails', () => {
    const registryError: Error = new Error('Registry error')
    jest.spyOn(httpService, 'get').mockReturnValue(throwError(() => registryError))

    expect(service.retrieveTrustedNotaryIssuers()).rejects.toThrow(registryError)
  })

  it('should clear cache', async () => {
    expect(await service.retrieveTrustedNotaryIssuers()).toEqual(responseIssuers)
    expect(httpService.get).toHaveBeenCalledTimes(1)

    service.clearTrustedNotaryIssuersCache()

    expect(await service.retrieveTrustedNotaryIssuers()).toEqual(responseIssuers)
    expect(httpService.get).toHaveBeenCalledTimes(2)
  })

  it('should return true if notary issuer is trusted', async () => {
    expect(await service.isTrusted(responseIssuers[0])).toBe(true)
    expect(await service.isTrusted(responseIssuers[1])).toBe(true)
  })

  it('should return false if notary issuer is not trusted', async () => {
    expect(await service.isTrusted('notary.malicious.com')).toBe(false)
  })
})

describe('TrustedNotaryIssuerService (development mode)', () => {
  beforeAll(async () => {
    const moduleRef = await Test.createTestingModule({
      imports: [HttpModule],
      providers: [TrustedNotaryIssuerService, ConfigService]
    })
      .overrideProvider(ConfigService)
      .useValue(new ConfigServiceMock({ production: 'false', REGISTRY_URL: 'https://test-registry.eu' }))
      .compile()

    service = moduleRef.get<TrustedNotaryIssuerService>(TrustedNotaryIssuerService)
    httpService = moduleRef.get<HttpService>(HttpService)
  })

  beforeEach(() => {
    jest.resetAllMocks()
    service.clearTrustedNotaryIssuersCache()

    jest.spyOn(httpService, 'get').mockReturnValue(
      of({
        data: responseIssuers
      } as unknown as AxiosResponse)
    )
  })

  it('should return true in development mode if notary issuer is not trusted', async () => {
    expect(await service.isTrusted('notary.unknown.com')).toBe(true)
  })
})
