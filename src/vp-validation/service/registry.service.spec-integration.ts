import { CACHE_MANAGER } from '@nestjs/cache-manager'
import { ConfigService } from '@nestjs/config'
import { Test, TestingModule } from '@nestjs/testing'

import { Cache } from 'cache-manager'
import { HttpHandler, HttpResponse, http } from 'msw'
import { SetupServerApi, setupServer } from 'msw/node'

import { OntologyException } from '../../common/exception/ontology.exception'
import { ShaclShapeException } from '../../common/exception/shacl-shape.exception'
import { ConfigServiceMock } from '../../tests/config.service.mock'
import { VpContentValidationModule } from '../vp-content-validation.module'
import { RegistryService } from './registry.service'

import mocked = jest.mocked

describe('RegistryService', () => {
  const shapesUrl = 'https://registry.example.org/shapes/latest'
  const shapesResponseText = `I'm a shape, trust me`

  const ontologyUrl = 'https://registry.example.org/owl/latest'
  const ontologyResponseText = `I'm an ontology, trust me`

  const errorResponseUrl = 'https://registry.example.org/fail'

  const cacheManagerMock: Cache = {
    get: jest.fn(),
    set: jest.fn()
  } as unknown as Cache

  let registryMock: SetupServerApi
  let service: RegistryService

  beforeAll(async () => {
    const handlers: HttpHandler[] = [
      http.get(shapesUrl, () => {
        return HttpResponse.text(shapesResponseText)
      }),
      http.get(ontologyUrl, () => {
        return HttpResponse.text(ontologyResponseText)
      }),
      http.get(errorResponseUrl, () => {
        return new HttpResponse(null, {
          status: 500,
          statusText: 'Server Exception'
        })
      })
    ]
    registryMock = setupServer(...handlers)
    registryMock.listen({ onUnhandledRequest: 'bypass' })

    const moduleFixture: TestingModule = await Test.createTestingModule({
      imports: [VpContentValidationModule]
    })
      .overrideProvider('PRIVATE_KEY')
      .useValue(null)
      .overrideProvider(CACHE_MANAGER)
      .useValue(cacheManagerMock)
      .overrideProvider(ConfigService)
      .useValue(new ConfigServiceMock({ BASE_URL: 'https://localhost', PRIVATE_KEY: process.env.PRIVATE_KEY }))
      .compile()

    service = await moduleFixture.resolve<RegistryService>(RegistryService)
  })

  afterEach(() => {
    jest.resetAllMocks()
  })

  afterAll(() => {
    registryMock.close()
  })

  describe('Load turtle shapes', () => {
    it('should load turtle shapes from shapes URL', async () => {
      mocked(cacheManagerMock.get).mockResolvedValue(null)
      mocked(cacheManagerMock.set).mockResolvedValue()

      expect(await service.loadTurtleShapesFromUrl(shapesUrl)).toEqual(shapesResponseText)

      expect(cacheManagerMock.get).toHaveBeenCalledWith(shapesUrl + 'shapes')
      expect(cacheManagerMock.set).toHaveBeenCalledWith(shapesUrl + 'shapes', shapesResponseText, 3_600_000)
    })

    it('should load turtle shapes from cache', async () => {
      mocked(cacheManagerMock.get).mockResolvedValue(shapesResponseText)

      expect(await service.loadTurtleShapesFromUrl(shapesUrl)).toEqual(shapesResponseText)

      expect(cacheManagerMock.get).toHaveBeenCalledWith(shapesUrl + 'shapes')
      expect(cacheManagerMock.set).not.toHaveBeenCalled()
    })

    it('should throw an exception when loading turtle shapes fails', () => {
      mocked(cacheManagerMock.get).mockResolvedValue(null)

      expect(service.loadTurtleShapesFromUrl(errorResponseUrl)).rejects.toThrow(
        new ShaclShapeException(`An error occurred while loading turtle shapes from ${errorResponseUrl}: Request failed with status code 500`)
      )
    })
  })

  describe('Load ontology', () => {
    it('should load ontology from URL', async () => {
      mocked(cacheManagerMock.get).mockResolvedValue(null)
      mocked(cacheManagerMock.set).mockResolvedValue()

      expect(await service.loadOntologyFromUrl(ontologyUrl)).toEqual(ontologyResponseText)

      expect(cacheManagerMock.get).toHaveBeenCalledWith(ontologyUrl + 'ontology')
      expect(cacheManagerMock.set).toHaveBeenCalledWith(ontologyUrl + 'ontology', ontologyResponseText, 600_000)
    })

    it('should load ontology from cache', async () => {
      mocked(cacheManagerMock.get).mockResolvedValue(ontologyResponseText)

      expect(await service.loadOntologyFromUrl(ontologyUrl)).toEqual(ontologyResponseText)

      expect(cacheManagerMock.get).toHaveBeenCalledWith(ontologyUrl + 'ontology')
      expect(cacheManagerMock.set).not.toHaveBeenCalled()
    })

    it('should throw an exception when loading ontology shapes fails', () => {
      mocked(cacheManagerMock.get).mockResolvedValue(null)

      expect(service.loadOntologyFromUrl(errorResponseUrl)).rejects.toThrow(
        new OntologyException(`An error occurred while loading the ontology from ${errorResponseUrl}: Request failed with status code 500`)
      )
    })
  })
})
