import { Module } from '@nestjs/common'

import { makeSummaryProvider } from '@willsoto/nestjs-prometheus'

import { CommonModule } from '../common/common.module'
import { RegistryService } from './service/registry.service'
import { ShaclService } from './service/shacl.service'
import { TrustedNotaryIssuerService } from './service/trusted-notary-issuer.service'
import { VerifiablePresentationValidationService } from './service/verifiable-presentation-validation.service'

@Module({
  imports: [CommonModule],
  providers: [
    VerifiablePresentationValidationService,
    TrustedNotaryIssuerService,
    ShaclService,
    RegistryService,
    makeSummaryProvider({
      name: 'shacl_process_time',
      help: 'Distribution of the SHACL validation time in seconds',
      labelNames: ['result']
    })
  ],
  exports: [VerifiablePresentationValidationService]
})
export class VpContentValidationModule {}
