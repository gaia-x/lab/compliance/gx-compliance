import { Logger } from '@nestjs/common'

import { Driver } from 'neo4j-driver'

import { VerifiablePresentation } from '../../common/constants'
import { errorMessageDocumentation } from '../../common/utils/error-handling.util'
import { FilterValidationResult, ValidationFilter } from './common/validation-filter.interface'

interface CustomerInstructionsProperties {
  type: string
  value: string
}

/**
 * Implementation of the Gaia-X
 * <a href="https://gaia-x.gitlab.io/policy-rules-committee/compliance-document/criterions/#P2.2.1">P2.2.1</a> &
 * <a href="https://gaia-x.gitlab.io/policy-rules-committee/compliance-document/criterions/#P2.2.2">P2.2.2</a>
 * labelling criterion</a>.
 */
export class ServiceOfferingHasCustomerInstructionsFilter implements ValidationFilter {
  private readonly logger: Logger = new Logger(ServiceOfferingHasCustomerInstructionsFilter.name)

  async doFilter(
    vpUUID: string,
    _verifiablePresentation: VerifiablePresentation,
    driver: Driver,
    contextVersion: string
  ): Promise<FilterValidationResult> {
    this.logger.debug(`Checking that service offerings have customer instructions for VPUUID ${vpUUID}...`)

    const query = `MATCH (:_https_w3id_org_gaia_x_${contextVersion}_ServiceOffering_)<-[:_http_www_w3_org_1999_02_22_rdf_syntax_ns_type_]-(serviceOffering)
        -[:_https_www_w3_org_2018_credentials_credentialSubject_]->(credentialSubject)
      WHERE serviceOffering.vpID="${vpUUID}"
      CALL {
        WITH credentialSubject
        OPTIONAL MATCH (credentialSubject)-[:_https_w3id_org_gaia_x_${contextVersion}_customerInstructions_]->()
          -[r*]->(property)
        RETURN COLLECT({ type: type(r[0]), value: property.value }) AS properties
      }
      RETURN serviceOffering.value as serviceOfferingId, properties`

    const session = driver.session()
    try {
      const results = await session.executeRead(tx => tx.run(query))
      let isP221Valid = true
      let isP222Valid = true
      const errorMessages: string[] = []
      for (const record of results.records) {
        const serviceOfferingId: string = record.get('serviceOfferingId')
        const properties: CustomerInstructionsProperties[] = record.get('properties')

        if (!properties.some(property => property.type === `_https_w3id_org_gaia_x_${contextVersion}_terms_`)) {
          this.logger.error(`P2.2.1 - Service offering ${serviceOfferingId} is missing customer instruction terms for VPUUID ${vpUUID}...`)
          errorMessages.push(`P2.2.1 - Service offering ${serviceOfferingId} is missing customer instruction terms`)
          isP221Valid = false
        }

        if (!properties.some(property => property.type === `_https_w3id_org_gaia_x_${contextVersion}_means_`)) {
          this.logger.error(`P2.2.2 - Service offering ${serviceOfferingId} is missing customer instruction means for VPUUID ${vpUUID}...`)
          errorMessages.push(`P2.2.2 - Service offering ${serviceOfferingId} is missing customer instruction means`)
          isP222Valid = false
        }
      }

      const validatedCriteria = []
      if (isP221Valid) {
        validatedCriteria.push('P2.2.1')
      } else {
        errorMessages.push(errorMessageDocumentation('P2.2.1'))
      }
      if (isP222Valid) {
        validatedCriteria.push('P2.2.2')
      } else {
        errorMessages.push(errorMessageDocumentation('P2.2.2'))
      }

      return {
        conforms: !errorMessages.length,
        results: errorMessages,
        validatedCriteria: !errorMessages.length ? validatedCriteria : ['']
      }
    } catch (error) {
      this.logger.error(
        `An error occurred while checking that service offerings have customer instructions for VPUUID ${vpUUID}. Error: ${error.message}`
      )

      return {
        conforms: false,
        results: [`An error occurred while checking that service offerings have customer instructions : ${error.message}`],
        validatedCriteria: []
      }
    } finally {
      await session.close()
    }
  }
}
