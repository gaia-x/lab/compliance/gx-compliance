import { Logger } from '@nestjs/common'

import { errorMessageDocumentation } from '../../common/utils/error-handling.util'
import { LegalDocumentFilter, ServiceOfferingLegalDocuments } from './common/legal-document.filter'
import { FilterValidationResult } from './common/validation-filter.interface'

/**
 * Implementation of the Gaia-X
 * <a href="https://gaia-x.gitlab.io/policy-rules-committee/compliance-document/criterions/#P2.2.7">P2.2.7 labelling criterion</a>.
 */
export class ServiceOfferingHasCustomerAuditingRightsFilter extends LegalDocumentFilter {
  constructor() {
    super(new Logger(ServiceOfferingHasCustomerAuditingRightsFilter.name))
  }

  verifyLegalDocuments(vpUUID: string, contextVersion: string, results: ServiceOfferingLegalDocuments[]): FilterValidationResult {
    this.logger.debug(`Checking that service offerings have customer auditing rights for VPUUID ${vpUUID}...`)

    const errorMessages: string[] = results
      .filter(
        result => !result.legalDocuments.some(legalDocument => legalDocument.type === `w3id.org/gaia-x/${contextVersion}#CustomerAuditingRights`)
      )
      .map(result => {
        this.logger.error(
          `P2.2.7 - Service offering ${result.serviceOfferingId} is missing customer auditing rights in its legal documents for VPUUID ${vpUUID}...`
        )
        return `P2.2.7 - Service offering ${result.serviceOfferingId} is missing customer auditing rights in its legal documents`
      })

    if (errorMessages.length) {
      errorMessages.push(errorMessageDocumentation('P2.2.7'))
    }

    return {
      conforms: !errorMessages.length,
      results: errorMessages,
      validatedCriteria: !errorMessages.length ? ['P2.2.7'] : []
    }
  }
}
