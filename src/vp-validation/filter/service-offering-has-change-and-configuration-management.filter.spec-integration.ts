import { Driver } from 'neo4j-driver'

import { ValidationResult } from '../../common/dto'
import { insertObjectInMemGraph } from '../../tests/memgraph-environment-setup'
import vpWithMissingChangeAndConfigurationManagementDocument from '../filter/fixtures/service-offering-has-change-and-configuration-management/vp-missing-change-and-configuration-management.json'
import vpWithValidServiceOffering from '../filter/fixtures/service-offering-has-change-and-configuration-management/vp-valid-change-and-configuration-management.json'
import { VerifiablePresentationValidationService } from '../service/verifiable-presentation-validation.service'
import { ValidationFilter } from './common/validation-filter.interface'
import { ServiceOfferingHasChangeAndConfigurationManagementFilter } from './service-offering-has-change-and-configuration-management.filter'

describe('ServiceOfferingHasChangeAndConfigurationManagementFilter', () => {
  let vpUuid: string
  const filter: ValidationFilter = new ServiceOfferingHasChangeAndConfigurationManagementFilter()

  beforeEach(() => {
    vpUuid = VerifiablePresentationValidationService.getUUIDStartingWithALetter()
  })

  it('should pass with a valid presentation', async () => {
    await insertObjectInMemGraph(global.memgraphDriver, vpUuid, vpWithValidServiceOffering)

    const result: ValidationResult = await filter.doFilter(vpUuid, null, global.memgraphDriver, 'development')

    expect(result.conforms).toBe(true)
    expect(result.results).toHaveLength(0)
  })

  it('should not pass when ChangeAndConfigurationManagement document is missing', async () => {
    await insertObjectInMemGraph(global.memgraphDriver, vpUuid, vpWithMissingChangeAndConfigurationManagementDocument)

    const result: ValidationResult = await filter.doFilter(vpUuid, null, global.memgraphDriver, 'development')

    expect(result.conforms).toBe(false)

    expect(result.results).toEqual([
      'P3.1.12 - Service offering <https://gaia-x.eu/service-offering1.json> is missing a Change And Configuration Management legal document',
      'P3.1.12 - Service offering <https://gaia-x.eu/service-offering.json> is missing a Change And Configuration Management legal document',
      'P3.1.12 - More details about this criterion here: https://gitlab.com/gaia-x/lab/compliance/gx-compliance/-/blob/development/docs/labelling-criteria.md#criterion-p3112'
    ])
  })

  it('should not pass when query fails', async () => {
    const driverMock: Driver = {
      session: () => ({
        executeRead: jest.fn().mockRejectedValue(new Error('Test error')),
        close: jest.fn()
      })
    } as unknown as Driver

    const result: ValidationResult = await filter.doFilter(vpUuid, null, driverMock, 'development')

    expect(result.conforms).toBe(false)
    expect(result.results).toEqual(['An unexpected error has occurred while collecting legal documents: Test error'])
  })
})
