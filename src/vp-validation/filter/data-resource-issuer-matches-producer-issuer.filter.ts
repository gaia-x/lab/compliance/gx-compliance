import { Logger } from '@nestjs/common'

import { Driver } from 'neo4j-driver'

import { VerifiablePresentation } from '../../common/constants'
import { FilterValidationResult, ValidationFilter } from './common/validation-filter.interface'

export class DataResourceIssuerMatchesProducerIssuerFilter implements ValidationFilter {
  private readonly logger: Logger = new Logger(DataResourceIssuerMatchesProducerIssuerFilter.name)

  async doFilter(
    vpUUID: string,
    _verifiablePresentation: VerifiablePresentation,
    driver: Driver,
    contextVersion: string
  ): Promise<FilterValidationResult> {
    this.logger.debug(`Checking that data resource issuer and producer issuer match for VPUUID ${vpUUID}...`)

    const query = `MATCH (dataResourceIssuer)<-[:_https_www_w3_org_2018_credentials_issuer_]-(dataResource)
        -[:_http_www_w3_org_1999_02_22_rdf_syntax_ns_type_]->(:_https_w3id_org_gaia_x_${contextVersion}_DataResource_)
      WHERE dataResourceIssuer.vpID="${vpUUID}" 
      CALL {
        WITH dataResource
        OPTIONAL MATCH (dataResource)-[:_https_www_w3_org_2018_credentials_credentialSubject_]->()
          -[:_https_w3id_org_gaia_x_${contextVersion}_producedBy_]->()
          <-[:_https_www_w3_org_2018_credentials_credentialSubject_]-()
          -[:_https_www_w3_org_2018_credentials_issuer_]->(participantIssuer)
        RETURN DISTINCT participantIssuer
      }
      RETURN dataResourceIssuer.value AS dataResourceIssuer, participantIssuer.value AS participantIssuer`

    const session = driver.session()
    try {
      const results = await session.executeRead(tx => tx.run(query))

      this.logger.debug(`Found ${results.records.length} data resources for VPUUID ${vpUUID}...`)
      const issuersMatch: boolean = results.records
        .map(record => record.get('dataResourceIssuer') === record.get('participantIssuer'))
        .reduce((a, b) => a && b, true)

      return {
        conforms: issuersMatch,
        results: issuersMatch ? [] : [`Data resource issuer and producer issuer do not match`],
        validatedCriteria: []
      }
    } catch (error) {
      this.logger.error(
        `An error occurred while checking that data resource issuer and producer issuer match for VPUUID ${vpUUID}. Error: ${error.message}`
      )

      return {
        conforms: false,
        results: [`An error occurred while checking that data resource issuer and producer issuer match: ${error.message}`],
        validatedCriteria: []
      }
    } finally {
      await session.close()
    }
  }
}
