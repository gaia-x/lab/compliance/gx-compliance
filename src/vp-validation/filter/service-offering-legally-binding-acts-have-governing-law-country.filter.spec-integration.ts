import { ValidationResult } from '../../common/dto'
import { insertObjectInMemGraph } from '../../tests/memgraph-environment-setup'
import { VerifiablePresentationValidationService } from '../service/verifiable-presentation-validation.service'
import { ValidationFilter } from './common/validation-filter.interface'
import vpWithServiceOfferingWithLegallyBindingActWithoutEEA from './fixtures/service-offering-legally-binding-acts-have-governing-law-country/vp-with-service-offering-with-legally-binding-act-without-EEA.json'
import vpWithServiceOfferingWithLegallyBindingActWithoutGoverningLaws from './fixtures/service-offering-legally-binding-acts-have-governing-law-country/vp-with-service-offering-with-legally-binding-act-without-governing-laws.json'
import vpWithValidServiceOffering from './fixtures/service-offering-legally-binding-acts-have-governing-law-country/vp-with-valid-service-offering.json'
import { ServiceOfferingLegallyBindingActsHaveGoverningLawCountry } from './service-offering-legally-binding-acts-have-governing-law.country'

describe('ServiceOfferingLegallyBindingActsHaveGoverningLawCountry', () => {
  let vpUuid: string
  const filter: ValidationFilter = new ServiceOfferingLegallyBindingActsHaveGoverningLawCountry()

  beforeEach(() => {
    vpUuid = VerifiablePresentationValidationService.getUUIDStartingWithALetter()
  })

  it('should be conform with at least a document having governingLawCountries containing an EEA country', async () => {
    await insertObjectInMemGraph(global.memgraphDriver, vpUuid, vpWithValidServiceOffering)
    const validationResult: ValidationResult = await filter.doFilter(vpUuid, null, global.memgraphDriver, 'development')

    expect(validationResult.conforms).toBe(true)
    expect(validationResult.results).toHaveLength(0)
  })

  it('should not be conform if no document contains a governing law country', async () => {
    await insertObjectInMemGraph(global.memgraphDriver, vpUuid, vpWithServiceOfferingWithLegallyBindingActWithoutGoverningLaws)
    const validationResult: ValidationResult = await filter.doFilter(vpUuid, null, global.memgraphDriver, 'development')

    expect(validationResult.conforms).toBe(false)
    expect(validationResult.results).toHaveLength(3)
    expect(validationResult.results).toContain(
      `P1.1.5 - Service offering <https://example.org/service-offering.json> does not have a governing law country for legally binding act https://example.org/service-offering/legally-binding-act-2.pdf`
    )
    expect(validationResult.results).toContain(
      `P1.1.5 - Service offering <https://example.org/service-offering.json> does not have a governing law country for legally binding act https://example.org/service-offering/legally-binding-act-3.pdf`
    )
    expect(validationResult.results).toContain(
      `P1.1.5 - More details about this criterion here: https://gitlab.com/gaia-x/lab/compliance/gx-compliance/-/blob/development/docs/labelling-criteria.md#criterion-p115`
    )
  })

  it('should not be conform if no document contains a governing law country in EEA', async () => {
    await insertObjectInMemGraph(global.memgraphDriver, vpUuid, vpWithServiceOfferingWithLegallyBindingActWithoutEEA)
    const validationResult: ValidationResult = await filter.doFilter(vpUuid, null, global.memgraphDriver, 'development')

    expect(validationResult.conforms).toBe(false)
    expect(validationResult.results).toEqual([
      `P1.1.2 - Service offering <https://example.org/service-offering.json> with legally binding act https://example.org/service-offering/legally-binding-act-2.pdf must have at least one EEA governing law country`,
      `P1.1.2 - Service offering <https://example.org/service-offering.json> with legally binding act https://example.org/service-offering/legally-binding-act.pdf must have at least one EEA governing law country`,
      'P1.1.2 - More details about this criterion here: https://gitlab.com/gaia-x/lab/compliance/gx-compliance/-/blob/development/docs/labelling-criteria.md#criterion-p112'
    ])
  })
})
