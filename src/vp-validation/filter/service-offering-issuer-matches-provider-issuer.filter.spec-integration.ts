import { Driver } from 'neo4j-driver'

import { ValidationResult } from '../../common/dto'
import { insertObjectInMemGraph } from '../../tests/memgraph-environment-setup'
import vpWithServiceOfferingIssuerMatchingProviderIssuer from '../filter/fixtures/service-offering-issuer-matches-provider-issuer/vp-with-service-offering-issuer-matching-provider-issuer.json'
import vpWithServiceOfferingIssuerNotMatchingProviderIssuer from '../filter/fixtures/service-offering-issuer-matches-provider-issuer/vp-with-service-offering-issuer-not-matching-provider-issuer.json'
import { VerifiablePresentationValidationService } from '../service/verifiable-presentation-validation.service'
import { ValidationFilter } from './common/validation-filter.interface'
import { ServiceOfferingIssuerMatchesProviderIssuerFilter } from './service-offering-issuer-matches-provider-issuer.filter'

describe('ServiceOfferingIssuerMatchesProviderIssuerFilter', () => {
  let vpUuid: string
  const filter: ValidationFilter = new ServiceOfferingIssuerMatchesProviderIssuerFilter()

  beforeEach(() => {
    vpUuid = VerifiablePresentationValidationService.getUUIDStartingWithALetter()
  })

  it('should return a positive conformity', async () => {
    await insertObjectInMemGraph(global.memgraphDriver, vpUuid, vpWithServiceOfferingIssuerMatchingProviderIssuer)

    const result: ValidationResult = await filter.doFilter(vpUuid, null, global.memgraphDriver, 'development')

    expect(result.conforms).toBe(true)
    expect(result.results).toHaveLength(0)
  })

  it('should return a negative conformity with a reason when service offering and provider issuer do not match', async () => {
    await insertObjectInMemGraph(global.memgraphDriver, vpUuid, vpWithServiceOfferingIssuerNotMatchingProviderIssuer)

    const result: ValidationResult = await filter.doFilter(vpUuid, null, global.memgraphDriver, 'development')

    expect(result.conforms).toBe(false)
    expect(result.results).toEqual(['Service offering issuer and provider issuer do not match'])
  })

  it('should return a positive conformity when no service offering is present', async () => {
    await insertObjectInMemGraph(global.memgraphDriver, vpUuid, {})

    const result: ValidationResult = await filter.doFilter(vpUuid, null, global.memgraphDriver, 'development')

    expect(result.conforms).toBe(true)
    expect(result.results).toHaveLength(0)
  })

  it('should return a negative conformity with a reason when the query fails', async () => {
    const driverMock: Driver = {
      session: () => ({
        executeRead: jest.fn().mockRejectedValue(new Error('Test error')),
        close: jest.fn()
      })
    } as unknown as Driver

    const result: ValidationResult = await filter.doFilter(vpUuid, null, driverMock, 'development')

    expect(result.conforms).toBe(false)
    expect(result.results).toEqual(['An error occurred while checking that service offering issuer and provider issuer match: Test error'])
  })
})
