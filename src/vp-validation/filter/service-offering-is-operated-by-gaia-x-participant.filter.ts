import { Logger } from '@nestjs/common'

import { Driver } from 'neo4j-driver'

import { VerifiablePresentation } from '../../common/constants'
import { errorMessageDocumentation } from '../../common/utils/error-handling.util'
import { FilterValidationResult, ValidationFilter } from './common/validation-filter.interface'

/**
 * Implementation of the Gaia-X
 * <a href="https://gaia-x.gitlab.io/policy-rules-committee/compliance-document/criterions/#P1.3.2">P1.3.2 labelling criterion</a>.
 *
 * Minimal checks are done in this class as the SHACL shape takes care of most data integrity verifications such as :
 * <ul>
 *   <li>{@link ServiceOffering#providedBy} must be a {@link LegalPerson}</li>
 *   <li>{@link ServiceOffering#headquartersAddress} must be provided</li>
 *   <li>{@link ServiceOffering#legalAddress} must be provided</li>
 *</ul>
 *
 * Checking that registration numbers are from trusted issuer is done by the
 * {@link RegistrationNumbersAreFromTrustedIssuersFilter}.
 */
export class ServiceOfferingIsOperatedByGaiaXParticipantFilter implements ValidationFilter {
  private readonly logger: Logger = new Logger(ServiceOfferingIsOperatedByGaiaXParticipantFilter.name)

  async doFilter(
    vpUUID: string,
    _verifiablePresentation: VerifiablePresentation,
    driver: Driver,
    contextVersion: string
  ): Promise<FilterValidationResult> {
    this.logger.debug(`Checking service offerings are operated by Gaia-X participant for VPUUID ${vpUUID}...`)

    const session = driver.session()
    const query = `MATCH (serviceOffering)-[:_http_www_w3_org_1999_02_22_rdf_syntax_ns_type_]->(:_https_w3id_org_gaia_x_${contextVersion}_ServiceOffering_)
      WHERE serviceOffering.vpID="${vpUUID}" 
      CALL {
        WITH serviceOffering
        OPTIONAL MATCH (serviceOffering)-[:_https_www_w3_org_2018_credentials_credentialSubject_]->()
          -[:_https_w3id_org_gaia_x_${contextVersion}_providedBy_]->()
          -[:_https_w3id_org_gaia_x_${contextVersion}_registrationNumber_]->(registrationNumber)
        RETURN registrationNumber
      }
      RETURN serviceOffering.value AS serviceOfferingId, registrationNumber;`

    try {
      const results = await session.executeRead(tx => tx.run(query))

      const errorMessages: string[] = []
      for (const record of results.records) {
        const serviceOfferingId: string = record.get('serviceOfferingId')

        if (!record.get('registrationNumber')) {
          this.logger.error(
            `P1.3.2 - Service offering ${serviceOfferingId} must have a trusted provider registration number issuer for VPUUID ${vpUUID}...`
          )
          errorMessages.push(`P1.3.2 - Service offering ${serviceOfferingId} must have a trusted provider registration number issuer`)
        }
      }

      if (errorMessages.length) {
        errorMessages.push(errorMessageDocumentation('P1.3.2'))
      }

      return {
        conforms: !errorMessages.length,
        results: errorMessages,
        validatedCriteria: !errorMessages.length ? ['P1.3.2'] : []
      }
    } catch (error) {
      this.logger.error(
        `P1.3.2 - An error occurred while checking service offerings are operated by Gaia-X participant for VPUUID ${vpUUID}. Error: ${error.message}`
      )

      return {
        conforms: false,
        results: [`P1.3.2 - An error occurred while checking service offerings are operated by Gaia-X participant: ${error.message}`],
        validatedCriteria: []
      }
    } finally {
      await session.close()
    }
  }
}
