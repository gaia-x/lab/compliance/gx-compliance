import { Logger } from '@nestjs/common'

import { errorMessageDocumentation } from '../../common/utils/error-handling.util'
import { LegalDocumentFilter, ServiceOfferingLegalDocuments } from './common/legal-document.filter'
import { FilterValidationResult } from './common/validation-filter.interface'

/**
 * Implementation of the Gaia-X
 * <a href="https://gaia-x.gitlab.io/policy-rules-committee/compliance-document/criterions/#P3.1.13">P3.1.13 labelling criterion</a>.
 */
export class ServiceOfferingHasDevelopmentCycleSecurityFilter extends LegalDocumentFilter {
  constructor() {
    super(new Logger(ServiceOfferingHasDevelopmentCycleSecurityFilter.name))
  }

  verifyLegalDocuments(vpUUID: string, contextVersion: string, results: ServiceOfferingLegalDocuments[]): FilterValidationResult {
    this.logger.debug(`Checking that service offerings have development cycle security for VPUUID ${vpUUID}...`)

    const errorMessages: string[] = results
      .filter(
        result => !result.legalDocuments.some(legalDocument => legalDocument.type === `w3id.org/gaia-x/${contextVersion}#DevelopmentCycleSecurity`)
      )
      .map(result => {
        this.logger.error(
          `P3.1.13 - Service offering ${result.serviceOfferingId} is missing development cycle security in its legal documents for VPUUID ${vpUUID}...`
        )
        return `P3.1.13 - Service offering ${result.serviceOfferingId} is missing development cycle security in its legal documents`
      })

    if (errorMessages.length) {
      errorMessages.push(errorMessageDocumentation('P3.1.13'))
    }

    return {
      conforms: !errorMessages.length,
      results: errorMessages,
      validatedCriteria: !errorMessages.length ? ['P3.1.13'] : ['']
    }
  }
}
