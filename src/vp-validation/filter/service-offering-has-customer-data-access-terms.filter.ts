import { Logger } from '@nestjs/common'

import { errorMessageDocumentation } from '../../common/utils/error-handling.util'
import { LegalDocumentFilter, ServiceOfferingLegalDocuments } from './common/legal-document.filter'
import { FilterValidationResult } from './common/validation-filter.interface'

/**
 * Implementation of the Gaia-X
 * <a href="https://gaia-x.gitlab.io/policy-rules-committee/compliance-document/criterions/#P5.2.1">P5.2.1 labelling criterion</a>.
 * <a href="https://gitlab.com/gaia-x/technical-committee/service-characteristics-working-group/service-characteristics/-/blame/develop/single-point-of-truth/legal-document.yaml?ref_type=heads#L48">CustomerDataAccessTerms Shacl definition</a>.
 */
export class ServiceOfferingHasCustomerDataAccessTermsFilter extends LegalDocumentFilter {
  constructor() {
    super(new Logger(ServiceOfferingHasCustomerDataAccessTermsFilter.name))
  }

  verifyLegalDocuments(vpUUID: string, contextVersion: string, results: ServiceOfferingLegalDocuments[]): FilterValidationResult {
    this.logger.debug(`Checking that service offerings have at least one Customer Data Access Terms legal document for VPUUID ${vpUUID}...`)

    const errorMessages: string[] = results
      .filter(
        result => !result.legalDocuments.some(legalDocument => legalDocument.type === `w3id.org/gaia-x/${contextVersion}#CustomerDataAccessTerms`)
      )
      .map(result => {
        this.logger.error(
          `P5.2.1 - Service offering ${result.serviceOfferingId} is missing a Customer Data Access Terms legal document for VPUUID ${vpUUID}...`
        )
        return `P5.2.1 - Service offering ${result.serviceOfferingId} is missing a Customer Data Access Terms legal document`
      })

    if (errorMessages.length) {
      errorMessages.push(errorMessageDocumentation('P5.2.1'))
    }

    return {
      conforms: !errorMessages.length,
      results: errorMessages,
      validatedCriteria: !errorMessages.length ? ['P5.2.1'] : ['']
    }
  }
}
