import { Logger } from '@nestjs/common'

import { Driver } from 'neo4j-driver'

import { VerifiablePresentation } from '../../common/constants'
import { errorMessageDocumentation } from '../../common/utils/error-handling.util'
import { FilterValidationResult, ValidationFilter } from './common/validation-filter.interface'

/**
 * Implementation of the Gaia-X
 * <a href="https://gaia-x.gitlab.io/policy-rules-committee/compliance-document/criterions/#P3.1.9">P3.1.9</a>
 */
export class ServiceOfferingHasCryptographicSecurityStandards implements ValidationFilter {
  private readonly logger: Logger = new Logger(ServiceOfferingHasCryptographicSecurityStandards.name)

  async doFilter(
    vpUUID: string,
    _verifiablePresentation: VerifiablePresentation,
    driver: Driver,
    contextVersion: string
  ): Promise<FilterValidationResult> {
    this.logger.debug(`Checking that service offerings have cryptographic security standards for VPUUID ${vpUUID}...`)

    const query = `MATCH (:_https_w3id_org_gaia_x_${contextVersion}_ServiceOffering_)<-[:_http_www_w3_org_1999_02_22_rdf_syntax_ns_type_]-(serviceOffering)
        -[:_https_www_w3_org_2018_credentials_credentialSubject_]->(credentialSubject)
      WHERE serviceOffering.vpID="${vpUUID}"
      CALL {
        WITH credentialSubject
        OPTIONAL MATCH (credentialSubject)-[:_https_w3id_org_gaia_x_${contextVersion}_cryptographicSecurityStandards_]->(standard)
        RETURN COLLECT(standard.value) AS standards
      }
      RETURN serviceOffering.value as serviceOfferingId, standards`

    const session = driver.session()
    try {
      const results = await session.executeRead(tx => tx.run(query))

      const errorMessages: string[] = []
      for (const record of results.records) {
        const serviceOfferingId: string = record.get('serviceOfferingId')
        const cryptographicSecurityStandards: string[] = record.get('standards')

        if (!cryptographicSecurityStandards.length) {
          this.logger.error(`P3.1.9 - Service offering ${serviceOfferingId} is missing cryptographic security standards for VPUUID ${vpUUID}...`)
          errorMessages.push(`P3.1.9 - Service offering ${serviceOfferingId} is missing cryptographic security standards`)
        }
      }

      if (errorMessages.length) {
        errorMessages.push(errorMessageDocumentation('P3.1.9'))
      }

      return {
        conforms: !errorMessages.length,
        results: errorMessages,
        validatedCriteria: !errorMessages.length ? ['P3.1.9'] : ['']
      }
    } catch (error) {
      this.logger.error(
        `An error occurred while checking that service offerings have cryptographic security standards for VPUUID ${vpUUID}. Error: ${error.message}`
      )

      return {
        conforms: false,
        results: [`An error occurred while checking that service offerings have cryptographic security standards : ${error.message}`],
        validatedCriteria: []
      }
    } finally {
      await session.close()
    }
  }
}
