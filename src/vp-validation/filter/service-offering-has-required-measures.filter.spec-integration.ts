import { Driver } from 'neo4j-driver'

import { ValidationResult } from '../../common/dto'
import { insertObjectInMemGraph } from '../../tests/memgraph-environment-setup'
import { VerifiablePresentationValidationService } from '../service/verifiable-presentation-validation.service'
import { ValidationFilter } from './common/validation-filter.interface'
import missingRequiredMeasures from './fixtures/service-offering-has-required-measures/vp-missing-required-measures.json'
import validVP from './fixtures/service-offering-has-required-measures/vp-valid-required-measures.json'
import { ServiceOfferingHasRequiredMeasuresFilter } from './service-offering-has-required-measures.filter'

describe('ServiceOfferingHasRequiredMeasuresFilter', () => {
  let vpUuid: string
  const filter: ValidationFilter = new ServiceOfferingHasRequiredMeasuresFilter()

  beforeEach(() => {
    vpUuid = VerifiablePresentationValidationService.getUUIDStartingWithALetter()
  })

  it('should pass with valid required measures', async () => {
    await insertObjectInMemGraph(global.memgraphDriver, vpUuid, validVP)

    const result: ValidationResult = await filter.doFilter(vpUuid, null, global.memgraphDriver, 'development')

    expect(result).toBeDefined()
    expect(result.conforms).toBeTruthy()
    expect(result.results).toHaveLength(0)
  })

  it('should not pass with missing required measures', async () => {
    await insertObjectInMemGraph(global.memgraphDriver, vpUuid, missingRequiredMeasures)

    const result: ValidationResult = await filter.doFilter(vpUuid, null, global.memgraphDriver, 'development')

    expect(result).toBeDefined()
    expect(result.conforms).toBeFalsy()
    expect(result.results).toHaveLength(3)
    expect(result.results).toContain('P2.1.3 - Service Offering <https://gaia-x.eu/service-offering1.json> must define requiredMeasures.')
    expect(result.results).toContain('P2.1.3 - Service Offering <https://gaia-x.eu/service-offering2.json> must define requiredMeasures.')
    expect(result.results).toContain(
      'P2.1.3 - More details about this criterion here: https://gitlab.com/gaia-x/lab/compliance/gx-compliance/-/blob/development/docs/labelling-criteria.md#criterion-p213'
    )
  })

  it('should not pass when query fails', async () => {
    const driverMock: Driver = {
      session: () => ({
        executeRead: jest.fn().mockRejectedValue(new Error('Test error')),
        close: jest.fn()
      })
    } as unknown as Driver

    const result: ValidationResult = await filter.doFilter(vpUuid, null, driverMock, 'development')

    expect(result.conforms).toBe(false)
    expect(result.results).toEqual(['An error occurred while checking P2.1.3: Test error'])
  })
})
