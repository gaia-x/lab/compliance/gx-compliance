import { Logger } from '@nestjs/common'

import { errorMessageDocumentation } from '../../common/utils/error-handling.util'
import { LegalDocumentFilter, ServiceOfferingLegalDocuments } from './common/legal-document.filter'
import { FilterValidationResult } from './common/validation-filter.interface'

/**
 * Implementation of the Gaia-X
 * <a href="https://gaia-x.gitlab.io/policy-rules-committee/compliance-document/criterions/#P3.1.8">P3.1.8 labelling criterion</a>.
 *  * <a href="https://gitlab.com/gaia-x/technical-committee/service-characteristics-working-group/service-characteristics/-/blame/develop/single-point-of-truth/legal-document.yaml?ref_type=heads#L63">AccessControlManagement Shacl definition</a>.
 */
export class ServiceOfferingHasAccessControlManagementFilter extends LegalDocumentFilter {
  constructor() {
    super(new Logger(ServiceOfferingHasAccessControlManagementFilter.name))
  }

  verifyLegalDocuments(vpUUID: string, contextVersion: string, results: ServiceOfferingLegalDocuments[]): FilterValidationResult {
    this.logger.debug(`Checking that service offerings have at least one Access Control Management legal document for VPUUID ${vpUUID}...`)

    const errorMessages: string[] = results
      .filter(
        result => !result.legalDocuments.some(legalDocument => legalDocument.type === `w3id.org/gaia-x/${contextVersion}#AccessControlManagement`)
      )
      .map(result => {
        this.logger.error(
          `P3.1.8 - Service offering ${result.serviceOfferingId} is missing an Access Control Management legal document for VPUUID ${vpUUID}...`
        )
        return `P3.1.8 - Service offering ${result.serviceOfferingId} is missing an Access Control Management legal document`
      })

    if (errorMessages.length) {
      errorMessages.push(errorMessageDocumentation('P3.1.8'))
    }

    return {
      conforms: !errorMessages.length,
      results: errorMessages,
      validatedCriteria: !errorMessages.length ? ['P3.1.8'] : []
    }
  }
}
