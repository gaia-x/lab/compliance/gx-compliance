import { Logger } from '@nestjs/common'

import { errorMessageDocumentation } from '../../common/utils/error-handling.util'
import { LegalDocumentFilter, ServiceOfferingLegalDocuments } from './common/legal-document.filter'
import { FilterValidationResult } from './common/validation-filter.interface'

/**
 * Implementation of the Gaia-X
 * <a href="https://gaia-x.gitlab.io/policy-rules-committee/compliance-document/criterions/#P1.1.1">P1.1.1 labelling criterion</a>.
 */
export class ServiceOfferingHasLegallyBindingActFilter extends LegalDocumentFilter {
  constructor() {
    super(new Logger(ServiceOfferingHasLegallyBindingActFilter.name))
  }

  verifyLegalDocuments(vpUUID: string, contextVersion: string, results: ServiceOfferingLegalDocuments[]): FilterValidationResult {
    this.logger.debug(`Checking service offering has legally binding act for VPUUID ${vpUUID}...`)

    const errorMessages: string[] = []
    for (const result of results) {
      if (
        result.legalDocuments.length < 1 ||
        !result.legalDocuments.some(legalDocument => legalDocument.type === `w3id.org/gaia-x/${contextVersion}#LegallyBindingAct`)
      ) {
        this.logger.error(`P1.1.1 - No legally binding act is provided for the service offering ${result.serviceOfferingId} for VPUUID ${vpUUID}...`)
        errorMessages.push(`P1.1.1 - No legally binding act is provided for the service offering ${result.serviceOfferingId}`)
      }
    }

    if (errorMessages.length) {
      errorMessages.push(errorMessageDocumentation('P1.1.1'))
    }

    return {
      conforms: !errorMessages.length,
      results: errorMessages,
      validatedCriteria: !errorMessages.length ? ['P1.1.1'] : []
    }
  }
}
