import { Driver } from 'neo4j-driver'

import { ValidationResult } from '../../common/dto'
import { insertObjectInMemGraph } from '../../tests/memgraph-environment-setup'
import vpWithInsecureCommunications from '../filter/fixtures/communications-are-secure-filter/vp-with-insecure-communications.json'
import vpWithInternetExchangePoint from '../filter/fixtures/communications-are-secure-filter/vp-with-internet-exchange-point.json'
import vpWithPointOfPresence from '../filter/fixtures/communications-are-secure-filter/vp-with-point-of-presence.json'
import { VerifiablePresentationValidationService } from '../service/verifiable-presentation-validation.service'
import { ValidationFilter } from './common/validation-filter.interface'
import { CommunicationsAreSecureFilter } from './communications-are-secure.filter'

describe('CommunicationsAreSecureFilter', () => {
  let vpUuid: string
  const filter: ValidationFilter = new CommunicationsAreSecureFilter()

  beforeEach(() => {
    vpUuid = VerifiablePresentationValidationService.getUUIDStartingWithALetter()
  })

  it('should return a positive conformity', async () => {
    await insertObjectInMemGraph(global.memgraphDriver, vpUuid, vpWithInternetExchangePoint)

    const secondVpUuid: string = VerifiablePresentationValidationService.getUUIDStartingWithALetter()
    await insertObjectInMemGraph(global.memgraphDriver, secondVpUuid, vpWithPointOfPresence)

    let result: ValidationResult = await filter.doFilter(vpUuid, null, global.memgraphDriver, 'development')

    expect(result.conforms).toBe(true)
    expect(result.results).toHaveLength(0)

    result = await filter.doFilter(secondVpUuid, null, global.memgraphDriver, 'development')

    expect(result.conforms).toBe(true)
    expect(result.results).toHaveLength(0)
  })

  it('should return a negative conformity with a reason when communications are not secure', async () => {
    await insertObjectInMemGraph(global.memgraphDriver, vpUuid, vpWithInsecureCommunications)

    const result: ValidationResult = await filter.doFilter(vpUuid, null, global.memgraphDriver, 'development')

    expect(result.conforms).toBe(false)
    expect(result.results).toEqual([
      'P3.1.10 - Service offering <https://example.org/so-without-point-of-presence.json#cs> is missing a point of presence description',
      'P3.1.10 - Service offering <https://example.org/so-without-datacenter.json#cs> is missing a point of presence description',
      'P3.1.10 - Service offering <https://example.org/so-without-datacenter.json#cs> is missing a datacenter description',
      'P3.1.10 - More details about this criterion here: https://gitlab.com/gaia-x/lab/compliance/gx-compliance/-/blob/development/docs/labelling-criteria.md#criterion-p3110'
    ])
  })

  it('should return a negative conformity with a reason when the query fails', async () => {
    const driverMock: Driver = {
      session: () => ({
        executeRead: jest.fn().mockRejectedValue(new Error('Test error')),
        close: jest.fn()
      })
    } as unknown as Driver

    const result: ValidationResult = await filter.doFilter(vpUuid, null, driverMock, 'development')

    expect(result.conforms).toBe(false)
    expect(result.results).toEqual(['An error occurred while checking that communications are secure: Test error'])
  })
})
