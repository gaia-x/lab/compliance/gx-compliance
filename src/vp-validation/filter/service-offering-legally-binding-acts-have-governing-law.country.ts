import { Logger } from '@nestjs/common'

import { EEA_COUNTRY_NAME_ALPHA2 } from '../../common/constants'
import { LegalDocument } from '../../common/model/legal-document'
import { errorMessageDocumentation } from '../../common/utils/error-handling.util'
import { LegalDocumentFilter, ServiceOfferingLegalDocuments } from './common/legal-document.filter'
import { FilterValidationResult } from './common/validation-filter.interface'

/**
 * Implementation of the Gaia-X
 * <a href="https://gaia-x.gitlab.io/policy-rules-committee/compliance-document/criterions/#P1.1.2">P1.1.2 labelling criterion</a>.
 *
 * This class also implements the Gaia-X
 * <a href="https://gaia-x.gitlab.io/policy-rules-committee/compliance-document/criterions/#P1.1.5">P1.1.5 labelling criterion</a>.
 */
export class ServiceOfferingLegallyBindingActsHaveGoverningLawCountry extends LegalDocumentFilter {
  constructor() {
    super(new Logger(ServiceOfferingLegallyBindingActsHaveGoverningLawCountry.name))
  }

  verifyLegalDocuments(vpUUID: string, contextVersion: string, results: ServiceOfferingLegalDocuments[]): FilterValidationResult {
    this.logger.debug(`Checking that service offerings have legally binding acts that can be governed by EEA for VPUUID ${vpUUID}...`)

    const errorMessages: string[] = []
    let isP115Valid = true
    let isP112Valid = true
    for (const result of results) {
      const legallyBindingActs: LegalDocument[] = result.legalDocuments.filter(
        legalDocument => legalDocument.type === `w3id.org/gaia-x/${contextVersion}#LegallyBindingAct`
      )

      for (const legallyBindingAct of legallyBindingActs) {
        if (!legallyBindingAct.governingLawCountries.length) {
          this.logger.error(
            `P1.1.5 - Service offering ${result.serviceOfferingId} does not have a governing law country for legally binding act ${legallyBindingAct.url} for VPUUID ${vpUUID}...`
          )
          errorMessages.push(
            `P1.1.5 - Service offering ${result.serviceOfferingId} does not have a governing law country for legally binding act ${legallyBindingAct.url}`
          )
          isP115Valid = false
        } else if (!legallyBindingAct.governingLawCountries.some(governingLawCountry => EEA_COUNTRY_NAME_ALPHA2.includes(governingLawCountry))) {
          this.logger.error(
            `P1.1.2 - Service offering ${result.serviceOfferingId} with legally binding act ${legallyBindingAct.url} must have at least one EEA governing law country for VPUUID ${vpUUID}...`
          )
          errorMessages.push(
            `P1.1.2 - Service offering ${result.serviceOfferingId} with legally binding act ${legallyBindingAct.url} must have at least one EEA governing law country`
          )
          isP112Valid = false
        }
      }
    }
    const validatedCriteria = []
    if (isP115Valid) {
      validatedCriteria.push('P1.1.5')
    } else {
      errorMessages.push(errorMessageDocumentation('P1.1.5'))
    }
    if (isP112Valid) {
      validatedCriteria.push('P1.1.2')
    } else {
      errorMessages.push(errorMessageDocumentation('P1.1.2'))
    }
    return {
      conforms: !errorMessages.length,
      results: errorMessages,
      validatedCriteria
    }
  }
}
