import { Driver } from 'neo4j-driver'

import { ValidationResult } from '../../common/dto'
import { insertObjectInMemGraph } from '../../tests/memgraph-environment-setup'
import vpWithInvalidServiceOfferings from '../filter/fixtures/service-offering-has-provider-contact-information-filter/vp-with-invalid-service-offerings.json'
import vpWithValidServiceOfferings from '../filter/fixtures/service-offering-has-provider-contact-information-filter/vp-with-valid-service-offerings.json'
import { VerifiablePresentationValidationService } from '../service/verifiable-presentation-validation.service'
import { ValidationFilter } from './common/validation-filter.interface'
import { ServiceOfferingHasProviderContactInformationFilter } from './service-offering-has-provider-contact-information.filter'

describe('ServiceOfferingHasProviderContactInformationFilter', () => {
  let vpUuid: string
  const filter: ValidationFilter = new ServiceOfferingHasProviderContactInformationFilter()

  beforeEach(() => {
    vpUuid = VerifiablePresentationValidationService.getUUIDStartingWithALetter()
  })

  it('should return a positive conformity', async () => {
    await insertObjectInMemGraph(global.memgraphDriver, vpUuid, vpWithValidServiceOfferings)

    const result: ValidationResult = await filter.doFilter(vpUuid, null, global.memgraphDriver, 'development')

    expect(result.conforms).toBe(true)
    expect(result.results).toHaveLength(0)
  })

  it('should return a negative conformity with a reason when provider contact information is missing', async () => {
    await insertObjectInMemGraph(global.memgraphDriver, vpUuid, vpWithInvalidServiceOfferings)

    const result: ValidationResult = await filter.doFilter(vpUuid, null, global.memgraphDriver, 'development')

    expect(result.conforms).toBe(false)
    expect(result.results).toEqual([
      `P1.2.8 - Service offering <https://example.org/invalid-service-offering.json> is missing provider contact information`,
      'P1.2.8 - More details about this criterion here: https://gitlab.com/gaia-x/lab/compliance/gx-compliance/-/blob/development/docs/labelling-criteria.md#criterion-p128'
    ])
  })

  it('should return a negative conformity with a reason when the query fails', async () => {
    const driverMock: Driver = {
      session: () => ({
        executeRead: jest.fn().mockRejectedValue(new Error('Test error')),
        close: jest.fn()
      })
    } as unknown as Driver

    const result: ValidationResult = await filter.doFilter(vpUuid, null, driverMock, 'development')

    expect(result.conforms).toBe(false)
    expect(result.results).toEqual(['An error occurred while checking that the service offering has provider contact information: Test error'])
  })
})
