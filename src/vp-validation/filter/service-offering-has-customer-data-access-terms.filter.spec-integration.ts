import { Driver } from 'neo4j-driver'

import { ValidationResult } from '../../common/dto'
import { insertObjectInMemGraph } from '../../tests/memgraph-environment-setup'
import vpWithMissingCustomerDataAccessTermsDocument from '../filter/fixtures/service-offering-has-customer-data-access-terms/vp-missing-customer-data-access-terms.json'
import vpWithValidServiceOffering from '../filter/fixtures/service-offering-has-customer-data-access-terms/vp-valid-customer-data-access-terms.json'
import { VerifiablePresentationValidationService } from '../service/verifiable-presentation-validation.service'
import { ValidationFilter } from './common/validation-filter.interface'
import { ServiceOfferingHasCustomerDataAccessTermsFilter } from './service-offering-has-customer-data-access-terms.filter'

describe('ServiceOfferingHasCustomerDataAccessTermsFilter', () => {
  let vpUuid: string
  const filter: ValidationFilter = new ServiceOfferingHasCustomerDataAccessTermsFilter()

  beforeEach(() => {
    vpUuid = VerifiablePresentationValidationService.getUUIDStartingWithALetter()
  })

  it('should pass with a valid presentation', async () => {
    await insertObjectInMemGraph(global.memgraphDriver, vpUuid, vpWithValidServiceOffering)

    const result: ValidationResult = await filter.doFilter(vpUuid, null, global.memgraphDriver, 'development')

    expect(result.conforms).toBe(true)
    expect(result.results).toHaveLength(0)
  })

  it('should not pass when CustomerDataAccessTerms document is missing', async () => {
    await insertObjectInMemGraph(global.memgraphDriver, vpUuid, vpWithMissingCustomerDataAccessTermsDocument)

    const result: ValidationResult = await filter.doFilter(vpUuid, null, global.memgraphDriver, 'development')

    expect(result.conforms).toBe(false)

    expect(result.results).toEqual([
      'P5.2.1 - Service offering <https://gaia-x.eu/service-offering1.json> is missing a Customer Data Access Terms legal document',
      'P5.2.1 - Service offering <https://gaia-x.eu/service-offering.json> is missing a Customer Data Access Terms legal document',
      'P5.2.1 - More details about this criterion here: https://gitlab.com/gaia-x/lab/compliance/gx-compliance/-/blob/development/docs/labelling-criteria.md#criterion-p521'
    ])
  })

  it('should not pass when query fails', async () => {
    const driverMock: Driver = {
      session: () => ({
        executeRead: jest.fn().mockRejectedValue(new Error('Test error')),
        close: jest.fn()
      })
    } as unknown as Driver

    const result: ValidationResult = await filter.doFilter(vpUuid, null, driverMock, 'development')

    expect(result.conforms).toBe(false)
    expect(result.results).toEqual(['An unexpected error has occurred while collecting legal documents: Test error'])
  })
})
