import { Logger } from '@nestjs/common'

import { DateTime } from 'luxon'
import { Driver } from 'neo4j-driver'

import { VerifiablePresentation } from '../../common/constants'
import { ValidationResult } from '../../common/dto'
import { FilterValidationResult, ValidationFilter } from './common/validation-filter.interface'

export class VerifiableCredentialsAreNotExpiredFilter implements ValidationFilter {
  private readonly logger: Logger = new Logger(VerifiableCredentialsAreNotExpiredFilter.name)

  // eslint-disable-next-line @typescript-eslint/no-unused-vars
  async doFilter(vpUUID: string, verifiablePresentation: VerifiablePresentation, _driver: Driver): Promise<FilterValidationResult> {
    this.logger.debug(`Checking that verifiable credentials are not expired for VPUUID ${vpUUID}...`)

    const validationResult: ValidationResult = new ValidationResult()
    verifiablePresentation.verifiableCredential.forEach(vc => {
      if (DateTime.fromISO(vc.validFrom) > DateTime.now()) {
        this.logger.error(`VC ${vc.id} validFrom ${vc.validFrom} is in the future`)

        validationResult.conforms = false
        validationResult.results.push(`VC ${vc.id} validFrom ${vc.validFrom} is in the future`)
      }
      if (vc.validUntil) {
        if (DateTime.fromISO(vc.validUntil) < DateTime.now()) {
          this.logger.error(`VC ${vc.id} validUntil ${vc.validUntil} is in the past`)

          validationResult.conforms = false
          validationResult.results.push(`VC ${vc.id} validUntil ${vc.validUntil} is in the past`)
        }
      }
    })

    return {
      ...validationResult,
      validatedCriteria: []
    }
  }
}
