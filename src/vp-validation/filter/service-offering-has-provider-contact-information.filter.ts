import { Logger } from '@nestjs/common'

import { Driver } from 'neo4j-driver'

import { VerifiablePresentation } from '../../common/constants'
import { errorMessageDocumentation } from '../../common/utils/error-handling.util'
import { FilterValidationResult, ValidationFilter } from './common/validation-filter.interface'

/**
 * Implementation of the Gaia-X
 * <a href="https://gaia-x.gitlab.io/policy-rules-committee/compliance-document/criterions/#P1.2.8">P1.2.8 labelling criterion</a>.
 */
export class ServiceOfferingHasProviderContactInformationFilter implements ValidationFilter {
  private readonly logger: Logger = new Logger(ServiceOfferingHasProviderContactInformationFilter.name)

  async doFilter(
    vpUUID: string,
    _verifiablePresentation: VerifiablePresentation,
    driver: Driver,
    contextVersion: string
  ): Promise<FilterValidationResult> {
    this.logger.debug(`Checking that the service offering has provider contact information for VPUUID ${vpUUID}...`)

    const query = `MATCH (credentialSubject)<-[:_https_www_w3_org_2018_credentials_credentialSubject_]-(serviceOffering)
        -[:_http_www_w3_org_1999_02_22_rdf_syntax_ns_type_]->(:_https_w3id_org_gaia_x_${contextVersion}_ServiceOffering_)
      WHERE serviceOffering.vpID="${vpUUID}" 
      CALL {
        WITH credentialSubject
        OPTIONAL MATCH (credentialSubject)-[:_https_w3id_org_gaia_x_${contextVersion}_providerContactInformation_]->()
        -->(properties)
        RETURN properties
      }
      RETURN serviceOffering.value AS serviceOfferingId, COLLECT(properties) AS properties`

    const session = driver.session()
    try {
      const results = await session.executeRead(tx => tx.run(query))

      this.logger.debug(`Found ${results.records.length} service offerings for VPUUID ${vpUUID}...`)

      const errorMessages: string[] = []
      for (const record of results.records) {
        const serviceOfferingId: string = record.get('serviceOfferingId')

        if (!record.get('properties').length) {
          this.logger.error(`P1.2.8 - Service offering ${serviceOfferingId} is missing provider contact information for VPUUID ${vpUUID}...`)
          errorMessages.push(`P1.2.8 - Service offering ${serviceOfferingId} is missing provider contact information`)
        }
      }

      if (errorMessages.length) {
        errorMessages.push(errorMessageDocumentation('P1.2.8'))
      }

      return {
        conforms: !errorMessages.length,
        results: errorMessages,
        validatedCriteria: !errorMessages.length ? ['P1.2.8'] : []
      }
    } catch (error) {
      this.logger.error(
        `An error occurred while checking that the service offering has provider contact information for VPUUID ${vpUUID}. Error: ${error.message}`
      )

      return {
        conforms: false,
        results: [`An error occurred while checking that the service offering has provider contact information: ${error.message}`],
        validatedCriteria: []
      }
    } finally {
      await session.close()
    }
  }
}
