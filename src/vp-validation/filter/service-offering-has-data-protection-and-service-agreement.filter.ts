import { Logger } from '@nestjs/common'

import { errorMessageDocumentation } from '../../common/utils/error-handling.util'
import { LegalDocumentFilter, ServiceOfferingLegalDocuments } from './common/legal-document.filter'
import { FilterValidationResult } from './common/validation-filter.interface'

/**
 * Implementation of the Gaia-X
 * <a href="https://gaia-x.gitlab.io/policy-rules-committee/compliance-document/criterions/#P2.1.1">P2.1.1 labelling criterion</a>.
 * <a href="https://gitlab.com/gaia-x/technical-committee/service-characteristics-working-group/service-characteristics/-/blame/develop/single-point-of-truth/legal-document.yaml?ref_type=heads#L114">DataProtectionRegulationMeasures and ServiceAgreementOffer Shacl definition</a>.
 */
export class ServiceOfferingHasDataProtectionAndServiceAgreementFilter extends LegalDocumentFilter {
  constructor() {
    super(new Logger(ServiceOfferingHasDataProtectionAndServiceAgreementFilter.name))
  }

  verifyLegalDocuments(vpUUID: string, contextVersion: string, results: ServiceOfferingLegalDocuments[]): FilterValidationResult {
    this.logger.debug(
      `Checking that service offerings have at least one DataProtectionRegulationMeasures and ServiceAgreementOffer legal documents for VPUUID ${vpUUID}...`
    )

    const errorMessages: string[] = ['DataProtectionRegulationMeasures', 'ServiceAgreementOffer'].flatMap(requiredDocument =>
      results
        .filter(
          result => !result.legalDocuments.some(legalDocument => legalDocument.type === `w3id.org/gaia-x/${contextVersion}#${requiredDocument}`)
        )
        .map(result => {
          this.logger.error(
            `P2.1.1 - Service offering ${result.serviceOfferingId} is missing a ${requiredDocument} legal document for VPUUID ${vpUUID}...`
          )
          return `P2.1.1 - Service offering ${result.serviceOfferingId} is missing a ${requiredDocument} legal document`
        })
    )

    if (errorMessages.length) {
      errorMessages.push(errorMessageDocumentation('P2.1.1'))
    }

    return {
      conforms: !errorMessages.length,
      results: errorMessages,
      validatedCriteria: !errorMessages.length ? ['P2.1.1'] : ['']
    }
  }
}
