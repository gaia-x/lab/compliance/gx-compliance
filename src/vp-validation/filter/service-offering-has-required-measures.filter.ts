import { Logger } from '@nestjs/common'

import { Driver, Session } from 'neo4j-driver'

import { VerifiablePresentation } from '../../common/constants'
import { errorMessageDocumentation } from '../../common/utils/error-handling.util'
import { FilterValidationResult, ValidationFilter } from './common/validation-filter.interface'

/**
 * Implementation of the Gaia-X
 * <a href="https://gaia-x.gitlab.io/policy-rules-committee/compliance-document/criterions/#P2.1.3">P2.1.3 labelling criterion</a>.
 */
export class ServiceOfferingHasRequiredMeasuresFilter implements ValidationFilter {
  private readonly logger: Logger = new Logger(ServiceOfferingHasRequiredMeasuresFilter.name)

  async doFilter(
    vpUUID: string,
    _verifiablePresentation: VerifiablePresentation,
    driver: Driver,
    contextVersion: string
  ): Promise<FilterValidationResult> {
    this.logger.debug(`Checking that service offering has at least one requiredMeasures for VPUUID ${vpUUID}...`)

    const query = `MATCH (:_https_w3id_org_gaia_x_${contextVersion}_ServiceOffering_)<-[:_http_www_w3_org_1999_02_22_rdf_syntax_ns_type_]-(serviceOffering)
      WHERE serviceOffering.vpID="${vpUUID}"
      CALL {
        WITH serviceOffering
        OPTIONAL MATCH (serviceOffering)-[:_https_www_w3_org_2018_credentials_credentialSubject_]->(credentialSubject)
        -[:_https_w3id_org_gaia_x_${contextVersion}_requiredMeasures_]->(requiredMeasures)
        RETURN requiredMeasures
      }
      RETURN serviceOffering.value as serviceOfferingId, COUNT(requiredMeasures) AS requiredMeasuresCount, requiredMeasures`

    const session: Session = driver.session()

    try {
      const results = await session.executeRead(tx => tx.run(query))
      const errorMessages: string[] = []
      for (const record of results.records) {
        const serviceOfferingId: string = record.get('serviceOfferingId')
        const requiredMeasuresCount = record.get('requiredMeasuresCount').toNumber()
        const requiredMeasures = record.get('requiredMeasures')

        if (requiredMeasuresCount < 1 || !requiredMeasures) {
          errorMessages.push(`P2.1.3 - Service Offering ${serviceOfferingId} must define requiredMeasures.`)
        }
      }

      if (errorMessages.length) {
        errorMessages.push(errorMessageDocumentation('P2.1.3'))
      }

      return {
        conforms: !errorMessages.length,
        results: errorMessages,
        validatedCriteria: !errorMessages.length ? ['P2.1.3'] : []
      }
    } catch (error) {
      this.logger.error(`An error occurred while checking P2.1.3 for VPUUID ${vpUUID}. Error: ${error.message}`)

      return {
        conforms: false,
        results: [`An error occurred while checking P2.1.3: ${error.message}`],
        validatedCriteria: []
      }
    } finally {
      await session.close()
    }
  }
}
