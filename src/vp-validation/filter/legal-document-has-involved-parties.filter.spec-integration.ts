import { Driver } from 'neo4j-driver'

import { ValidationResult } from '../../common/dto'
import { insertObjectInMemGraph } from '../../tests/memgraph-environment-setup'
import { VerifiablePresentationValidationService } from '../service/verifiable-presentation-validation.service'
import { ValidationFilter } from './common/validation-filter.interface'
import mismatchingInvolvedPartiesVP from './fixtures/legal-document-has-involved-parties/vp-mismatching-involved-parties.json'
import missingInvolvedPartiesVP from './fixtures/legal-document-has-involved-parties/vp-missing-involved-parties.json'
import validVP from './fixtures/legal-document-has-involved-parties/vp-valid-involved-parties.json'
import { LegalDocumentHasInvolvedParties } from './legal-document-has-involved-parties.filter'

describe('LegalDocumentHasInvolvedParties', () => {
  let vpUuid: string
  const filter: ValidationFilter = new LegalDocumentHasInvolvedParties()

  beforeEach(() => {
    vpUuid = VerifiablePresentationValidationService.getUUIDStartingWithALetter()
  })

  it('should pass with valid involved parties and corresponding legalPerson', async () => {
    await insertObjectInMemGraph(global.memgraphDriver, vpUuid, validVP)

    const result: ValidationResult = await filter.doFilter(vpUuid, null, global.memgraphDriver, 'development')

    expect(result.conforms).toBe(true)
    expect(result.results).toHaveLength(0)
  })

  it('should not pass with missing involved parties values', async () => {
    await insertObjectInMemGraph(global.memgraphDriver, vpUuid, missingInvolvedPartiesVP)

    const result: ValidationResult = await filter.doFilter(vpUuid, null, global.memgraphDriver, 'development')

    expect(result.conforms).toBe(false)
    expect(result.results).toEqual([
      `P1.1.3 - Legally binding act of service offering <https://gaia-x.eu/service-offering.json> is missing involved parties`,
      'P1.1.3 - More details about this criterion here: https://gitlab.com/gaia-x/lab/compliance/gx-compliance/-/blob/development/docs/labelling-criteria.md#criterion-p113'
    ])
  })

  it('should not pass with valid involved parties and missing corresponding legalPerson', async () => {
    await insertObjectInMemGraph(global.memgraphDriver, vpUuid, mismatchingInvolvedPartiesVP)

    const result: ValidationResult = await filter.doFilter(vpUuid, null, global.memgraphDriver, 'development')

    expect(result.conforms).toBe(false)
    expect(result.results).toEqual([
      `P1.1.3 - Legally binding act of service offering <https://gaia-x.eu/service-offering.json> is referencing an involved party <https://example.org/participant-2.json#cs> that does not match any legal person`,
      `P1.1.3 - Legally binding act of service offering <https://gaia-x.eu/service-offering.json> is referencing an involved party <https://example.org/participant.json#cs> that does not match any legal person`,
      'P1.1.3 - More details about this criterion here: https://gitlab.com/gaia-x/lab/compliance/gx-compliance/-/blob/development/docs/labelling-criteria.md#criterion-p113'
    ])
  })

  it('should not pass when query fails', async () => {
    const driverMock: Driver = {
      session: () => ({
        executeRead: jest.fn().mockRejectedValue(new Error('Test error')),
        close: jest.fn()
      })
    } as unknown as Driver

    const result: ValidationResult = await filter.doFilter(vpUuid, null, driverMock, 'development')

    expect(result.conforms).toBe(false)
    expect(result.results).toEqual(['An error occurred while checking P1.1.3: Test error'])
  })
})
