import { Driver } from 'neo4j-driver'

import { VerifiablePresentation } from '../../../common/constants'

export interface ValidationFilter {
  doFilter(vpUUID: string, verifiablePresentation: VerifiablePresentation, driver: Driver, contextVersion?: string): Promise<FilterValidationResult>
}

export type FilterValidationResult = {
  conforms: boolean
  results: string[]
  validatedCriteria: string[]
}
