import { Logger } from '@nestjs/common'

import { Driver } from 'neo4j-driver'

import { PROTECTED_THIRD_COUNTRY_CODES, VerifiablePresentation } from '../../common/constants'
import { errorMessageDocumentation } from '../../common/utils/error-handling.util'
import { FilterValidationResult, ValidationFilter } from './common/validation-filter.interface'

/**
 * Implementation of the Gaia-X
 * <a href="https://gaia-x.gitlab.io/policy-rules-committee/compliance-document/criterions/#P2.2.4">P2.2.4 labelling criterion</a></li>
 */
export class ServiceOfferingHasDescribedDataTransfersFilter implements ValidationFilter {
  private readonly logger: Logger = new Logger(ServiceOfferingHasDescribedDataTransfersFilter.name)

  async doFilter(
    vpUUID: string,
    _verifiablePresentation: VerifiablePresentation,
    driver: Driver,
    contextVersion: string
  ): Promise<FilterValidationResult> {
    this.logger.debug(`Checking that the service offering has described possible personal data transfers for VPUUID ${vpUUID}...`)

    const query = `MATCH (credentialSubject)<-[:_https_www_w3_org_2018_credentials_credentialSubject_]-(serviceOffering)
        -[:_http_www_w3_org_1999_02_22_rdf_syntax_ns_type_]->(:_https_w3id_org_gaia_x_${contextVersion}_ServiceOffering_)
      WHERE serviceOffering.vpID="${vpUUID}" 
      CALL {
        WITH credentialSubject
        MATCH (credentialSubject)-[:_https_w3id_org_gaia_x_${contextVersion}_possiblePersonalDataTransfers_]->(dataTransfer)
          -[:_http_www_w3_org_1999_02_22_rdf_syntax_ns_type_]->(:_https_w3id_org_gaia_x_${contextVersion}_ThirdCountryDataTransfer_)
        OPTIONAL MATCH (dataTransfer)-[:_https_w3id_org_gaia_x_${contextVersion}_country_]->(country)
        RETURN COLLECT(country.value) AS countries
      }
      RETURN serviceOffering.value AS serviceOfferingId, countries`

    const session = driver.session()
    try {
      const results = await session.executeRead(tx => tx.run(query))

      this.logger.debug(`Found ${results.records.length} service offerings for VPUUID ${vpUUID}...`)

      const errorMessages: string[] = []
      for (const record of results.records) {
        const serviceOfferingId: string = record.get('serviceOfferingId')

        for (const country of record.get('countries')) {
          if (!PROTECTED_THIRD_COUNTRY_CODES.includes(country)) {
            this.logger.error(
              `P2.2.4 - Service offering ${serviceOfferingId} might transfer data to an unsecure third country (according to GDPR's Chapter V) with code ${country} for VPUUID ${vpUUID}...`
            )
            errorMessages.push(
              `P2.2.4 - Service offering ${serviceOfferingId} might transfer data to an unsecure third country (according to GDPR's Chapter V) with code ${country}`
            )
          }
        }
      }

      if (errorMessages.length) {
        errorMessages.push(errorMessageDocumentation('P2.2.4'))
      }

      return {
        conforms: !errorMessages.length,
        results: errorMessages,
        validatedCriteria: !errorMessages.length ? ['P2.2.4'] : ['']
      }
    } catch (error) {
      this.logger.error(
        `An error occurred while checking that the service offering has described possible personal data transfers for VPUUID ${vpUUID}. Error: ${error.message}`
      )

      return {
        conforms: false,
        results: [`An error occurred while checking that the service offering has described possible personal data transfers: ${error.message}`],
        validatedCriteria: []
      }
    } finally {
      await session.close()
    }
  }
}
