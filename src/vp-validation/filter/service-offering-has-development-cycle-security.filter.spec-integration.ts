import { Driver } from 'neo4j-driver'

import { ValidationResult } from '../../common/dto'
import { insertObjectInMemGraph } from '../../tests/memgraph-environment-setup'
import vpWithInvalidServiceOfferings from '../filter/fixtures/service-offering-has-development-cycle-security-filter/vp-with-invalid-service-offerings.json'
import vpWithValidServiceOffering from '../filter/fixtures/service-offering-has-development-cycle-security-filter/vp-with-valid-service-offering.json'
import { VerifiablePresentationValidationService } from '../service/verifiable-presentation-validation.service'
import { ValidationFilter } from './common/validation-filter.interface'
import { ServiceOfferingHasDevelopmentCycleSecurityFilter } from './service-offering-has-development-cycle-security.filter'

describe('ServiceOfferingHasDevelopmentCycleSecurityFilter', () => {
  let vpUuid: string
  const filter: ValidationFilter = new ServiceOfferingHasDevelopmentCycleSecurityFilter()

  beforeEach(() => {
    vpUuid = VerifiablePresentationValidationService.getUUIDStartingWithALetter()
  })

  it('should return a positive conformity', async () => {
    await insertObjectInMemGraph(global.memgraphDriver, vpUuid, vpWithValidServiceOffering)

    const result: ValidationResult = await filter.doFilter(vpUuid, null, global.memgraphDriver, 'development')

    expect(result.conforms).toBe(true)
    expect(result.results).toHaveLength(0)
  })

  it('should return a negative conformity with a reason when development cycle security are missing from service offerings', async () => {
    await insertObjectInMemGraph(global.memgraphDriver, vpUuid, vpWithInvalidServiceOfferings)

    const result: ValidationResult = await filter.doFilter(vpUuid, null, global.memgraphDriver, 'development')

    expect(result.conforms).toBe(false)

    expect(result.results).toEqual([
      'P3.1.13 - Service offering <https://example.org/invalid-service-offering-2.json> is missing development cycle security in its legal documents',
      'P3.1.13 - Service offering <https://example.org/invalid-service-offering-1.json> is missing development cycle security in its legal documents',
      'P3.1.13 - More details about this criterion here: https://gitlab.com/gaia-x/lab/compliance/gx-compliance/-/blob/development/docs/labelling-criteria.md#criterion-p3113'
    ])
  })

  it('should return a negative conformity with a reason when the query fails', async () => {
    const driverMock: Driver = {
      session: () => ({
        executeRead: jest.fn().mockRejectedValue(new Error('Test error')),
        close: jest.fn()
      })
    } as unknown as Driver

    const result: ValidationResult = await filter.doFilter(vpUuid, null, driverMock, 'development')

    expect(result.conforms).toBe(false)
    expect(result.results).toEqual(['An unexpected error has occurred while collecting legal documents: Test error'])
  })
})
