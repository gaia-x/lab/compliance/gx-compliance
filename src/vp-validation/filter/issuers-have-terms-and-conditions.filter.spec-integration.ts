import { Driver } from 'neo4j-driver'

import { ValidationResult } from '../../common/dto'
import { insertObjectInMemGraph } from '../../tests/memgraph-environment-setup'
import vpWithIssuersAndOneMissingTermsAndConditions from '../filter/fixtures/issuers-have-terms-and-conditions-filter/vp-with-issuers-and-one-missing-terms-and-conditions.json'
import vpWithIssuersWithInvalidTermsAndConditions from '../filter/fixtures/issuers-have-terms-and-conditions-filter/vp-with-issuers-with-invalid-terms-and-conditions.json'
import vpWithIssuersWithTermsAndConditions from '../filter/fixtures/issuers-have-terms-and-conditions-filter/vp-with-issuers-with-terms-and-conditions.json'
import { VerifiablePresentationValidationService } from '../service/verifiable-presentation-validation.service'
import { ValidationFilter } from './common/validation-filter.interface'
import { IssuersHaveTermsAndConditionsFilter } from './issuers-have-terms-and-conditions.filter'

describe('IssuersHaveTermsAndConditionsFilter', () => {
  let vpUuid: string
  let filter: ValidationFilter

  beforeEach(() => {
    jest.resetAllMocks()

    filter = new IssuersHaveTermsAndConditionsFilter()
    vpUuid = VerifiablePresentationValidationService.getUUIDStartingWithALetter()
  })

  it('should return a positive conformity', async () => {
    await insertObjectInMemGraph(global.memgraphDriver, vpUuid, vpWithIssuersWithTermsAndConditions)
    const result: ValidationResult = await filter.doFilter(vpUuid, null, global.memgraphDriver, 'development')

    expect(result.conforms).toBe(true)
    expect(result.results).toHaveLength(0)
  })

  it('should return a negative conformity with reason when an issuer does not have terms and conditions', async () => {
    await insertObjectInMemGraph(global.memgraphDriver, vpUuid, vpWithIssuersAndOneMissingTermsAndConditions)

    const result: ValidationResult = await filter.doFilter(vpUuid, null, global.memgraphDriver, 'development')

    expect(result.conforms).toBe(false)
    expect(result.results).toEqual(['The issuer: second-issuer.com is missing a gx:Issuer entity with terms and conditions'])
  })

  it('should return a negative conformity with reason when an issuer has invalid terms and conditions', async () => {
    await insertObjectInMemGraph(global.memgraphDriver, vpUuid, vpWithIssuersWithInvalidTermsAndConditions)

    const result: ValidationResult = await filter.doFilter(vpUuid, null, global.memgraphDriver, 'development')

    expect(result.conforms).toBe(false)
    expect(result.results).toEqual([
      'The issuer: first-trusted-issuer.com has an incorrect Gaia-X terms and conditions hash. Please follow https://docs.gaia-x.eu/ontology/development/enums/GaiaXTermsAndConditions for more details.',
      'The issuer: unused-trusted-issuer.com has an incorrect Gaia-X terms and conditions hash. Please follow https://docs.gaia-x.eu/ontology/development/enums/GaiaXTermsAndConditions for more details.'
    ])
  })

  it('should return a negative conformity with a reason when the query fails', async () => {
    const driverMock: Driver = {
      session: () => ({
        executeRead: jest.fn().mockRejectedValue(new Error('Test error')),
        close: jest.fn()
      })
    } as unknown as Driver

    const result: ValidationResult = await filter.doFilter(vpUuid, null, driverMock, 'development')

    expect(result.conforms).toBe(false)
    expect(result.results).toEqual(['An error occurred while checking that each issuer has a valid Terms And Conditions: Test error'])
  })
})
