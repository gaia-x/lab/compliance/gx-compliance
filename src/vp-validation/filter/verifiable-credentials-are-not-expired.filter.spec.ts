import { DateTime } from 'luxon'

import { VerifiablePresentation } from '../../common/constants'
import { ValidationResult } from '../../common/dto'
import { VerifiablePresentationValidationService } from '../service/verifiable-presentation-validation.service'
import { ValidationFilter } from './common/validation-filter.interface'
import vpWithNonExpiredVcs from './fixtures/verifiable-credentials-are-not-expired-filter/vp-with-non-expired-vcs.json'
import { VerifiableCredentialsAreNotExpiredFilter } from './verifiable-credentials-are-not-expired.filter'

describe('VerifiableCredentialsAreNotExpiredFilter', () => {
  let vpUuid: string
  const filter: ValidationFilter = new VerifiableCredentialsAreNotExpiredFilter()

  beforeEach(() => {
    vpUuid = VerifiablePresentationValidationService.getUUIDStartingWithALetter()
  })

  it('should return positive conformity when verifiable credentials are not expired', async () => {
    const vp: VerifiablePresentation = structuredClone(vpWithNonExpiredVcs)

    const validationResult: ValidationResult = await filter.doFilter(vpUuid, vp, null, 'development')

    expect(validationResult.conforms).toBe(true)
    expect(validationResult.results).toHaveLength(0)
  })

  it('should return negative conformity when a verifiable credential is not yet valid', async () => {
    const vp: VerifiablePresentation = structuredClone(vpWithNonExpiredVcs)
    vp.verifiableCredential[1].validFrom = DateTime.now().plus({ day: 5 }).toISO()

    const validationResult: ValidationResult = await filter.doFilter(vpUuid, vp, null, 'development')

    expect(validationResult.conforms).toBe(false)
    expect(validationResult.results).toEqual([
      `VC ${vp.verifiableCredential[1].id} validFrom ${vp.verifiableCredential[1].validFrom} is in the future`
    ])
  })

  it('should return negative conformity when a verifiable credential is expired', async () => {
    const vp: VerifiablePresentation = structuredClone(vpWithNonExpiredVcs)
    vp.verifiableCredential[2].validUntil = DateTime.now().minus({ day: 3 }).toISO()

    const validationResult: ValidationResult = await filter.doFilter(vpUuid, vp, null, 'development')

    expect(validationResult.conforms).toBe(false)
    expect(validationResult.results).toEqual([
      `VC ${vp.verifiableCredential[2].id} validUntil ${vp.verifiableCredential[2].validUntil} is in the past`
    ])
  })
})
