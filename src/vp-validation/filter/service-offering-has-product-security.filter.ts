import { Logger } from '@nestjs/common'

import { errorMessageDocumentation } from '../../common/utils/error-handling.util'
import { LegalDocumentFilter, ServiceOfferingLegalDocuments } from './common/legal-document.filter'
import { FilterValidationResult } from './common/validation-filter.interface'

/**
 * Implementation of the Gaia-X
 * <a href="https://gaia-x.gitlab.io/policy-rules-committee/compliance-document/criterions/#P3.1.20>P3.1.20 labelling criterion</a>.
 */
export class ServiceOfferingHasProductSecurityFilter extends LegalDocumentFilter {
  constructor() {
    super(new Logger(ServiceOfferingHasProductSecurityFilter.name))
  }

  verifyLegalDocuments(vpUUID: string, contextVersion: string, results: ServiceOfferingLegalDocuments[]): FilterValidationResult {
    this.logger.debug(`Checking that service offerings have product security for VPUUID ${vpUUID}...`)

    const errorMessages: string[] = results
      .filter(result => !result.legalDocuments.some(legalDocument => legalDocument.type === `w3id.org/gaia-x/${contextVersion}#ProductSecurity`))
      .map(result => {
        this.logger.error(
          `P3.1.20 - Service offering ${result.serviceOfferingId} is missing product security in its legal documents for VPUUID ${vpUUID}...`
        )
        return `P3.1.20 - Service offering ${result.serviceOfferingId} is missing product security in its legal documents`
      })

    if (errorMessages.length) {
      errorMessages.push(errorMessageDocumentation('P3.1.20'))
    }

    return {
      conforms: !errorMessages.length,
      results: errorMessages,
      validatedCriteria: !errorMessages.length ? ['P3.1.20'] : []
    }
  }
}
