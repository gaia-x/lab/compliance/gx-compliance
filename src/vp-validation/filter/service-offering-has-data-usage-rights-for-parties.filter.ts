import { Logger } from '@nestjs/common'

import { errorMessageDocumentation } from '../../common/utils/error-handling.util'
import { LegalDocumentFilter, ServiceOfferingLegalDocuments } from './common/legal-document.filter'
import { FilterValidationResult } from './common/validation-filter.interface'

/**
 * Implementation of the Gaia-X
 * <a href="https://gaia-x.gitlab.io/policy-rules-committee/compliance-document/criterions/#P1.2.2">P1.2.2 labelling criterion</a>.
 */
export class ServiceOfferingHasDataUsageRightsForPartiesFilter extends LegalDocumentFilter {
  constructor() {
    super(new Logger(ServiceOfferingHasDataUsageRightsForPartiesFilter.name))
  }

  verifyLegalDocuments(vpUUID: string, contextVersion: string, results: ServiceOfferingLegalDocuments[]): FilterValidationResult {
    this.logger.debug(`Checking that service offerings have data usage rights of parties for VPUUID ${vpUUID}...`)

    const errorMessages: string[] = []
    for (const result of results) {
      if (!result.legalDocuments.some(legalDocument => legalDocument.type === `w3id.org/gaia-x/${contextVersion}#LegallyBindingAct`)) {
        this.logger.error(
          `P1.2.2 - Service offering ${result.serviceOfferingId} is missing a legally binding act in its legal documents for VPUUID ${vpUUID}...`
        )
        errorMessages.push(`P1.2.2 - Service offering ${result.serviceOfferingId} is missing a legally binding act in its legal documents`)
      }

      if (!result.legalDocuments.some(legalDocument => legalDocument.type === `w3id.org/gaia-x/${contextVersion}#CustomerDataProcessingTerms`)) {
        this.logger.error(
          `P1.2.2 - Service offering ${result.serviceOfferingId} is missing customer data processing terms in its legal documents for VPUUID ${vpUUID}...`
        )
        errorMessages.push(`P1.2.2 - Service offering ${result.serviceOfferingId} is missing customer data processing terms in its legal documents`)
      }

      if (!result.legalDocuments.some(legalDocument => legalDocument.type === `w3id.org/gaia-x/${contextVersion}#CustomerDataAccessTerms`)) {
        this.logger.error(
          `P1.2.2 - Service offering ${result.serviceOfferingId} is missing customer data access terms in its legal documents for VPUUID ${vpUUID}...`
        )
        errorMessages.push(`P1.2.2 - Service offering ${result.serviceOfferingId} is missing customer data access terms in its legal documents`)
      }
    }

    if (errorMessages.length) {
      errorMessages.push(errorMessageDocumentation('P1.2.2'))
    }

    return {
      conforms: !errorMessages.length,
      results: errorMessages,
      validatedCriteria: !errorMessages.length ? ['P1.2.2'] : []
    }
  }
}
