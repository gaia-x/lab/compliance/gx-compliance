import { Driver } from 'neo4j-driver'

import { ValidationResult } from '../../common/dto'
import { insertObjectInMemGraph } from '../../tests/memgraph-environment-setup'
import { VerifiablePresentationValidationService } from '../service/verifiable-presentation-validation.service'
import { ValidationFilter } from './common/validation-filter.interface'
import vpWithInvalidServiceOfferings from './fixtures/service-offering-has-cryptographic-security-standards-filter/vp-with-invalid-service-offerings.json'
import vpWithValidServiceOffering from './fixtures/service-offering-has-cryptographic-security-standards-filter/vp-with-valid-service-offering.json'
import { ServiceOfferingHasCryptographicSecurityStandards } from './service-offering-has-cryptographic-security-standards.filter'

describe('ServiceOfferingHasCryptographicSecurityStandards', () => {
  let vpUuid: string
  const filter: ValidationFilter = new ServiceOfferingHasCryptographicSecurityStandards()

  beforeEach(() => {
    vpUuid = VerifiablePresentationValidationService.getUUIDStartingWithALetter()
  })

  it('should return a positive conformity', async () => {
    await insertObjectInMemGraph(global.memgraphDriver, vpUuid, vpWithValidServiceOffering)

    const result: ValidationResult = await filter.doFilter(vpUuid, null, global.memgraphDriver, 'development')

    expect(result.conforms).toBe(true)
    expect(result.results).toHaveLength(0)
  })

  it('should return a non conform result with reason when cryptographic security standards are missing', async () => {
    await insertObjectInMemGraph(global.memgraphDriver, vpUuid, vpWithInvalidServiceOfferings)

    const result: ValidationResult = await filter.doFilter(vpUuid, null, global.memgraphDriver, 'development')

    expect(result.conforms).toBe(false)
    expect(result.results).toEqual([
      `P3.1.9 - Service offering <https://example.org/service-offering-1> is missing cryptographic security standards`,
      `P3.1.9 - Service offering <https://example.org/service-offering-2> is missing cryptographic security standards`,
      'P3.1.9 - More details about this criterion here: https://gitlab.com/gaia-x/lab/compliance/gx-compliance/-/blob/development/docs/labelling-criteria.md#criterion-p319'
    ])
  })

  it('should return a negative conformity with a reason when the query fails', async () => {
    const driverMock: Driver = {
      session: () => ({
        executeRead: jest.fn().mockRejectedValue(new Error('Test error')),
        close: jest.fn()
      })
    } as unknown as Driver

    const result: ValidationResult = await filter.doFilter(vpUuid, null, driverMock, 'development')

    expect(result.conforms).toBe(false)
    expect(result.results).toEqual(['An error occurred while checking that service offerings have cryptographic security standards : Test error'])
  })
})
