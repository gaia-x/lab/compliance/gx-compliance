import { Driver } from 'neo4j-driver'

import { ValidationResult } from '../../common/dto'
import { insertObjectInMemGraph } from '../../tests/memgraph-environment-setup'
import { VerifiablePresentationValidationService } from '../service/verifiable-presentation-validation.service'
import { ValidationFilter } from './common/validation-filter.interface'
import vpWithRegistrationNumbersFromTrustedIssuers from './fixtures/service-offering-is-operated-by-gaia-x-participant-filter/vp-with-a-trusted-registration-number-issuer.json'
import vpWithASingleNonTrustedRegistrationNumbersIssuer from './fixtures/service-offering-is-operated-by-gaia-x-participant-filter/vp-with-invalid-service-offerings.json'
import { ServiceOfferingIsOperatedByGaiaXParticipantFilter } from './service-offering-is-operated-by-gaia-x-participant.filter'

describe('ServiceOfferingIsOperatedByGaiaXParticipantFilter', () => {
  let vpUuid: string
  const filter: ValidationFilter = new ServiceOfferingIsOperatedByGaiaXParticipantFilter()

  beforeEach(() => {
    vpUuid = VerifiablePresentationValidationService.getUUIDStartingWithALetter()
  })

  it('should return a positive conformity', async () => {
    await insertObjectInMemGraph(global.memgraphDriver, vpUuid, vpWithRegistrationNumbersFromTrustedIssuers)
    const result: ValidationResult = await filter.doFilter(vpUuid, null, global.memgraphDriver, 'development')

    expect(result.conforms).toBe(true)
    expect(result.results).toHaveLength(0)
  })

  it('should return a negative conformity with reason when at least one registration number issuer is not trusted', async () => {
    await insertObjectInMemGraph(global.memgraphDriver, vpUuid, vpWithASingleNonTrustedRegistrationNumbersIssuer)

    const result: ValidationResult = await filter.doFilter(vpUuid, null, global.memgraphDriver, 'development')

    expect(result.conforms).toBe(false)
    expect(result.results).toEqual([
      'P1.3.2 - Service offering <https://example.org/provider-missing-service-offering.json> must have a trusted provider registration number issuer',
      'P1.3.2 - Service offering <https://example.org/registration-number-missing-service-offering.json> must have a trusted provider registration number issuer',
      'P1.3.2 - More details about this criterion here: https://gitlab.com/gaia-x/lab/compliance/gx-compliance/-/blob/development/docs/labelling-criteria.md#criterion-p132'
    ])
  })

  it('should return a negative conformity with a reason when the query fails', async () => {
    const driverMock: Driver = {
      session: () => ({
        executeRead: jest.fn().mockRejectedValue(new Error('Test error')),
        close: jest.fn()
      })
    } as unknown as Driver

    const result: ValidationResult = await filter.doFilter(vpUuid, null, driverMock, 'development')

    expect(result.conforms).toBe(false)
    expect(result.results).toEqual(['P1.3.2 - An error occurred while checking service offerings are operated by Gaia-X participant: Test error'])
  })
})
