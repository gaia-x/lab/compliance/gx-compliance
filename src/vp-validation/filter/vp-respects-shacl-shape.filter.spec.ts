import { Summary } from 'prom-client'

import { ShaclService } from '../service/shacl.service'
import { VerifiablePresentationValidationService } from '../service/verifiable-presentation-validation.service'
import { FilterValidationResult, ValidationFilter } from './common/validation-filter.interface'
import { VpRespectsShaclShapeFilter } from './vp-respects-shacl-shape.filter'

describe('VpRespectsShaclShapeFilter', () => {
  it('should return the validation result returned by the SHACL service', () => {
    const validationResult: FilterValidationResult = { conforms: true, results: [], validatedCriteria: [] }
    const summaryMock: Summary = {
      startTimer: () => {
        return jest.fn()
      }
    } as unknown as Summary
    const shaclServiceMock = {
      verifyShape: async () => validationResult
    } as unknown as ShaclService

    const filter: ValidationFilter = new VpRespectsShaclShapeFilter(shaclServiceMock, summaryMock)

    expect(
      filter.doFilter(
        VerifiablePresentationValidationService.getUUIDStartingWithALetter(),
        { '@context': [], '@type': ['TestCredential'], verifiableCredential: [] },
        null,
        'development'
      )
    ).resolves.toEqual(validationResult)
  })
})
