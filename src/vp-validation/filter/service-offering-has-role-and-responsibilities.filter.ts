import { Logger } from '@nestjs/common'

import { errorMessageDocumentation } from '../../common/utils/error-handling.util'
import { LegalDocumentFilter, ServiceOfferingLegalDocuments } from './common/legal-document.filter'
import { FilterValidationResult } from './common/validation-filter.interface'

/**
 * Implementation of the Gaia-X
 * <a href="https://gaia-x.gitlab.io/policy-rules-committee/compliance-document/criterions/#P2.1.2">P2.1.2 labelling criterion</a>.
 */
export class ServiceOfferingHasRoleAndResponsibilitiesFilter extends LegalDocumentFilter {
  constructor() {
    super(new Logger(ServiceOfferingHasRoleAndResponsibilitiesFilter.name))
  }

  verifyLegalDocuments(vpUUID: string, contextVersion: string, results: ServiceOfferingLegalDocuments[]): FilterValidationResult {
    this.logger.debug(`Checking that service offerings have at least one Role and Responsibilities legal document for VPUUID ${vpUUID}...`)

    const errorMessages: string[] = results
      .filter(
        result => !result.legalDocuments.some(legalDocument => legalDocument.type === `w3id.org/gaia-x/${contextVersion}#RoleAndResponsibilities`)
      )
      .map(result => {
        this.logger.error(
          `P2.1.2 - Service offering ${result.serviceOfferingId} is missing a Role and Responsibilities legal document for VPUUID ${vpUUID}...`
        )
        return `P2.1.2 - Service offering ${result.serviceOfferingId} is missing a Role and Responsibilities legal document`
      })

    if (errorMessages.length) {
      errorMessages.push(errorMessageDocumentation('P2.1.2'))
    }

    return {
      conforms: !errorMessages.length,
      results: errorMessages,
      validatedCriteria: !errorMessages.length ? ['P2.1.2'] : []
    }
  }
}
