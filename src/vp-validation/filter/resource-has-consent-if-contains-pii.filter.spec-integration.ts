import { Driver } from 'neo4j-driver'

import { ValidationResult } from '../../common/dto'
import { insertObjectInMemGraph } from '../../tests/memgraph-environment-setup'
import vpWithResourcesWithAddressAndPiiAndConsent from '../filter/fixtures/resource-has-consent-if-contains-pii-filter/vp-with-resources-with-address-and-pii-and-consent.json'
import vpWithResourcesWithAddressAndPiiNoConsent from '../filter/fixtures/resource-has-consent-if-contains-pii-filter/vp-with-resources-with-address-and-pii-no-consent.json'
import vpWithResourcesWithAddressNoPiiNoConsent from '../filter/fixtures/resource-has-consent-if-contains-pii-filter/vp-with-resources-with-address-no-pii-no-consent.json'
import { VerifiablePresentationValidationService } from '../service/verifiable-presentation-validation.service'
import { ValidationFilter } from './common/validation-filter.interface'
import { ResourceHasConsentIfContainsPiiFilter } from './resource-has-consent-if-contains-pii.filter'

describe('ResourceHasConsentIfContainsPiiFilter', () => {
  let vpUuid: string

  beforeEach(() => {
    vpUuid = VerifiablePresentationValidationService.getUUIDStartingWithALetter()
  })

  it('should return a positive conformity', async () => {
    await insertObjectInMemGraph(global.memgraphDriver, vpUuid, vpWithResourcesWithAddressAndPiiAndConsent)

    const filter: ValidationFilter = new ResourceHasConsentIfContainsPiiFilter()
    const result: ValidationResult = await filter.doFilter(vpUuid, null, global.memgraphDriver, 'development')

    expect(result.conforms).toBe(true)
    expect(result.results).toHaveLength(0)
  })

  it('should return a positive conformity when PII & consent are missing', async () => {
    await insertObjectInMemGraph(global.memgraphDriver, vpUuid, vpWithResourcesWithAddressNoPiiNoConsent)

    const filter: ValidationFilter = new ResourceHasConsentIfContainsPiiFilter()
    const result: ValidationResult = await filter.doFilter(vpUuid, null, global.memgraphDriver, 'development')

    expect(result.conforms).toBe(true)
    expect(result.results).toHaveLength(0)
  })

  it('should return a negative conformity with reason when consent is missing', async () => {
    await insertObjectInMemGraph(global.memgraphDriver, vpUuid, vpWithResourcesWithAddressAndPiiNoConsent)

    const filter: ValidationFilter = new ResourceHasConsentIfContainsPiiFilter()
    const result: ValidationResult = await filter.doFilter(vpUuid, null, global.memgraphDriver, 'development')

    expect(result.conforms).toBe(false)
    expect(result.results).toEqual(['Resource contains PII without Consent'])
  })

  it('should return a negative conformity with a reason when the query fails', async () => {
    const driverMock: Driver = {
      session: () => ({
        executeRead: jest.fn().mockRejectedValue(new Error('Test error')),
        close: jest.fn()
      })
    } as unknown as Driver

    const filter: ValidationFilter = new ResourceHasConsentIfContainsPiiFilter()
    const result: ValidationResult = await filter.doFilter(vpUuid, null, driverMock, 'development')

    expect(result.conforms).toBe(false)
    expect(result.results).toEqual(['An error occurred while checking for PII and consent in resources: Test error'])
  })
})
