import { Driver } from 'neo4j-driver'

import { ValidationResult } from '../../common/dto'
import { insertObjectInMemGraph } from '../../tests/memgraph-environment-setup'
import { VerifiablePresentationValidationService } from '../service/verifiable-presentation-validation.service'
import { ValidationFilter } from './common/validation-filter.interface'
import vpWithInvalidServiceOfferings from './fixtures/legally-binding-acts-cover-entire-service-offering-filter/vp-with-invalid-service-offerings.json'
import vpWithValidServiceOfferings from './fixtures/legally-binding-acts-cover-entire-service-offering-filter/vp-with-valid-service-offerings.json'
import { LegallyBindingActsCoverEntireServiceOfferingFilter } from './legally-binding-acts-cover-entire-service-offering.filter'

describe('LegallyBindingActsCoverEntireServiceOfferingFilter', () => {
  let vpUuid: string
  const filter: ValidationFilter = new LegallyBindingActsCoverEntireServiceOfferingFilter()

  beforeEach(() => {
    vpUuid = VerifiablePresentationValidationService.getUUIDStartingWithALetter()
  })

  it('should return a positive conformity', async () => {
    await insertObjectInMemGraph(global.memgraphDriver, vpUuid, vpWithValidServiceOfferings)

    const result: ValidationResult = await filter.doFilter(vpUuid, null, global.memgraphDriver, 'development')

    expect(result.conforms).toBe(true)
    expect(result.results).toHaveLength(0)
  })

  it('should return a negative conformity with a reason when some service offerings are missing required attributes', async () => {
    await insertObjectInMemGraph(global.memgraphDriver, vpUuid, vpWithInvalidServiceOfferings)

    const result: ValidationResult = await filter.doFilter(vpUuid, null, global.memgraphDriver, 'development')

    expect(result.conforms).toBe(false)
    expect(result.results).toEqual([
      'P1.1.4 - Service offering example.org/service-offering-without-aggregation-of-resources.json is missing a service scope or, an aggregation of resources and a depends on attribute',
      'P1.1.4 - Service offering example.org/service-offering-without-any-required-attribute.json is missing a service scope or, an aggregation of resources and a depends on attribute',
      'P1.1.4 - Service offering example.org/service-offering-without-depends-on.json is missing a service scope or, an aggregation of resources and a depends on attribute',
      'P1.1.4 - More details about this criterion here: https://gitlab.com/gaia-x/lab/compliance/gx-compliance/-/blob/development/docs/labelling-criteria.md#criterion-p114'
    ])
  })

  it('should return a negative conformity with a reason when the query fails', async () => {
    const driverMock: Driver = {
      session: () => ({
        executeRead: jest.fn().mockRejectedValue(new Error('Test error')),
        close: jest.fn()
      })
    } as unknown as Driver

    const result: ValidationResult = await filter.doFilter(vpUuid, null, driverMock, 'development')

    expect(result.conforms).toBe(false)
    expect(result.results).toEqual(['An error occurred while checking that legally binding acts cover the entire service offering: Test error'])
  })
})
