import { Logger } from '@nestjs/common'

import { Driver } from 'neo4j-driver'

import { errorMessageDocumentation } from '../../common/utils/error-handling.util'
import { FilterValidationResult, ValidationFilter } from './common/validation-filter.interface'

interface SubContractorProperties {
  type: string
  value: string
}

export class ServiceOfferingHasSubContractorDetailsFilter implements ValidationFilter {
  private readonly logger: Logger = new Logger(ServiceOfferingHasSubContractorDetailsFilter.name)

  async doFilter(vpUUID: string, _verifiablePresentation: any, driver: Driver, contextVersion: string): Promise<FilterValidationResult> {
    this.logger.debug(`Checking that each subcontractor has required details for VPUUID ${vpUUID}...`)

    const query = `
      MATCH (credentialSubject)<-[:_https_www_w3_org_2018_credentials_credentialSubject_]-(serviceOffering)
        -[:_http_www_w3_org_1999_02_22_rdf_syntax_ns_type_]->(:_https_w3id_org_gaia_x_${contextVersion}_ServiceOffering_)
      WHERE serviceOffering.vpID="${vpUUID}"
      CALL {
        WITH credentialSubject
        OPTIONAL MATCH (credentialSubject)-[:_https_w3id_org_gaia_x_${contextVersion}_subContractors_]->(subContractor)
        OPTIONAL MATCH (subContractor)-[r*]->(property)
        RETURN subContractor.value AS subContractorId, COLLECT({ type: type(r[0]), value: property.value }) AS properties
      }
      RETURN serviceOffering.value AS serviceOfferingId, COLLECT(subContractorId) AS subcontractors, COLLECT(properties) AS properties`

    const session = driver.session()
    try {
      const results = await session.executeRead(tx => tx.run(query))

      const errorMessages: string[] = []
      let isP126Valid = true
      let isP127Valid = true
      for (const record of results.records) {
        const serviceOfferingId: string = record.get('serviceOfferingId')
        const subcontractors: string[] = record.get('subcontractors')
        const properties: SubContractorProperties[][] = record.get('properties')

        if (!subcontractors.length) {
          this.logger.error(`P1.2.6 - P1.2.7 - Service offering ${serviceOfferingId} has no subcontractors for VPUUID ${vpUUID}...`)
          errorMessages.push(`P1.2.6 - P1.2.7 - Service offering ${serviceOfferingId} has no subcontractors`)
          isP126Valid = false
          isP127Valid = false
        } else {
          for (let i = 0; i < subcontractors.length; i++) {
            const subContractorId = subcontractors[i]
            const subContractorProperties = properties[i]

            if (!subContractorProperties.some(prop => prop.type === `_https_w3id_org_gaia_x_${contextVersion}_communicationMethods_`)) {
              this.logger.error(
                `P1.2.6 - Subcontractor ${subContractorId} in service offering ${serviceOfferingId} is missing a communication method for VPUUID ${vpUUID}...`
              )
              errorMessages.push(
                `P1.2.6 - Subcontractor ${subContractorId} in service offering ${serviceOfferingId} is missing a communication method`
              )
              isP126Valid = false
            }

            if (!subContractorProperties.some(prop => prop.type === `_https_w3id_org_gaia_x_${contextVersion}_informationDocuments_`)) {
              this.logger.error(
                `P1.2.6 - Subcontractor ${subContractorId} in service offering ${serviceOfferingId} is missing information documents for VPUUID ${vpUUID}...`
              )
              errorMessages.push(
                `P1.2.6 - Subcontractor ${subContractorId} in service offering ${serviceOfferingId} is missing information documents`
              )
              isP126Valid = false
            }

            if (!subContractorProperties.some(prop => prop.type === `_https_w3id_org_gaia_x_${contextVersion}_applicableJurisdiction_`)) {
              this.logger.error(
                `P1.2.7 - Subcontractor ${subContractorId} in service offering ${serviceOfferingId} is missing an applicable jurisdiction for VPUUID ${vpUUID}...`
              )
              errorMessages.push(
                `P1.2.7 - Subcontractor ${subContractorId} in service offering ${serviceOfferingId} is missing an applicable jurisdiction`
              )
              isP127Valid = false
            }

            if (!subContractorProperties.some(prop => prop.type === `_https_w3id_org_gaia_x_${contextVersion}_legalName_`)) {
              this.logger.error(
                `P1.2.7 - Subcontractor ${subContractorId} in service offering ${serviceOfferingId} is missing a legal name for VPUUID ${vpUUID}...`
              )
              errorMessages.push(`P1.2.7 - Subcontractor ${subContractorId} in service offering ${serviceOfferingId} is missing a legal name`)
              isP127Valid = false
            }
          }
        }
      }

      const validatedCriteria = []
      if (isP126Valid) {
        validatedCriteria.push('P1.2.6')
      } else {
        errorMessages.push(errorMessageDocumentation('P1.2.6'))
      }
      if (isP127Valid) {
        validatedCriteria.push('P1.2.7')
      } else {
        errorMessages.push(errorMessageDocumentation('P1.2.7'))
      }

      return {
        conforms: errorMessages.length < 1,
        results: errorMessages,
        validatedCriteria
      }
    } catch (error) {
      this.logger.error(`An error occurred while checking subcontractor details for VPUUID ${vpUUID}. Error: ${error.message}`)
      return {
        conforms: false,
        results: [`An error occurred while checking subcontractor details: ${error.message}`],
        validatedCriteria: []
      }
    } finally {
      await session.close()
    }
  }
}
