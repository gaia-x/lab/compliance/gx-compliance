import { Logger } from '@nestjs/common'

import { Driver } from 'neo4j-driver'

import { VerifiablePresentation } from '../../common/constants'
import { errorMessageDocumentation } from '../../common/utils/error-handling.util'
import { FilterValidationResult, ValidationFilter } from './common/validation-filter.interface'

/**
 * Implementation of the Gaia-X
 * <a href="https://gaia-x.gitlab.io/policy-rules-committee/compliance-document/criterions/#P1.3.1">P1.3.1 labelling criterion</a>.
 * <a href="https://gitlab.com/gaia-x/technical-committee/service-characteristics-working-group/service-characteristics/-/blame/develop/single-point-of-truth/service-offering.yaml?ref_type=heads#L71">Service Policy Definition</a>.
 */
export class ServiceOfferingHasServicePolicyFilter implements ValidationFilter {
  private readonly logger: Logger = new Logger(ServiceOfferingHasServicePolicyFilter.name)

  async doFilter(
    vpUUID: string,
    _verifiablePresentation: VerifiablePresentation,
    driver: Driver,
    contextVersion: string
  ): Promise<FilterValidationResult> {
    this.logger.debug(`P1.3.1 - Checking that service offerings have service policy for VPUUID ${vpUUID}...`)

    const query = `MATCH (:_https_w3id_org_gaia_x_${contextVersion}_ServiceOffering_)<-[:_http_www_w3_org_1999_02_22_rdf_syntax_ns_type_]
    -(serviceOffering)-[:_https_www_w3_org_2018_credentials_credentialSubject_]->(credentialSubject)
      WHERE serviceOffering.vpID="${vpUUID}"
      CALL {
        WITH credentialSubject
        OPTIONAL MATCH (credentialSubject)-[:_https_w3id_org_gaia_x_${contextVersion}_servicePolicy_]->(policy)
        -[:_http_www_w3_org_1999_02_22_rdf_syntax_ns_type_]->(:_https_w3id_org_gaia_x_${contextVersion}_AccessUsagePolicy_)
        RETURN COLLECT(policy.value) AS policies
      }
      RETURN serviceOffering.value as serviceOfferingId, policies`

    const session = driver.session()
    try {
      const results = await session.executeRead(tx => tx.run(query))

      const errorMessages: string[] = []
      for (const record of results.records) {
        const serviceOfferingId: string = record.get('serviceOfferingId')
        const policies: string[] = record.get('policies')

        if (!policies.length) {
          this.logger.error(`P1.3.1 - Service offering ${serviceOfferingId} is missing a service policy for VPUUID ${vpUUID}...`)
          errorMessages.push(`P1.3.1 - Service offering ${serviceOfferingId} is missing a service policy`)
        }
      }

      if (errorMessages.length) {
        errorMessages.push(errorMessageDocumentation('P1.3.1'))
      }

      return {
        conforms: !errorMessages.length,
        results: errorMessages,
        validatedCriteria: !errorMessages.length ? ['P1.3.1'] : []
      }
    } catch (error) {
      this.logger.error(
        `P1.3.1 - An error occurred while checking that service offerings have service policy for VPUUID ${vpUUID}. Error: ${error.message}`
      )

      return {
        conforms: false,
        results: [`P1.3.1 - An error occurred while checking that service offerings have service policy : ${error.message}`],
        validatedCriteria: []
      }
    } finally {
      await session.close()
    }
  }
}
