import { Logger } from '@nestjs/common'

import { Driver } from 'neo4j-driver'

import { VerifiablePresentation } from '../../common/constants'
import { graphValueFormat } from '../../common/utils/graph-value-format'
import { FilterValidationResult, ValidationFilter } from './common/validation-filter.interface'

export class IssuersHaveTermsAndConditionsFilter implements ValidationFilter {
  private readonly logger: Logger = new Logger(IssuersHaveTermsAndConditionsFilter.name)

  async doFilter(
    vpUUID: string,
    _verifiablePresentation: VerifiablePresentation,
    driver: Driver,
    contextVersion: string
  ): Promise<FilterValidationResult> {
    this.logger.debug(`Checking that each issuer has a valid Terms And Conditions VPUUID ${vpUUID}...`)

    const session = driver.session()

    const query = `MATCH (nodeIssuer)<-[:_https_www_w3_org_2018_credentials_issuer_]-(node)
    -[:_http_www_w3_org_1999_02_22_rdf_syntax_ns_type_]->(nodeType)
    WITH DISTINCT nodeIssuer
      WHERE nodeIssuer.vpID="${vpUUID}" AND NOT 
        nodeType.value=~"<https://w3id.org/gaia-x/${contextVersion}#RegistrationNumber>"
      OPTIONAL MATCH (nodeIssuer)<-[:_https_www_w3_org_2018_credentials_issuer_]-(issuer)
        -[:_http_www_w3_org_1999_02_22_rdf_syntax_ns_type_]->(:_https_w3id_org_gaia_x_${contextVersion}_Issuer_)
      CALL {
        WITH issuer
        OPTIONAL MATCH (issuer)-[:_https_www_w3_org_2018_credentials_credentialSubject_]->()
          -[:_https_w3id_org_gaia_x_${contextVersion}_gaiaxTermsAndConditions_]->(termsAndConditions)
        RETURN termsAndConditions
      }
      RETURN nodeIssuer.value AS issuer, termsAndConditions.value AS termsAndConditionsHash;`

    try {
      const results = await session.executeRead(tx => tx.run(query))
      this.logger.debug(`Found ${results.records.length} issuers for VPUUID ${vpUUID}}`)

      const missingTermsAndConditions: string[] = []
      const invalidTermsAndConditions: string[] = []
      for (const result of results.records) {
        const issuerValue: string = graphValueFormat(result.get('issuer'))

        if (!result.get('termsAndConditionsHash')) {
          this.logger.error(`${issuerValue} is missing a gx:Issuer entity with terms and conditions for VPUUID ${vpUUID}`)
          missingTermsAndConditions.push(issuerValue)
        }

        if (
          result.get('termsAndConditionsHash') &&
          result.get('termsAndConditionsHash') != '4bd7554097444c960292b4726c2efa1373485e8a5565d94d41195214c5e0ceb3'
        ) {
          this.logger.error(`${issuerValue} has an incorrect Gaia-X terms and conditions hash for VPUUID ${vpUUID}`)
          invalidTermsAndConditions.push(issuerValue)
        }
      }

      if (missingTermsAndConditions.length > 0 || invalidTermsAndConditions.length > 0) {
        return {
          conforms: false,
          results: missingTermsAndConditions
            .map(issuer => `The issuer: ${issuer} is missing a gx:Issuer entity with terms and conditions`)
            .concat(
              invalidTermsAndConditions.map(
                issuer =>
                  `The issuer: ${issuer} has an incorrect Gaia-X terms and conditions hash. Please follow https://docs.gaia-x.eu/ontology/${contextVersion}/enums/GaiaXTermsAndConditions for more details.`
              )
            ),
          validatedCriteria: []
        }
      }

      return {
        conforms: true,
        results: [],
        validatedCriteria: []
      }
    } catch (error) {
      this.logger.error(
        `An error occurred while checking that each issuer has a valid Terms And Conditions for VPUUID ${vpUUID}. Error: ${error.message}`
      )

      return {
        conforms: false,
        results: [`An error occurred while checking that each issuer has a valid Terms And Conditions: ${error.message}`],
        validatedCriteria: []
      }
    } finally {
      await session.close()
    }
  }
}
