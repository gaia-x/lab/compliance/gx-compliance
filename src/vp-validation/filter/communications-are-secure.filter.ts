import { Logger } from '@nestjs/common'

import { Driver } from 'neo4j-driver'

import { VerifiablePresentation } from '../../common/constants'
import { errorMessageDocumentation } from '../../common/utils/error-handling.util'
import { FilterValidationResult, ValidationFilter } from './common/validation-filter.interface'

export class CommunicationsAreSecureFilter implements ValidationFilter {
  private readonly logger: Logger = new Logger(CommunicationsAreSecureFilter.name)

  /**
   * Please note that verifying the location information of resources is not necessary as SHACL shapes are in charge of
   * that part.
   *
   * @param vpUUID
   * @param _verifiablePresentation
   * @param driver
   * @param contextVersion
   */
  async doFilter(
    vpUUID: string,
    _verifiablePresentation: VerifiablePresentation,
    driver: Driver,
    contextVersion: string
  ): Promise<FilterValidationResult> {
    this.logger.debug(`Checking that communications are secure for VPUUID ${vpUUID}...`)
    const pointOfPresenceRdfId = `<https://w3id.org/gaia-x/${contextVersion}#PointOfPresence>`
    const datacenterRdfId = `<https://w3id.org/gaia-x/${contextVersion}#Datacenter>`

    const resourceTypeQuery = `MATCH (:_https_w3id_org_gaia_x_${contextVersion}_ServiceOffering_)<-[:_http_www_w3_org_1999_02_22_rdf_syntax_ns_type_]-()
        -[:_https_www_w3_org_2018_credentials_credentialSubject_]->(serviceOffering)
      WHERE serviceOffering.vpID = '${vpUUID}'
      
      OPTIONAL MATCH (serviceOffering)-[:_https_w3id_org_gaia_x_${contextVersion}_aggregationOfResources_ *]->(resource)
        <-[:_https_www_w3_org_2018_credentials_credentialSubject_]-()
        -[:_http_www_w3_org_1999_02_22_rdf_syntax_ns_type_]->(resourceType)
          
      OPTIONAL MATCH (resource)<-[:_https_w3id_org_gaia_x_${contextVersion}_refersTo_]-(datacenterAllocation)
        <-[:_https_w3id_org_gaia_x_${contextVersion}_datacenterAllocation_]-(allocatedResource)
        <-[:_https_www_w3_org_2018_credentials_credentialSubject_]-()
        -[:_http_www_w3_org_1999_02_22_rdf_syntax_ns_type_]->(allocatedResourceType)
      
      OPTIONAL MATCH (allocatedResource)<-[:_https_w3id_org_gaia_x_${contextVersion}_interconnectionPointIdentifier_]-(point)
        <-[:_https_www_w3_org_2018_credentials_credentialSubject_]-()
        -[:_http_www_w3_org_1999_02_22_rdf_syntax_ns_type_]->(pointType) 
      
      RETURN serviceOffering.value AS serviceOfferingId, 
        collect(DISTINCT pointType.value) + collect(DISTINCT allocatedResourceType.value) + collect(DISTINCT resourceType.value) AS resourceTypes`

    const session = driver.session()
    try {
      const resourceTypeResults = await session.executeRead(tx => tx.run(resourceTypeQuery))

      this.logger.debug(`Found ${resourceTypeResults.records.length} service offerings for VPUUID ${vpUUID}...`)

      const errorMessages: string[] = []
      for (const record of resourceTypeResults.records) {
        const serviceOfferingId: string = record.get('serviceOfferingId')
        const resourceTypes: string[] = record.get('resourceTypes')

        if (!resourceTypes.includes(pointOfPresenceRdfId)) {
          this.logger.error(`Service offering ${serviceOfferingId} is missing a point of presence description for VPUUID ${vpUUID}...`)
          errorMessages.push(`P3.1.10 - Service offering ${serviceOfferingId} is missing a point of presence description`)
        }

        if (!resourceTypes.includes(datacenterRdfId)) {
          this.logger.error(`Service offering ${serviceOfferingId} is missing a datacenter description for VPUUID ${vpUUID}...`)
          errorMessages.push(`P3.1.10 - Service offering ${serviceOfferingId} is missing a datacenter description`)
        }
      }

      if (errorMessages.length) {
        errorMessages.push(errorMessageDocumentation('P3.1.10'))
      }

      return {
        conforms: !errorMessages.length,
        results: errorMessages,
        validatedCriteria: !errorMessages.length ? ['P3.1.10'] : ['']
      }
    } catch (error) {
      this.logger.error(`An error occurred while checking that communications are secure for VPUUID ${vpUUID}. Error: ${error.message}`)

      return {
        conforms: false,
        results: [`An error occurred while checking that communications are secure: ${error.message}`],
        validatedCriteria: []
      }
    } finally {
      await session.close()
    }
  }
}
