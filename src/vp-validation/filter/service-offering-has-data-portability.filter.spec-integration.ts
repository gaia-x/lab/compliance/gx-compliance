import { Driver } from 'neo4j-driver'

import { ValidationResult } from '../../common/dto'
import { insertObjectInMemGraph } from '../../tests/memgraph-environment-setup'
import { VerifiablePresentationValidationService } from '../service/verifiable-presentation-validation.service'
import { ValidationFilter } from './common/validation-filter.interface'
import vpWithInvalidServiceOfferings from './fixtures/service-offering-has-data-portability-filter/vp-with-invalid-service-offerings.json'
import vpWithValidServiceOffering from './fixtures/service-offering-has-data-portability-filter/vp-with-valid-service-offering.json'
import { ServiceOfferingHasDataPortabilityFilter } from './service-offering-has-data-portability.filter'

describe('ServiceOfferingHasDataPortabilityFilter', () => {
  let vpUuid: string
  const filter: ValidationFilter = new ServiceOfferingHasDataPortabilityFilter()

  beforeEach(() => {
    vpUuid = VerifiablePresentationValidationService.getUUIDStartingWithALetter()
  })

  it('should return a positive conformity', async () => {
    await insertObjectInMemGraph(global.memgraphDriver, vpUuid, vpWithValidServiceOffering)

    const result: ValidationResult = await filter.doFilter(vpUuid, null, global.memgraphDriver, 'development')

    expect(result.conforms).toBe(true)
    expect(result.results).toHaveLength(0)
  })

  it('should return a non conform result with reason when data portability is missing', async () => {
    await insertObjectInMemGraph(global.memgraphDriver, vpUuid, vpWithInvalidServiceOfferings)

    const result: ValidationResult = await filter.doFilter(vpUuid, null, global.memgraphDriver, 'development')

    expect(result.conforms).toBe(false)
    expect(result.results).toEqual([
      `P3.1.11 - Service offering <https://example.org/service-offering-1> is missing data portability`,
      `P3.1.11 - Service offering <https://example.org/service-offering-2> is missing data portability`,
      `P4.1.1 - Service offering <https://example.org/service-offering-3> has invalid data portability documentation URI 'not-a-URI'`,
      `P4.1.1 - Service offering <https://example.org/service-offering-3> has invalid data portability documentation URI 'not-a-URL'`,
      `P4.1.1 - Service offering <https://example.org/service-offering-3> has invalid data portability pricing URI 'pricing-page-please'`,
      `P4.1.1 - Service offering <https://example.org/service-offering-3> is missing data portability means`,
      `P4.1.1 - Service offering <https://example.org/service-offering-3> is missing data portability formats`,
      `P4.1.1 - Service offering <https://example.org/service-offering-3> is missing data portability documentation`,
      `P4.1.2 - Service offering <https://example.org/service-offering-3> is missing data portability pricing`,
      `P4.1.1 - Service offering <https://example.org/service-offering-3> is missing data portability means`,
      `P4.1.1 - Service offering <https://example.org/service-offering-3> is missing data portability formats`,
      `P4.1.1 - Service offering <https://example.org/service-offering-3> is missing data portability documentation`,
      `P4.1.2 - Service offering <https://example.org/service-offering-3> is missing data portability pricing`,
      `P4.1.1 - Service offering <https://example.org/service-offering-3> is missing data portability legal documentation`,
      `P4.1.1 - Service offering <https://example.org/service-offering-3> data portability legal document is invalid: Error: Legal document url 'invalid-url' is invalid`,
      'P4.1.1 - More details about this criterion here: https://gitlab.com/gaia-x/lab/compliance/gx-compliance/-/blob/development/docs/labelling-criteria.md#criterion-p411',
      'P4.1.2 - More details about this criterion here: https://gitlab.com/gaia-x/lab/compliance/gx-compliance/-/blob/development/docs/labelling-criteria.md#criterion-p412',
      'P3.1.11 - More details about this criterion here: https://gitlab.com/gaia-x/lab/compliance/gx-compliance/-/blob/development/docs/labelling-criteria.md#criterion-p3111'
    ])
  })

  it('should return a negative conformity with a reason when the query fails', async () => {
    const driverMock: Driver = {
      session: () => ({
        executeRead: jest.fn().mockRejectedValue(new Error('Test error')),
        close: jest.fn()
      })
    } as unknown as Driver

    const result: ValidationResult = await filter.doFilter(vpUuid, null, driverMock, 'development')

    expect(result.conforms).toBe(false)
    expect(result.results).toEqual(['An error occurred while checking that service offerings have data portability : Test error'])
  })
})
