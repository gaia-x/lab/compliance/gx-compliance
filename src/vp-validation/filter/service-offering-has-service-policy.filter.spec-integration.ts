import { Driver } from 'neo4j-driver'

import { ValidationResult } from '../../common/dto'
import { insertObjectInMemGraph } from '../../tests/memgraph-environment-setup'
import vpWithMissingServicePolicy from '../filter/fixtures/service-offering-has-service-policy/vp-missing-service-policy.json'
import vpWithValidServiceOffering from '../filter/fixtures/service-offering-has-service-policy/vp-valid-service-policy.json'
import { VerifiablePresentationValidationService } from '../service/verifiable-presentation-validation.service'
import { ValidationFilter } from './common/validation-filter.interface'
import { ServiceOfferingHasServicePolicyFilter } from './service-offering-has-service-policy.filter'

describe('ServiceOfferingHasServicePolicyFilter', () => {
  let vpUuid: string
  const filter: ValidationFilter = new ServiceOfferingHasServicePolicyFilter()

  beforeEach(() => {
    vpUuid = VerifiablePresentationValidationService.getUUIDStartingWithALetter()
  })

  it('should pass with a valid presentation', async () => {
    await insertObjectInMemGraph(global.memgraphDriver, vpUuid, vpWithValidServiceOffering)

    const result: ValidationResult = await filter.doFilter(vpUuid, null, global.memgraphDriver, 'development')

    expect(result.conforms).toBe(true)
    expect(result.results).toHaveLength(0)
  })

  it('should not pass when service policy is missing', async () => {
    await insertObjectInMemGraph(global.memgraphDriver, vpUuid, vpWithMissingServicePolicy)

    const result: ValidationResult = await filter.doFilter(vpUuid, null, global.memgraphDriver, 'development')

    expect(result.conforms).toBe(false)

    expect(result.results).toContain('P1.3.1 - Service offering <https://gaia-x.eu/service-offering.json> is missing a service policy')
    expect(result.results).toContain('P1.3.1 - Service offering <https://gaia-x.eu/service-offering1.json> is missing a service policy')
    expect(result.results).toContain(
      'P1.3.1 - More details about this criterion here: https://gitlab.com/gaia-x/lab/compliance/gx-compliance/-/blob/development/docs/labelling-criteria.md#criterion-p131'
    )
  })

  it('should not pass when query fails', async () => {
    const driverMock: Driver = {
      session: () => ({
        executeRead: jest.fn().mockRejectedValue(new Error('Test error')),
        close: jest.fn()
      })
    } as unknown as Driver

    const result: ValidationResult = await filter.doFilter(vpUuid, null, driverMock, 'development')

    expect(result.conforms).toBe(false)
    expect(result.results).toEqual(['P1.3.1 - An error occurred while checking that service offerings have service policy : Test error'])
  })
})
