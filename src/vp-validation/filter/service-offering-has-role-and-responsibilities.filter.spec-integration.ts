import { Driver } from 'neo4j-driver'

import { ValidationResult } from '../../common/dto'
import { insertObjectInMemGraph } from '../../tests/memgraph-environment-setup'
import vpWithMissingRoleAndResponsibilityDocument from '../filter/fixtures/service-offering-has-roles-and-responsibilities/vp-missing-roles-and-responsibilities.json'
import vpWithValidServiceOffering from '../filter/fixtures/service-offering-has-roles-and-responsibilities/vp-valid-roles-and-responsabilities.json'
import { VerifiablePresentationValidationService } from '../service/verifiable-presentation-validation.service'
import { ValidationFilter } from './common/validation-filter.interface'
import { ServiceOfferingHasRoleAndResponsibilitiesFilter } from './service-offering-has-role-and-responsibilities.filter'

describe('ServiceOfferingHasRolesAndResponsibilitiesFilter', () => {
  let vpUuid: string
  const filter: ValidationFilter = new ServiceOfferingHasRoleAndResponsibilitiesFilter()

  beforeEach(() => {
    vpUuid = VerifiablePresentationValidationService.getUUIDStartingWithALetter()
  })

  it('should pass with a valid presentation', async () => {
    await insertObjectInMemGraph(global.memgraphDriver, vpUuid, vpWithValidServiceOffering)

    const result: ValidationResult = await filter.doFilter(vpUuid, null, global.memgraphDriver, 'development')

    expect(result.conforms).toBe(true)
    expect(result.results).toHaveLength(0)
  })

  it('should not pass when RoleAndResponsibility document is missing', async () => {
    await insertObjectInMemGraph(global.memgraphDriver, vpUuid, vpWithMissingRoleAndResponsibilityDocument)

    const result: ValidationResult = await filter.doFilter(vpUuid, null, global.memgraphDriver, 'development')

    expect(result.conforms).toBe(false)

    expect(result.results).toEqual([
      'P2.1.2 - Service offering <https://gaia-x.eu/service-offering1.json> is missing a Role and Responsibilities legal document',
      'P2.1.2 - Service offering <https://gaia-x.eu/service-offering.json> is missing a Role and Responsibilities legal document',
      'P2.1.2 - More details about this criterion here: https://gitlab.com/gaia-x/lab/compliance/gx-compliance/-/blob/development/docs/labelling-criteria.md#criterion-p212'
    ])
  })

  it('should not pass when query fails', async () => {
    const driverMock: Driver = {
      session: () => ({
        executeRead: jest.fn().mockRejectedValue(new Error('Test error')),
        close: jest.fn()
      })
    } as unknown as Driver

    const result: ValidationResult = await filter.doFilter(vpUuid, null, driverMock, 'development')

    expect(result.conforms).toBe(false)
    expect(result.results).toEqual(['An unexpected error has occurred while collecting legal documents: Test error'])
  })
})
