import Joi from 'joi'

export default Joi.object({
  PORT: Joi.alternatives().try(Joi.string(), Joi.number()).default(3000),
  REGISTRY_URL: Joi.string().required(),
  PRIVATE_KEY: Joi.string().required(),
  PRIVATE_KEY_ALGORITHM: Joi.string().optional(),
  X509_CERTIFICATE: Joi.string().optional()
})
