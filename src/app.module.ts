import { Module } from '@nestjs/common'
import { ScheduleModule } from '@nestjs/schedule'

import { AppController } from './app.controller'
import { CommonModule } from './common/common.module'
import { CredentialOfferModule } from './credential-offer/credential-offer.module'
import { IdentityModule } from './identity/identity.module'
import { JwtModule } from './jwt/jwt.module'
import { VpContentValidationModule } from './vp-validation/vp-content-validation.module'

@Module({
  imports: [CredentialOfferModule, CommonModule, IdentityModule, JwtModule, ScheduleModule.forRoot(), VpContentValidationModule],
  controllers: [AppController]
})
export class AppModule {}
