import canonicalize from 'canonicalize'

import { CredentialSubjectDto, VerifiableCredentialDto } from '../dto'
import { ComplianceEvidenceDTO } from '../dto/compliant-credential-subject.dto'
import { HashingUtils } from '../utils/hashing.utils'
import { VerifiableCredentialUtils } from '../utils/verifiable-credential.utils'

export abstract class CompliantCredentialSubjectMapper {
  abstract type(): string

  public map(verifiableCredential: VerifiableCredentialDto<CredentialSubjectDto>): ComplianceEvidenceDTO {
    const hash: string = HashingUtils.sha256(canonicalize(verifiableCredential))

    return {
      id: verifiableCredential.id ?? verifiableCredential['@id'],
      type: VerifiableCredentialUtils.extractGxTypes(verifiableCredential).join(','),
      'gx:digestSRI': `sha256-${hash}`
    }
  }
}
