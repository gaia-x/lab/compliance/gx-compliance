import { FactoryProvider } from '@nestjs/common'
import { ConfigService } from '@nestjs/config'

export class PrivateKeyAlgorithmProvider {
  static create(): FactoryProvider<string> {
    return {
      provide: 'PRIVATE_KEY_ALGORITHM',
      inject: [ConfigService],
      useFactory: async (configService: ConfigService): Promise<string> => {
        return configService.get<string>('PRIVATE_KEY_ALGORITHM', 'PS256')
      }
    }
  }
}
