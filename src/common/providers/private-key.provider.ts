import { FactoryProvider } from '@nestjs/common'
import { ConfigService } from '@nestjs/config'

import { KeyObject } from 'crypto'
import * as jose from 'jose'
import { KeyLike } from 'jose'

export class PrivateKeyProvider {
  static create(): FactoryProvider<KeyObject | KeyLike> {
    return {
      provide: 'PRIVATE_KEY',
      inject: [ConfigService],
      useFactory: async (configService: ConfigService): Promise<KeyLike | KeyObject> => {
        const privateKey: string = configService.get<string>('PRIVATE_KEY')

        return await jose.importPKCS8(privateKey, null)
      }
    }
  }
}
