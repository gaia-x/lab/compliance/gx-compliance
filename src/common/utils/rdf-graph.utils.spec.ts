import { readFileSync } from 'fs'
import path from 'node:path'

import { ParentClasses, RdfGraphUtils } from './rdf-graph.utils'

describe('RdfGraphUtils', () => {
  const quads = `<https://trusted-issuer.com/lrn.json#cs> <https://w3id.org/gaia-x/development#countryCode> "BE" .
    <https://trusted-issuer.com/lrn.json#cs> <https://w3id.org/gaia-x/development#vatID> "BE0762747721" .
    <https://trusted-issuer.com/lrn.json> <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <https://w3id.org/gaia-x/development#VatID> .
    <https://trusted-issuer.com/lrn.json> <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <https://www.w3.org/2018/credentials#VerifiableCredential> .
    <https://trusted-issuer.com/lrn.json> <https://www.w3.org/2018/credentials#credentialSubject> <https://trusted-issuer.com/lrn.json#cs> .
    <https://trusted-issuer.com/lrn.json> <https://www.w3.org/2018/credentials#evidence> _:c14n0 .
    <https://trusted-issuer.com/lrn.json> <https://www.w3.org/2018/credentials#issuer> <did:web:gaia-x.unicorn-home.com> .
    <https://trusted-issuer.com/lrn.json> <https://www.w3.org/2018/credentials#validFrom> "2024-08-05T13:31:52.859+00:00"^^<http://www.w3.org/2001/XMLSchema#dateTime> .
    _:c14n0 <https://w3id.org/gaia-x/development#evidenceOf> "gx:vatID" .
    _:c14n0 <https://w3id.org/gaia-x/development#evidenceURL> "http://ec.europa.eu/taxation_customs/vies/services/checkVatService" .
    _:c14n0 <https://w3id.org/gaia-x/development#executionDate> "2024-05-15T12:10:23.900Z" .`

  it('should convert quads to queries', () => {
    const queries: string[] = RdfGraphUtils.quadsToQueries(
      'vpUUID',
      quads,
      new Map([['https://w3id.org/gaia-x/development#VatID', ['https://w3id.org/gaia-x/development#RegistrationNumber']]])
    )

    expect(queries).toEqual([
      "CREATE (_vpUUID_https_trusted_issuer_com_lrn_json_:_https_trusted_issuer_com_lrn_json_ {id:'_vpUUID_https_trusted_issuer_com_lrn_json_', value:'<https://trusted-issuer.com/lrn.json>', vpID:'vpUUID'});\n",
      "CREATE (_vpUUID_https_w3id_org_gaia_x_development_VatID_:_https_w3id_org_gaia_x_development_VatID_ {id:'_vpUUID_https_w3id_org_gaia_x_development_VatID_', value:'<https://w3id.org/gaia-x/development#VatID>', vpID:'vpUUID'});\n",
      "CREATE (_vpUUID_https_w3id_org_gaia_x_development_RegistrationNumber_:_https_w3id_org_gaia_x_development_RegistrationNumber_ {id:'_vpUUID_https_w3id_org_gaia_x_development_RegistrationNumber_', value:'<https://w3id.org/gaia-x/development#RegistrationNumber>', vpID:'vpUUID'});\n",
      "CREATE (_vpUUID_https_www_w3_org_2018_credentials_VerifiableCredential_:_https_www_w3_org_2018_credentials_VerifiableCredential_ {id:'_vpUUID_https_www_w3_org_2018_credentials_VerifiableCredential_', value:'<https://www.w3.org/2018/credentials#VerifiableCredential>', vpID:'vpUUID'});\n",
      "CREATE (_vpUUID_https_trusted_issuer_com_lrn_json_cs_:_https_trusted_issuer_com_lrn_json_cs_ {id:'_vpUUID_https_trusted_issuer_com_lrn_json_cs_', value:'<https://trusted-issuer.com/lrn.json#cs>', vpID:'vpUUID'});\n",
      "CREATE (_vpUUID_did_web_gaia_x_unicorn_home_com_:_did_web_gaia_x_unicorn_home_com_ {id:'_vpUUID_did_web_gaia_x_unicorn_home_com_', value:'<did:web:gaia-x.unicorn-home.com>', vpID:'vpUUID'});\n",
      "CREATE (_vpUUIDBE:_BE {id:'_vpUUIDBE', value:'BE', vpID:'vpUUID'});\n",
      "CREATE (_vpUUIDBE0762747721:_BE0762747721 {id:'_vpUUIDBE0762747721', value:'BE0762747721', vpID:'vpUUID'});\n",
      "CREATE (_vpUUID_c14n0:_c14n0 {id:'_vpUUID_c14n0', value:'_:c14n0', vpID:'vpUUID'});\n",
      "CREATE (_vpUUID2024_08_05T13_31_52_859_00_00:_2024_08_05T13_31_52_859_00_00 {id:'_vpUUID2024_08_05T13_31_52_859_00_00', value:'2024-08-05T13:31:52.859+00:00', vpID:'vpUUID'});\n",
      "CREATE (_vpUUIDgx_vatID:_gx_vatID {id:'_vpUUIDgx_vatID', value:'gx:vatID', vpID:'vpUUID'});\n",
      "CREATE (_vpUUIDhttp_ec_europa_eu_taxation_customs_vies_services_checkVatService:_http_ec_europa_eu_taxation_customs_vies_services_checkVatService {id:'_vpUUIDhttp_ec_europa_eu_taxation_customs_vies_services_checkVatService', value:'http://ec.europa.eu/taxation_customs/vies/services/checkVatService', vpID:'vpUUID'});\n",
      "CREATE (_vpUUID2024_05_15T12_10_23_900Z:_2024_05_15T12_10_23_900Z {id:'_vpUUID2024_05_15T12_10_23_900Z', value:'2024-05-15T12:10:23.900Z', vpID:'vpUUID'});\n",
      "MATCH (a),(b)\n        WHERE a.id='_vpUUID_https_trusted_issuer_com_lrn_json_' AND b.id='_vpUUID_https_w3id_org_gaia_x_development_RegistrationNumber_'\n        CREATE (a)-[r:_http_www_w3_org_1999_02_22_rdf_syntax_ns_type_ {pType:\"_http_www_w3_org_1999_02_22_rdf_syntax_ns_type_\"}]->(b);",
      "MATCH (a),(b)\n        WHERE a.id='_vpUUID_https_trusted_issuer_com_lrn_json_' AND b.id='_vpUUID_https_w3id_org_gaia_x_development_VatID_'\n        CREATE (a)-[r:_http_www_w3_org_1999_02_22_rdf_syntax_ns_type_ {pType:\"_http_www_w3_org_1999_02_22_rdf_syntax_ns_type_\"}]->(b);",
      "MATCH (a),(b)\n        WHERE a.id='_vpUUID_https_trusted_issuer_com_lrn_json_' AND b.id='_vpUUID_https_www_w3_org_2018_credentials_VerifiableCredential_'\n        CREATE (a)-[r:_http_www_w3_org_1999_02_22_rdf_syntax_ns_type_ {pType:\"_http_www_w3_org_1999_02_22_rdf_syntax_ns_type_\"}]->(b);",
      "MATCH (a),(b)\n        WHERE a.id='_vpUUID_https_trusted_issuer_com_lrn_json_' AND b.id='_vpUUID_https_trusted_issuer_com_lrn_json_cs_'\n        CREATE (a)-[r:_https_www_w3_org_2018_credentials_credentialSubject_ {pType:\"_https_www_w3_org_2018_credentials_credentialSubject_\"}]->(b);",
      "MATCH (a),(b)\n        WHERE a.id='_vpUUID_https_trusted_issuer_com_lrn_json_' AND b.id='_vpUUID_did_web_gaia_x_unicorn_home_com_'\n        CREATE (a)-[r:_https_www_w3_org_2018_credentials_issuer_ {pType:\"_https_www_w3_org_2018_credentials_issuer_\"}]->(b);",
      "MATCH (a),(b)\n        WHERE a.id='_vpUUID_https_trusted_issuer_com_lrn_json_cs_' AND b.id='_vpUUIDBE'\n        CREATE (a)-[r:_https_w3id_org_gaia_x_development_countryCode_ {pType:\"_https_w3id_org_gaia_x_development_countryCode_\"}]->(b);",
      "MATCH (a),(b)\n        WHERE a.id='_vpUUID_https_trusted_issuer_com_lrn_json_cs_' AND b.id='_vpUUIDBE0762747721'\n        CREATE (a)-[r:_https_w3id_org_gaia_x_development_vatID_ {pType:\"_https_w3id_org_gaia_x_development_vatID_\"}]->(b);",
      "MATCH (a),(b)\n        WHERE a.id='_vpUUID_https_trusted_issuer_com_lrn_json_' AND b.id='_vpUUID_c14n0'\n        CREATE (a)-[r:_https_www_w3_org_2018_credentials_evidence_ {pType:\"_https_www_w3_org_2018_credentials_evidence_\"}]->(b);",
      "MATCH (a),(b)\n        WHERE a.id='_vpUUID_https_trusted_issuer_com_lrn_json_' AND b.id='_vpUUID2024_08_05T13_31_52_859_00_00'\n        CREATE (a)-[r:_https_www_w3_org_2018_credentials_validFrom_ {pType:\"_https_www_w3_org_2018_credentials_validFrom_\"}]->(b);",
      "MATCH (a),(b)\n        WHERE a.id='_vpUUID_c14n0' AND b.id='_vpUUIDgx_vatID'\n        CREATE (a)-[r:_https_w3id_org_gaia_x_development_evidenceOf_ {pType:\"_https_w3id_org_gaia_x_development_evidenceOf_\"}]->(b);",
      "MATCH (a),(b)\n        WHERE a.id='_vpUUID_c14n0' AND b.id='_vpUUIDhttp_ec_europa_eu_taxation_customs_vies_services_checkVatService'\n        CREATE (a)-[r:_https_w3id_org_gaia_x_development_evidenceURL_ {pType:\"_https_w3id_org_gaia_x_development_evidenceURL_\"}]->(b);",
      "MATCH (a),(b)\n        WHERE a.id='_vpUUID_c14n0' AND b.id='_vpUUID2024_05_15T12_10_23_900Z'\n        CREATE (a)-[r:_https_w3id_org_gaia_x_development_executionDate_ {pType:\"_https_w3id_org_gaia_x_development_executionDate_\"}]->(b);"
    ])
  })

  it.each([
    [null, ''],
    ['', ''],
    ['.', 'root'],
    ['""', '_empty_'],
    ['https://example.org/context#MyNode', '_https_example_org_context_MyNode']
  ])('should prepare node names for graph', (input: string, output: string) => {
    expect(RdfGraphUtils.prepareNodeNameForGraph(input)).toEqual(output)
  })

  it.each([
    [null, 'childOf'],
    ['https://example.org/context#MyEdge', '_https_example_org_context_MyEdge']
  ])('should prepare edge names for graph', (input: string, output: string) => {
    expect(RdfGraphUtils.prepareEdgeNameForGraph(input)).toEqual(output)
  })

  it('should extract entity parent classes from turtle triples', async () => {
    const triples: string = readFileSync(path.resolve(__dirname, './fixtures/rdf-graph-utils/parent-and-children.ttl'), 'utf8')

    const parentClasses: ParentClasses = await RdfGraphUtils.extractEntityParentClasses(triples)

    expect(parentClasses.size).toEqual(6)
    expect(parentClasses.get('https://w3id.org/gaia-x/testing#Parent')).toEqual([])
    expect(parentClasses.get('https://w3id.org/gaia-x/testing#FirstChild')).toEqual(['https://w3id.org/gaia-x/testing#Parent'])
    expect(parentClasses.get('https://w3id.org/gaia-x/testing#GrandChild')).toEqual([
      'https://w3id.org/gaia-x/testing#FirstChild',
      'https://w3id.org/gaia-x/testing#Parent'
    ])
    expect(parentClasses.get('https://w3id.org/gaia-x/testing#GreatGrandChild')).toEqual([
      'https://w3id.org/gaia-x/testing#GrandChild',
      'https://w3id.org/gaia-x/testing#FirstChild',
      'https://w3id.org/gaia-x/testing#Parent'
    ])
    expect(parentClasses.get('https://w3id.org/gaia-x/testing#SecondChild')).toEqual(['https://w3id.org/gaia-x/testing#Parent'])
    expect(parentClasses.get('https://w3id.org/gaia-x/testing#SecondGrandChild')).toEqual([
      'https://w3id.org/gaia-x/testing#SecondChild',
      'https://w3id.org/gaia-x/testing#Parent'
    ])
  })
})
