import { DataFactory, NamedNode, Parser, Quad, Store } from 'n3'

import { OntologyException } from '../exception/ontology.exception'

const { namedNode } = DataFactory

interface RdfEntry {
  subject: string
  predicate: string
  object: string
  graph: string
}

interface Edge {
  from: string
  to: string
  predicate: string
}

export type ParentClasses = Map<string, string[]>

export class RdfGraphUtils {
  private static readonly NQUAD_REGEX: RegExp = /(_:\S+|<[^<>]+>|"([^"]*)"(?:\^\^\S+)?)/g

  private static readonly compareRDFEntryFn = (a: RdfEntry, b: RdfEntry): number => {
    if (a.object?.startsWith('<') && !b.object?.startsWith('<')) {
      return -1
    } else if (b.object?.startsWith('<')) {
      return 1
    }
    return 0
  }

  static quadsToQueries(vpUUID: string, quads: string, parentClasses: ParentClasses): string[] {
    const edges: Edge[] = []
    const nodes: string[] = []

    const rdfEntries: RdfEntry[] = quads
      .split('\n')
      .filter(quad => quad)
      .map(quad => this.quadToRDFEntry(quad))
      .sort(this.compareRDFEntryFn)

    rdfEntries.forEach(rdfEntry => {
      this.insertInNodesIfNotExisting(nodes, rdfEntry.subject)
      // In case we have a node with the ID of this literal, we point to the existing node
      this.renameObjectIfMatchingAnExistingNode(nodes, rdfEntry)
      this.insertInNodesIfNotExisting(nodes, rdfEntry.object)
      this.addParentEntityTypesToNode(nodes, edges, parentClasses, rdfEntry)

      edges.push({ from: rdfEntry.subject, to: rdfEntry.object, predicate: rdfEntry.predicate })
    })
    const queries = []
    queries.push(
      ...nodes.map(node => {
        const nodeName = this.prepareNodeNameForGraph(vpUUID + node)
        return `CREATE (${nodeName}:${this.prepareNodeNameForGraph(node)} {id:'${nodeName}', value:'${node}', vpID:'${vpUUID}'});\n`
      })
    )
    queries.push(
      ...edges.map(edge => {
        return `MATCH (a),(b)
        WHERE a.id='${this.prepareNodeNameForGraph(vpUUID + edge.from)}' AND b.id='${this.prepareNodeNameForGraph(vpUUID + edge.to)}'
        CREATE (a)-[r:${this.prepareEdgeNameForGraph(edge.predicate)} {pType:"${this.prepareEdgeNameForGraph(edge.predicate)}"}]->(b);`
      })
    )
    return queries
  }

  private static quadToRDFEntry(quad: string): RdfEntry {
    const matches: string[] = []
    for (const match of quad.matchAll(this.NQUAD_REGEX)) {
      // match[2] contains string entries without double quotes (omitting there IRI type ref)
      // match[1] is used for all other matches
      matches.push(match[2] ?? match[1])
    }

    return {
      subject: matches[0],
      predicate: matches[1],
      object: matches[2],
      graph: matches[3]
    }
  }

  static prepareNodeNameForGraph(name: string): string {
    if (!name || name === '') {
      return ''
    } else if ('.' === name) {
      return 'root'
    } else if (name === '""') {
      return '_empty_'
    }
    return this.sanitizeNames(name)
  }

  static prepareEdgeNameForGraph(name: string): string {
    if (!name) {
      return 'childOf'
    }
    return this.sanitizeNames(name)
  }

  static sanitizeNames(name: string) {
    let sanitizedName = name.replace(/[\W_]+/g, '_')
    if (!sanitizedName.startsWith('_')) {
      sanitizedName = '_' + sanitizedName
    }

    return sanitizedName
  }

  /**
   * Builds a {@link Map} from entity type to its parents from turtle triples.
   *
   * @param triples the turtle triples from which the parent classes will be extracted
   */
  static async extractEntityParentClasses(triples: string): Promise<ParentClasses> {
    const store: Store = await this.buildStore(triples)
    const parentClasses: ParentClasses = new Map()
    this.buildParentClassMap(null, store, parentClasses)

    return parentClasses
  }

  private static insertInNodesIfNotExisting(nodes: string[], nodeName: string) {
    if (nodes.indexOf(nodeName) < 0 && !!nodeName) {
      nodes.push(nodeName)
    }
  }

  /**
   * This allows JSON-LD to use string URI and not only {"@id"} structure
   * @param nodes the existing node list
   * @param rdfEntry an RDF entry composed of subject predicate object
   */
  private static renameObjectIfMatchingAnExistingNode = (nodes: string[], rdfEntry: RdfEntry) => {
    if (!rdfEntry || !rdfEntry.object) {
      return
    }
    const renamedEntry = rdfEntry.object.replace(/"(.*)"/g, '<$1>')
    if (rdfEntry.object.startsWith('"') && nodes.indexOf(renamedEntry) > -1) {
      rdfEntry.object = renamedEntry
    }
  }

  private static addParentEntityTypesToNode(nodes: string[], edges: Edge[], parentClasses: ParentClasses, rdfEntry: RdfEntry) {
    if (rdfEntry.predicate === '<http://www.w3.org/1999/02/22-rdf-syntax-ns#type>') {
      const type = rdfEntry.object.replaceAll(/[<>]/g, '')
      const parents: string[] = parentClasses.get(type)

      parents?.forEach(parent => {
        const parentNodeName = `<${parent}>`

        this.insertInNodesIfNotExisting(nodes, parentNodeName)
        edges.push({ from: rdfEntry.subject, predicate: '<http://www.w3.org/1999/02/22-rdf-syntax-ns#type>', to: parentNodeName })
      })
    }
  }

  /**
   * Parses turtle triples to convert them into an N3 {@link Store}, this enables us to conveniently query the
   * {@link Quad}s present in the store.
   *
   * @argument triples turtle triples as a {@link string}
   * @private
   */
  private static async buildStore(triples: string): Promise<Store> {
    return new Promise(resolve => {
      const parser: Parser<Quad> = new Parser()
      const store: Store = new Store()

      parser.parse(triples, (error: Error, quad: Quad) => {
        if (quad) {
          store.add(quad)
        } else if (error) {
          throw new OntologyException(`An error occurred while building ontology N3 store: ${error.message}`)
        } else {
          resolve(store)
        }
      })
    })
  }

  /**
   * Uses the N3 {@link Store} to build a {@link Map} from entity type to its parent entities. This is a recursive
   * method that traverses the whole RDF graph.
   *
   * @param classNode the {@link NamedNode} from which the traversal starts, this is <code>null</code> for the initial
   * call
   * @param store the N3 {@link Store} to query for {@link Quad}s
   * @param parentClasses the final {@link Map} containing entity types mapped to their parent entity types
   * @private
   */
  private static buildParentClassMap(classNode: NamedNode, store: Store, parentClasses: ParentClasses) {
    const quads: Quad[] = store
      .match(classNode, namedNode('http://www.w3.org/2000/01/rdf-schema#subClassOf'))
      .filter((subClassOfQuad: Quad) => subClassOfQuad.object instanceof NamedNode)
      .toArray()

    if (quads.length) {
      quads.forEach((subClassOfQuad: Quad) => {
        const child: NamedNode = subClassOfQuad.subject
        const parent: NamedNode = subClassOfQuad.object

        const parents: string[] = []
        parents.push(parent.id)

        if (!parentClasses.has(parent.id)) {
          this.buildParentClassMap(parent, store, parentClasses)
        }

        parentClasses.get(parent.id).forEach(grandParent => parents.push(grandParent))
        parentClasses.set(child.id, parents)
      })
    } else {
      if (classNode) {
        parentClasses.set(classNode.id, [])
      }
    }
  }
}
