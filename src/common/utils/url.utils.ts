export class UrlUtils {
  static isValid(url: string): boolean {
    try {
      new URL(url)
    } catch (_) {
      return false
    }

    return true
  }
}
