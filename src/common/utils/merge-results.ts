import { FilterValidationResult } from '../../vp-validation/filter'
import { ValidationResult } from '../dto'

export function mergeResults(...results: ValidationResult[]): ValidationResult {
  if (results && results.length > 0) {
    const resultArray = results.map(res => res.results)
    const res = resultArray.reduce((p, c) => c.concat(p))

    return {
      conforms: results.filter(r => !r.conforms).length == 0,
      results: res
    }
  }
  return { conforms: true, results: [] }
}

export function mergeFilterResults(...results: FilterValidationResult[]): FilterValidationResult {
  if (results && results.length > 0) {
    const resultArray = results.map(res => res.results)
    const res = resultArray.reduce((p, c) => c.concat(p))
    const validatedCriteria = results
      .flatMap(res => res.validatedCriteria)
      .map(criteria => `https://w3id.org/gaia-x/specs/cd24.06/criterion/${criteria}`)
    return {
      conforms: results.filter(r => !r.conforms).length == 0,
      results: res,
      validatedCriteria
    }
  }
  return { conforms: true, results: [], validatedCriteria: [] }
}
