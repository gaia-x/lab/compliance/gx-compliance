import { CredentialSubjectDto, VerifiableCredentialDto } from '../dto'

export class VerifiableCredentialUtils {
  static extractGxTypes(verifiableCredential: VerifiableCredentialDto<CredentialSubjectDto>): string[] {
    const credentialTypes = this.extractCredentialSubjectGxType(verifiableCredential)

    const credentialSubject = verifiableCredential.credentialSubject

    if (Array.isArray(credentialSubject)) {
      const credentialSubjectTypes = credentialSubject.filter(cs => cs).flatMap(cs => this.extractCredentialSubjectGxType(cs))
      return [...credentialSubjectTypes, ...credentialTypes]
    }

    if (!credentialSubject) {
      return credentialTypes
    }

    return [...this.extractCredentialSubjectGxType(credentialSubject), ...credentialTypes]
  }

  private static extractCredentialSubjectGxType(credentialSubject: CredentialSubjectDto | VerifiableCredentialDto<CredentialSubjectDto>): string[] {
    const type = credentialSubject.type ?? credentialSubject['@type']
    if (!type) {
      return []
    }
    if (Array.isArray(type)) {
      return type.filter(typ => typ.indexOf('VerifiableCredential') === -1)
    }
    return [type]
  }
}
