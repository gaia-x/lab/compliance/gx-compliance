import { errorMessageDocumentation } from './error-handling.util'

describe('errorMessageDocumentation', () => {
  test('should return the correct message for a given criterion', () => {
    const criterion = 'P1.1.5'
    const expectedMessage =
      'P1.1.5 - More details about this criterion here: https://gitlab.com/gaia-x/lab/compliance/gx-compliance/-/blob/development/docs/labelling-criteria.md#criterion-p115'
    expect(errorMessageDocumentation(criterion)).toBe(expectedMessage)
  })

  test('should transform P3.3 to criterion-p33', () => {
    const criterion = 'P3.3'
    const expectedMessage =
      'P3.3 - More details about this criterion here: https://gitlab.com/gaia-x/lab/compliance/gx-compliance/-/blob/development/docs/labelling-criteria.md#criterion-p33'
    expect(errorMessageDocumentation(criterion)).toBe(expectedMessage)
  })

  test('should handle patterns without dots', () => {
    const criterion = 'P4'
    const expectedMessage =
      'P4 - More details about this criterion here: https://gitlab.com/gaia-x/lab/compliance/gx-compliance/-/blob/development/docs/labelling-criteria.md#criterion-p4'
    expect(errorMessageDocumentation(criterion)).toBe(expectedMessage)
  })
})
