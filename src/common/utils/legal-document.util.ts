import { LegalDocument } from '../model/legal-document'
import { graphValueFormat } from './graph-value-format'
import { UrlUtils } from './url.utils'

export interface LegalDocumentProperty {
  type: string
  value: string | any
}

export class LegalDocumentUtil {
  static mapFromProperties(properties: LegalDocumentProperty[], contextVersion: string): LegalDocument {
    const legalDocument: LegalDocument = { type: '', mimeTypes: [], url: '', involvedParties: [], governingLawCountries: [] }

    for (const property of properties) {
      if (property.type === `_https_w3id_org_gaia_x_${contextVersion}_url_`) {
        legalDocument.url = property.value
      }

      if (property.type === '_http_www_w3_org_1999_02_22_rdf_syntax_ns_type_') {
        const value = graphValueFormat(property.value)
        if (value !== 'www.w3.org/2018/credentials#VerifiableCredential' && value !== `w3id.org/gaia-x/${contextVersion}#LegalDocument`) {
          // todo: discuss if we should handle compliance cases where the user fills multiple subtypes of legal document in one object (instead of referencing the same legal document in multiple sub-typed objects)
          legalDocument.type = value
        }
      }

      if (property.type === `_https_w3id_org_gaia_x_${contextVersion}_mimeTypes_`) {
        legalDocument.mimeTypes.push(property.value)
      }

      if (property.type === `_https_w3id_org_gaia_x_${contextVersion}_governingLawCountries_`) {
        legalDocument.governingLawCountries.push(property.value)
      }

      if (property.type === `_https_w3id_org_gaia_x_${contextVersion}_involvedParties_`) {
        legalDocument.involvedParties.push(property.value)
      }
    }

    return legalDocument
  }

  static isValid(legalDocument: LegalDocument): boolean | void {
    if (!legalDocument.url || !legalDocument.url.trim()) {
      throw new Error(`Legal document url '${legalDocument.url}' is invalid`)
    }

    if (!UrlUtils.isValid(legalDocument.url)) {
      throw new Error(`Legal document url '${legalDocument.url}' is invalid`)
    }

    return true
  }
}
