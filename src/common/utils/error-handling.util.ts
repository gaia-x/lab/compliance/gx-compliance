export function errorMessageDocumentation(criterion: string): string {
  const criterionAnchor: string = convertCriterionToAnchor(criterion)
  const docUrl = 'https://gitlab.com/gaia-x/lab/compliance/gx-compliance/-/blob/development/docs/labelling-criteria.md'
  return `${criterion} - More details about this criterion here: ${docUrl}#${criterionAnchor}`
}

function convertCriterionToAnchor(criterion) {
  return 'criterion-' + criterion.toLowerCase().replace(/\./g, '')
}
