import { UrlUtils } from './url.utils'

describe('urlUtils', () => {
  it.each(['https://example.org/test', 'did:web:example.org', 'did:web:example.org:test', 'ftp://example.org/storage'])(
    'should return true when URL is valid',
    (url: string) => {
      expect(UrlUtils.isValid(url)).toBe(true)
    }
  )

  it.each([null, '', ' ', 'not-a-uri'])('should return false if URL is invalid', (url: string) => {
    expect(UrlUtils.isValid(url)).toBe(false)
  })
})
