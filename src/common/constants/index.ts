import { VerifiableCredentialDto, VerifiablePresentationDto } from '../dto'

export const VC_JWT_MIME_TYPE = 'application/vc+jwt'
export const VP_JWT_MIME_TYPE = 'application/vp+jwt'
export const DEFAULT_VC_LIFE_EXPECTANCY_IN_DAYS = '90'
export const CD_24_06_RULES_VERSION = 'CD24.06'
export const CTY_VALUE = 'vc'
export const TYP_VALUE = 'vc+jwt'
export const X509_CERTIFICATE = 'X509_CERTIFICATE'

export type VerifiablePresentation = VerifiablePresentationDto<VerifiableCredentialDto<any>>

export const EEA_COUNTRY_NAME_ALPHA2 = [
  'AT',
  'BE',
  'BG',
  'CY',
  'CZ',
  'HR',
  'DE',
  'DK',
  'ES',
  'EE',
  'FI',
  'FR',
  'GR',
  'HU',
  'IE',
  'IS',
  'IT',
  'LV',
  'LI',
  'LT',
  'LU',
  'MT',
  'NL',
  'NO',
  'PL',
  'PT',
  'RO',
  'SK',
  'SI',
  'SE'
]

/**
 * List of adequately protected third countries according to GDPR's Chapter V.
 *
 * The list has been extracted from <a href="https://commission.europa.eu/law/law-topic/data-protection/international-dimension-data-protection/adequacy-decisions_en">https://commission.europa.eu/law/law-topic/data-protection/international-dimension-data-protection/adequacy-decisions_en</a>
 * @private
 */
export const PROTECTED_THIRD_COUNTRY_CODES: string[] = [
  'AD', // Andorra
  'AR', // Argentina
  'CA', // Canada
  'FO', // Faroe Islands
  'GG', // Guernsey
  'IL', // Israel
  'IM', // Isle of Man
  'JP', // Japan
  'JE', // Jersey
  'NZ', // New Zealand
  'KR', // Republic of Korea
  'CH', // Switzerland
  'GB', // United Kingdom
  'US', // United States
  'UY' // Uruguay
]
