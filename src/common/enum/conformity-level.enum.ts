export enum ConformityLevelEnum {
  STANDARD_COMPLIANCE,
  LABEL_LEVEL_1,
  LABEL_LEVEL_2,
  LABEL_LEVEL_3
}

export function ConformityLevelEnumToString(conformityLevelEnum: ConformityLevelEnum): string {
  switch (conformityLevelEnum) {
    case ConformityLevelEnum.STANDARD_COMPLIANCE:
      return 'SC'
    case ConformityLevelEnum.LABEL_LEVEL_1:
      return 'L1'
    case ConformityLevelEnum.LABEL_LEVEL_2:
      return 'L2'
    case ConformityLevelEnum.LABEL_LEVEL_3:
      return 'L3'
    default:
      return 'N/A'
  }
}
