import { HttpStatus } from '@nestjs/common'
import { ApiProperty } from '@nestjs/swagger'

export class ErrorResponseDto {
  @ApiProperty({
    description: 'The main error message describing the error globally',
    example: 'Something went wrong on module #1'
  })
  message: string

  @ApiProperty({
    required: false,
    description: 'Details about the error',
    example: `The flux capacitor isn't fully charged`
  })
  error?: string

  @ApiProperty({
    description: 'The corresponding HTTP status code',
    example: 500
  })
  statusCode: HttpStatus

  @ApiProperty({
    description: 'Details about the error in the form of an array',
    example: [`The flux capacitor isn't fully charged`, `You left the front door open when leaving`]
  })
  errors: string[]
}
