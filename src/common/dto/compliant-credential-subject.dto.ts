/**
 * DTO class representing a compliance response credential subject signed by Gaia-X
 */
export class CompliantCredentialSubjectDto {
  id: string
  'gx:labelLevel': string
  'gx:engineVersion': string
  'gx:rulesVersion': string
  'gx:validatedCriteria': string[]
  'gx:compliantCredentials': ComplianceEvidenceDTO[]
}

export class ComplianceEvidenceDTO {
  id: string
  type: string
  'gx:digestSRI': string
}
