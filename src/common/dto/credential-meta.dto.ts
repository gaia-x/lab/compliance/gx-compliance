import { ApiProperty } from '@nestjs/swagger'

export abstract class VerifiableCredentialDto<T extends CredentialSubjectDto> {
  @ApiProperty({
    description: 'The context to be used for the credential.'
  })
  public '@context': string[] | any

  @ApiProperty({
    description: 'The type of the credential.'
  })
  public type: string | string[]

  @ApiProperty({
    description: 'The identifier of the credential. Should be resolvable',
    required: false
  })
  public id?: string

  @ApiProperty({
    description: 'The claims of the credential.'
  })
  public credentialSubject: T | T[]

  @ApiProperty({
    description: 'The identifier of the issuer of the credential.'
  })
  public issuer: string

  @ApiProperty({
    description: 'The expiration date of the credential.'
  })
  public validUntil?: string

  @ApiProperty({
    description: 'The issuance date of the credential.'
  })
  public validFrom: string

  @ApiProperty({
    description: 'The name of the credential.'
  })
  public name?: string

  @ApiProperty({
    description: 'The description of the credential.'
  })
  public description?: string
}

export abstract class CredentialSubjectDto {
  @ApiProperty({
    description: 'The identifier of the credential subject.'
  })
  public 'id': string

  @ApiProperty({
    description: 'The type of the credential subject'
  })
  public type?: string | string[]
}
