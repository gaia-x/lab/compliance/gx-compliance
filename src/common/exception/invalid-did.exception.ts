import { BadRequestException } from '@nestjs/common'

export class InvalidDidException extends BadRequestException {
  constructor(description: string) {
    super(`Invalid DID`, description)
  }
}
