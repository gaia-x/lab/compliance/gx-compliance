import { InternalServerErrorException } from '@nestjs/common'

export class OntologyException extends InternalServerErrorException {
  constructor(description: string) {
    super(`Ontology exception`, description)
  }
}
