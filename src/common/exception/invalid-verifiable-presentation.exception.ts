import { HttpException, HttpStatus } from '@nestjs/common'

export class InvalidVerifiablePresentationException extends HttpException {
  constructor(message: string, errors?: string | string[]) {
    super(
      {
        message,
        error: 'Invalid verifiable presentation',
        statusCode: HttpStatus.BAD_REQUEST,
        errors: Array.isArray(errors) ? errors : [errors]
      },
      HttpStatus.BAD_REQUEST
    )
  }
}
