import { InternalServerErrorException } from '@nestjs/common'

export class ShaclShapeException extends InternalServerErrorException {
  constructor(description: string) {
    super(`SHACL Shape Error`, description)
  }
}
