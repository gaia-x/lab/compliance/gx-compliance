import { BadRequestException } from '@nestjs/common'

export class InvalidCertificateException extends BadRequestException {
  constructor(description: string) {
    super(`Invalid Certificate`, description)
  }
}
