import { HttpModule, HttpService } from '@nestjs/axios'
import { Test, TestingModule } from '@nestjs/testing'

import { DidResolver } from '@gaia-x/json-web-signature-2020'
import { importJWK } from 'jose'
import * as process from 'process'
import { of } from 'rxjs'

import * as packageJson from '../../../package.json'
import { x509VerificationMethod } from '../../identity/provider/x509-verification-method-identifier.provider'
import { RegistryService } from '../../vp-validation/service/registry.service'
import { ShaclService } from '../../vp-validation/service/shacl.service'
import { CommonModule } from '../common.module'
import { CD_24_06_RULES_VERSION } from '../constants'
import { ComplianceCredentialDto, CredentialSubjectDto, VerifiableCredentialDto, VerifiablePresentationDto } from '../dto'
import { ConformityLevelEnum } from '../enum/conformity-level.enum'
import { ComplianceCredentialMapper } from '../mapper/compliance-credential.mapper'
import { EvidenceService } from './evidence.service'
import { TimeService } from './time.service'

const didResolverMock = {
  resolve: jest.fn()
}

const httpServiceMock = {
  get: jest.fn()
}

const registryServiceMock = {
  isValidCertificateChain: jest.fn()
}

const shaclServiceMock = {
  getContextVersion: () => 'development'
}
// TODO: replace this cert & key by generated ones and rework all tests.

const privateKeyJwk = {
  alg: 'PS256',
  kty: 'RSA',
  n: '2AKm73KFalgCZq57KUHbX70fFmVScGB_Pa6_T4_f4YQDENyI2TWyhFu1HABXnHoZmP_JBouHhKkRg7-QPKidOOE1yyrutjmBHZ8sGI2j9KGTqsMna4DU7sOfWoqrlJWc8-RGi5z58uO3vA2zEoNh0qpoDuKGaqtSX2efzChH6mAnmhEu-4JcixkNES8AzcYX1UB5fPu4Vs64gIRgbke0h8f0riFbh5RjkM0eMka9RV0iLjU1j24Y59EE8DbAlHTM5JtHxlo6Hv5_iiw-WiFEcuEobneFIGQP6WiJq5Fql2vV7X7YEzV5ydQjuRCy4hDW_i6JEt0Y5qlP1AoYeb-isw',
  e: 'AQAB',
  d: 'TnioLFRuIzPVq3e3RkWmbCFIKdqlGC32C3JwDXc23bYXZwi2rjHTqEGoiYOWUEILConhfX8yu_6vXi05ONAITaGC6UuvbIN3ZEtuuXy7EyOfgWlj6KnksNhgC4RU0KWJXxCOkCl2o8YV-TsA0rjn0KAXLVEdg6K5Se9bHc-EUJ_y4lfiI7wR2gfoHGo3K2_44Xwf1guwceYVBKLyvZ5-861SzFg1WkASRSdMKpMrFH_CUgK3guvTtgDyMm_zWj_WGVZcU_m3zG7TOvykr569bYdb4MJstWUeqNzurjmMeTlajo9LaYNN6dHhKDM_kcoj85s4Qk-aKqUFbzZwxx6h',
  p: '7u3mH0Xux2JOmjT1Q32R-zwKomEGrjLPqNxDYLFG7NMCha_07rc2rsHTROtJwrM-K5CVW7Y1U_iFe5FP6AKCu056vMyi_ktz78KR7FjnuBAaRJx9UAQ0qscy_hsFmsZde7eS-Q4xMPOqpqL7CK5e62QfenCM4cTkMFVNPqYyt5E',
  q: '53GOzFDHTrpoBFy3duMNLv3NOVKK_ceJ_1q0tGCaR4K_K56-TJ2OFnkGDLKxvDVx_J5CKD1QnKgw9CxMSgJBYHSd5rw7PHLIKBqzcS9LpQcy1O_uXRyufcHpl9TuRwM63vFXooms-paCJRBcHnLnBRagelwJBwWQThK9MfBJvAM',
  dp: 'y8Fjew33aF9kqss40dp3MKpuYzWdLdc1EkrsxrvHwVTdlMaOeTkTUAsJMX_5wil7fklppfGIHtkUdGFipHfnpvZxYyqcYYJVF4V1Tfoved95l0Ng9mjvxSflS6AdsnUx6byOgQhiWN2jjUH2FNwnJFSZJ6Bt9GclNja4jhHLtJE',
  dq: 'TBGCwZx7JWEW9vgSRLzzhFJetUxv6mE-9fK2GeL2UjKi6o9ONJhELxav6lSBHj7irAjH7bnZWOPe0yIMIPcEzdMGYuPTBFcleijseXL2BdOL3XjOe0QGBcdKI2EUv7pMCyJ_jyh49hOpyszJuihBzeZV8GF3hhtKBp8aF-PGGEk',
  qi: 'qUGUPkS0MPP4BhSULbfNJxuA7sQYf2VN0iIPxNks_rjYkdUqXXZj4hvdRbYjRThDHkIlXItpZcQvfXiNaVUCP61E01s05q7nRZYctIsONdbJLt2gcQgbYIJqXQxhjXagWvDngnSikf3w_zx-sTuEnubZftiWgu3XRbHgDDDMJQo'
}

const certificateRaw: string =
  '-----BEGIN CERTIFICATE-----\n' +
  'MIIDmTCCAoGgAwIBAgIUb5XHRsa2R2DbB8djidTTTdXaXsUwDQYJKoZIhvcNAQEL\n' +
  'BQAwXDELMAkGA1UEBhMCRlIxDDAKBgNVBAgMA05BUTEPMA0GA1UEBwwGUGVzc2Fj\n' +
  'MQ8wDQYDVQQKDAZHYWlhLVgxHTAbBgNVBAMMFGNvbXBsaWFuY2UuZ2FpYS14LmV1\n' +
  'MB4XDTI0MDIwNzE1MTMwNFoXDTI0MDMwODE1MTMwNFowXDELMAkGA1UEBhMCRlIx\n' +
  'DDAKBgNVBAgMA05BUTEPMA0GA1UEBwwGUGVzc2FjMQ8wDQYDVQQKDAZHYWlhLVgx\n' +
  'HTAbBgNVBAMMFGNvbXBsaWFuY2UuZ2FpYS14LmV1MIIBIjANBgkqhkiG9w0BAQEF\n' +
  'AAOCAQ8AMIIBCgKCAQEA2AKm73KFalgCZq57KUHbX70fFmVScGB/Pa6/T4/f4YQD\n' +
  'ENyI2TWyhFu1HABXnHoZmP/JBouHhKkRg7+QPKidOOE1yyrutjmBHZ8sGI2j9KGT\n' +
  'qsMna4DU7sOfWoqrlJWc8+RGi5z58uO3vA2zEoNh0qpoDuKGaqtSX2efzChH6mAn\n' +
  'mhEu+4JcixkNES8AzcYX1UB5fPu4Vs64gIRgbke0h8f0riFbh5RjkM0eMka9RV0i\n' +
  'LjU1j24Y59EE8DbAlHTM5JtHxlo6Hv5/iiw+WiFEcuEobneFIGQP6WiJq5Fql2vV\n' +
  '7X7YEzV5ydQjuRCy4hDW/i6JEt0Y5qlP1AoYeb+iswIDAQABo1MwUTAdBgNVHQ4E\n' +
  'FgQU4P6iTnO5b5O5gsjPSpK+6g0AhN4wHwYDVR0jBBgwFoAU4P6iTnO5b5O5gsjP\n' +
  'SpK+6g0AhN4wDwYDVR0TAQH/BAUwAwEB/zANBgkqhkiG9w0BAQsFAAOCAQEAba6B\n' +
  'zqmBSfZIgWisXChHTF031ZylWDJ12nPz0PV0j4X+CKRYgRCJYE+JR4gFfkHZRGnM\n' +
  'RSScwQwBiIA1w5H1yJ1/OAosuxhwbu6XV8Z4f0BKoMShP9MeIcCeCBcDjxTBk0lf\n' +
  'hYudeh72i/NJfsNJC3oD+Oj2wkyWgT+dOrSHywm/HCioceNW14cY0Efxu4Y0Kdjh\n' +
  '5fpEBzLSabJklozCscuKfmuhQssy+97DN/yaqZ4ryapI2p2v9sZu41MkVc2wj18k\n' +
  'CW1aQ8klL/uU4bhyVOUkVTtFgEU+5RM+2b+RWAquxOCXPQzN6OEHh1rw004MvJE8\n' +
  'V/npm/ah2Zeaw76nhQ==\n' +
  '-----END CERTIFICATE-----\n'

const gaiaXVerifiableCredential: VerifiableCredentialDto<any> = {
  '@context': ['https://www.w3.org/2018/credentials/v1', 'https://w3id.org/gaia-x/development#'],
  type: ['VerifiableCredential'],
  id: 'https://wizard.lab.gaia-x.eu/api/credentials/2d37wbGvQzbAQ84yRouh2m2vBKkN8s5AfH9Q75HZRCUQmJW7yAVSNKzjJj6gcjE2mDNDUHCichXWdMH3S2c8AaDLm3kXmf5R8DFPWTYo5iRYkn8kvgU3AjMXc2qTbhuMHCpucKGgT1ZMkcHUygZkt11iD3T8VJNKYwsdk4MGoZwdqoFUuTKVcsXVTBA4ofD1Dtqzjavyng5WUpvJf4gRyfGkMvYYuHCgay8TK8Dayt6Rhcs3r2d1gRCg2UV419S9CpWZGwKQNEXdYbaB2eTiNbQ83KMd4mj1oSJgF7LLDZLJtKJbhwLzR3x35QUqEGevRxnRDKoPdHrEZN7r9TVAmvr9rt7Xq8eB4zGMTza59hisEAUaHsmWQNaVDorqFyZgN5bXswMK1irVQ5SVR9osCCRrKUKkntxfakjmSqapPfveMP39vkgTXfEhsfLUZXGwFcpgLpWxWRn1QLnJY11BVymS7DyaSvbSKotNFQxyV6vghfM2Jetw1mLxU5qsQqDYnDYJjPZQSmkwxjX3yenPVCz6N2ox83tj9AuuQrzg5p2iukNdunDd2QCsHaMEtTq9JVLzXtWs2eZbPkxCBEQwoKTGGVhKu5yxZjCtQGc?vcid=brown-horse',
  issuer:
    'did:web:wizard.lab.gaia-x.eu:api:credentials:2d37wbGvQzbAQ84yRouh2m2vBKkN8s5AfH9Q75HZRCUQmJW7yAVSNKzjJj6gcjE2mDNDUHCichXWdMH3S2c8AaDLm3kXmf5R8DFPWTYo5iRYkn8kvgU3AjMXc2qTbhuMHCpucKGgT1ZMkcHUygZkt11iD3T8VJNKYwsdk4MGoZwdqoFUuTKVcsXVTBA4ofD1Dtqzjavyng5WUpvJf4gRyfGkMvYYuHCgay8TK8Dayt6Rhcs3r2d1gRCg2UV419S9CpWZGwKQNEXdYbaB2eTiNbQ83KMd4mj1oSJgF7LLDZLJtKJbhwLzR3x35QUqEGevRxnRDKoPdHrEZN7r9TVAmvr9rt7Xq8eB4zGMTza59hisEAUaHsmWQNaVDorqFyZgN5bXswMK1irVQ5SVR9osCCRrKUKkntxfakjmSqapPfveMP39vkgTXfEhsfLUZXGwFcpgLpWxWRn1QLnJY11BVymS7DyaSvbSKotNFQxyV6vghfM2Jetw1mLxU5qsQqDYnDYJjPZQSmkwxjX3yenPVCz6N2ox83tj9AuuQrzg5p2iukNdunDd2QCsHaMEtTq9JVLzXtWs2eZbPkxCBEQwoKTGGVhKu5yxZjCtQGc',
  validFrom: '2023-07-12T08:58:07.859Z',
  validUntil: '2123-07-12T08:58:07.859Z',
  credentialSubject: {
    type: 'gx:LegalParticipant',
    registrationNumber: [
      {
        type: 'vatID',
        number: 'FR123456789'
      }
    ],
    headquarterAddress: {
      countrySubdivisionCode: 'BE-BRU'
    },
    legalAddress: {
      countrySubdivisionCode: 'BE-BRU'
    },
    id: 'https://wizard.lab.gaia-x.eu/api/credentials/2d37wbGvQzbAQ84yRouh2m2vBKkN8s5AfH9Q75HZRCUQmJW7yAVSNKzjJj6gcjE2mDNDUHCichXWdMH3S2c8AaDLm3kXmf5R8DFPWTYo5iRYkn8kvgU3AjMXc2qTbhuMHCpucKGgT1ZMkcHUygZkt11iD3T8VJNKYwsdk4MGoZwdqoFUuTKVcsXVTBA4ofD1Dtqzjavyng5WUpvJf4gRyfGkMvYYuHCgay8TK8Dayt6Rhcs3r2d1gRCg2UV419S9CpWZGwKQNEXdYbaB2eTiNbQ83KMd4mj1oSJgF7LLDZLJtKJbhwLzR3x35QUqEGevRxnRDKoPdHrEZN7r9TVAmvr9rt7Xq8eB4zGMTza59hisEAUaHsmWQNaVDorqFyZgN5bXswMK1irVQ5SVR9osCCRrKUKkntxfakjmSqapPfveMP39vkgTXfEhsfLUZXGwFcpgLpWxWRn1QLnJY11BVymS7DyaSvbSKotNFQxyV6vghfM2Jetw1mLxU5qsQqDYnDYJjPZQSmkwxjX3yenPVCz6N2ox83tj9AuuQrzg5p2iukNdunDd2QCsHaMEtTq9JVLzXtWs2eZbPkxCBEQwoKTGGVhKu5yxZjCtQGc#9894e9b0a38aa105b50bb9f4e7d0975641273416e70f166f4bd9fd1b00dfe81d'
  }
}

const mockedNtpTime: Date = new Date('2024-02-08T10:39:45.841Z')

describe('EvidenceService', () => {
  let originalBaseUrl: string
  let originalLifeExpectancy: string

  let proofService: EvidenceService
  let timeService: TimeService

  beforeAll(async () => {
    originalBaseUrl = process.env.BASE_URL
    originalLifeExpectancy = process.env.vcLifeExpectancyInDays

    process.env.BASE_URL = 'https://example.org'
    process.env.vcLifeExpectancyInDays = '12'
    process.env.USE_OFFLINE_DOCUMENT_LOADER = 'true'

    const moduleFixture: TestingModule = await Test.createTestingModule({
      imports: [CommonModule, HttpModule]
    })
      .overrideProvider(DidResolver)
      .useValue(didResolverMock)
      .overrideProvider(HttpService)
      .useValue(httpServiceMock)
      .overrideProvider(RegistryService)
      .useValue(registryServiceMock)
      .overrideProvider('PRIVATE_KEY')
      .useValue(await importJWK(privateKeyJwk))
      .overrideProvider(ShaclService)
      .useValue(shaclServiceMock)
      .compile()

    proofService = moduleFixture.get<EvidenceService>(EvidenceService)
    timeService = moduleFixture.get<TimeService>(TimeService)

    jest.spyOn(timeService, 'getNtpTime').mockResolvedValue(mockedNtpTime)
  })

  afterAll(() => {
    process.env.BASE_URL = originalBaseUrl
    process.env.vcLifeExpectancyInDays = originalLifeExpectancy
  })

  beforeEach(() => {
    jest.spyOn(didResolverMock, 'resolve').mockImplementation(async () => {
      return {
        '@context': ['https://www.w3.org/ns/did/v1'],
        id: 'did:web:example.org',
        verificationMethod: [
          {
            id: `did:web:example.org#${x509VerificationMethod}`,
            type: 'JsonWebKey2020',
            controller: 'did:web:example.org',
            publicKeyJwk: {
              alg: 'PS256',
              kty: 'RSA',
              n: '2AKm73KFalgCZq57KUHbX70fFmVScGB_Pa6_T4_f4YQDENyI2TWyhFu1HABXnHoZmP_JBouHhKkRg7-QPKidOOE1yyrutjmBHZ8sGI2j9KGTqsMna4DU7sOfWoqrlJWc8-RGi5z58uO3vA2zEoNh0qpoDuKGaqtSX2efzChH6mAnmhEu-4JcixkNES8AzcYX1UB5fPu4Vs64gIRgbke0h8f0riFbh5RjkM0eMka9RV0iLjU1j24Y59EE8DbAlHTM5JtHxlo6Hv5_iiw-WiFEcuEobneFIGQP6WiJq5Fql2vV7X7YEzV5ydQjuRCy4hDW_i6JEt0Y5qlP1AoYeb-isw',
              e: 'AQAB',
              x5u: 'https://example.org/.well-known/cert.crt'
            }
          }
        ],
        assertionMethod: [`did:web:example.org#${x509VerificationMethod}`]
      }
    })

    jest.spyOn(httpServiceMock, 'get').mockImplementation(() => {
      return of({
        data: certificateRaw,
        headers: {
          'Content-type': 'application/pkcs8'
        }
      })
    })

    jest.spyOn(registryServiceMock, 'isValidCertificateChain').mockReturnValue(true)
  })

  it('should be defined', () => {
    expect(proofService).toBeDefined()
  })

  it('should create a compliance credential', async () => {
    process.env.vcLifeExpectancyInDays = '90'
    jest.spyOn(shaclServiceMock, 'getContextVersion').mockReturnValue('testVersion')
    const verifiablePresentation: VerifiablePresentationDto<VerifiableCredentialDto<CredentialSubjectDto>> = {
      '@context': ['https://www.w3.org/2018/credentials/v1'],
      '@type': ['VerifiablePresentation'],
      verifiableCredential: [gaiaXVerifiableCredential]
    }

    const expectedID = 'https://example.org/myvc'

    const result: VerifiableCredentialDto<ComplianceCredentialDto> = await proofService.createComplianceCredential(
      verifiablePresentation,
      expectedID,
      { conforms: true, results: [], validatedCriteria: ['P1.1.1', 'P2.1.1', 'P3.1.13'] },
      ConformityLevelEnum.STANDARD_COMPLIANCE
    )

    expect(result).toEqual({
      '@context': ['https://www.w3.org/ns/credentials/v2', `https://w3id.org/gaia-x/testVersion#`],
      type: ['VerifiableCredential', 'gx:LabelCredential'],
      id: expectedID,
      issuer: 'did:web:example.org',
      validFrom: '2024-02-08T10:39:45.841Z',
      validUntil: '2024-04-12T12:58:38.000Z',
      credentialSubject: {
        id: `${expectedID}#cs`,
        'gx:engineVersion': packageJson.version,
        'gx:rulesVersion': CD_24_06_RULES_VERSION,
        'gx:labelLevel': 'SC',
        'gx:validatedCriteria': ['P1.1.1', 'P2.1.1', 'P3.1.13'],
        'gx:compliantCredentials': [new ComplianceCredentialMapper().map(gaiaXVerifiableCredential)]
      }
    })
  })
})
