import { HttpService } from '@nestjs/axios'
import { Injectable, Logger } from '@nestjs/common'
import { Cron, CronExpression } from '@nestjs/schedule'

import { DidResolver } from '@gaia-x/json-web-signature-2020'
import { DIDDocument } from 'did-resolver'
import { JsonWebKey } from 'did-resolver/src/resolver'
import * as jose from 'jose'
import { KeyLike, importJWK } from 'jose'
import { firstValueFrom } from 'rxjs'

import { InvalidCertificateException } from '../exception/invalid-certificate.exception'
import { InvalidDidException } from '../exception/invalid-did.exception'

@Injectable()
export class DIDService {
  private didCache: Record<string, DIDDocument>
  private certificateCache: Record<string, string>
  private readonly logger: Logger = new Logger(DIDService.name)

  constructor(private readonly didResolver: DidResolver, private readonly httpService: HttpService) {
    this.didCache = {}
    this.certificateCache = {}
  }

  @Cron(CronExpression.EVERY_2_HOURS)
  public cleanupCaches() {
    this.logger.log('Clearing didCache and certificateCache')
    this.didCache = {}
    this.certificateCache = {}
  }

  public async getDIDDocumentFromDID(did: string): Promise<DIDDocument> {
    const cachedDID = this.didCache[did]
    if (!!cachedDID) {
      return cachedDID
    }
    let didDocument: DIDDocument
    try {
      didDocument = await this.didResolver.resolve(did)
    } catch (error) {
      this.logger.warn(`Unable to load DID from ${did}`, error)
      throw new InvalidDidException(`Unable to retrieve your did ${did}: ${error.message}`)
    }
    if (!didDocument?.verificationMethod || didDocument?.verificationMethod?.constructor !== Array) {
      this.logger.warn(`DID ${did} does not contain verificationMethod array`)
      throw new InvalidDidException(`DID ${did} does not contain the verificationMethod array`)
    }
    this.didCache[did] = didDocument
    return didDocument || undefined
  }

  public getJWKFromDID(didDocument: DIDDocument, verificationMethodName: string): JsonWebKey {
    const verificationMethod = didDocument.verificationMethod.find(verificationMethod => verificationMethod.id === verificationMethodName)
    if (!verificationMethod) {
      throw new InvalidDidException(`Unable to find verificationMethod ${verificationMethodName} in the DID ${didDocument.id}`)
    }
    return didDocument.verificationMethod.find(verificationMethod => verificationMethod.id === verificationMethodName)?.publicKeyJwk
  }

  public async getPublicKeyFromJWK(jwk: JsonWebKey): Promise<KeyLike> {
    return <KeyLike>await importJWK(jwk)
  }

  public async checkx5cMatchesPublicKey(publicKey: KeyLike, x5c: string[]) {
    try {
      const spki = await jose.exportSPKI(publicKey)
      const certificatePem = x5c
        .map(cert => (cert.indexOf('-----BEGIN CERTIFICATE-----') > -1 ? cert : `-----BEGIN CERTIFICATE-----${cert}-----END CERTIFICATE-----`))
        .join()
      const x509 = await jose.importX509(certificatePem, null)
      const spkiX509 = await jose.exportSPKI(x509)
      return { conforms: spki === spkiX509, cert: certificatePem }
    } catch (error) {
      throw new InvalidCertificateException('Could not confirm X509 public key with certificate chain.' + error?.message)
    }
  }
  public async checkx5uMatchesPublicKey(publicKey: KeyLike, x5uURL: string) {
    try {
      const spki = await jose.exportSPKI(publicKey)
      const certificatePem = await this.loadCertificatesRaw(x5uURL)
      const x509 = await jose.importX509(certificatePem, null)
      const spkiX509 = await jose.exportSPKI(x509)

      return { conforms: spki === spkiX509, cert: certificatePem }
    } catch (error) {
      throw new InvalidCertificateException('Could not confirm X509 public key with certificate chain.' + error?.message)
    }
  }

  public async loadCertificatesRaw(url: string): Promise<string> {
    const certificateCached = this.certificateCache[url]
    if (!!certificateCached) {
      return certificateCached
    }
    try {
      const response = await firstValueFrom(this.httpService.get(url))
      const cert = response.data || undefined
      this.certificateCache[url] = cert
      return cert
    } catch (error) {
      this.logger.warn(`Unable to load x509 certificate from  ${url}`)
      throw new InvalidCertificateException(`Could not load X509 certificate(s) at ${url}`)
    }
  }
}
