export interface LegalDocument {
  url: string
  type: string
  mimeTypes: string[]
  governingLawCountries?: string[] //ISO-3166-2 code
  involvedParties: string[]
}
