import { HttpModule } from '@nestjs/axios'
import { CacheModule } from '@nestjs/cache-manager'
import { Global, Module } from '@nestjs/common'
import { ConfigModule } from '@nestjs/config'

import { PrometheusModule } from '@willsoto/nestjs-prometheus'

import { CertificateExpirationBatch } from '../batch/certificate-expiration.batch'
import { IdentityModule } from '../identity/identity.module'
import { RegistryService } from '../vp-validation/service/registry.service'
import { ShaclService } from '../vp-validation/service/shacl.service'
import { DidResolverProvider } from './providers/did-resolver.provider'
import { DocumentLoaderProvider } from './providers/document-loader.provider'
import { PrivateKeyAlgorithmProvider } from './providers/private-key-algorithm.provider'
import { PrivateKeyProvider } from './providers/private-key.provider'
import { DIDService } from './services/did.service'
import { EvidenceService } from './services/evidence.service'
import { ExpirationDateService } from './services/expiration-date.service'
import { TimeService } from './services/time.service'

const prometheusModule = PrometheusModule.register({
  customMetricPrefix: 'gaia_x'
})

@Global()
@Module({
  imports: [ConfigModule.forRoot(), CacheModule.register({ isGlobal: true }), HttpModule, prometheusModule, IdentityModule],
  providers: [
    CertificateExpirationBatch,
    DidResolverProvider.create(),
    DIDService,
    DocumentLoaderProvider.create(),
    ExpirationDateService,
    EvidenceService,
    RegistryService,
    PrivateKeyAlgorithmProvider.create(),
    PrivateKeyProvider.create(),
    ShaclService,
    TimeService
  ],
  exports: [
    ConfigModule,
    HttpModule,
    DIDService,
    EvidenceService,
    RegistryService,
    ShaclService,
    'PRIVATE_KEY',
    'PRIVATE_KEY_ALGORITHM',
    'documentLoader',
    prometheusModule
  ]
})
export class CommonModule {}
