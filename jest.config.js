module.exports = {
  projects: ['<rootDir>/../jest-unit.config.js', '<rootDir>/../jest-integration.config.js'],
  rootDir: 'src',
  maxWorkers: '50%',
  testTimeout: 30000
}
