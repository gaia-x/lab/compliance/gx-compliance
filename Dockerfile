FROM node:20-alpine AS development-build-stage

RUN apk upgrade --no-cache

USER node
WORKDIR /usr/src/app

COPY --chown=node package*.json ./

RUN npm install glob rimraf
RUN npm install

COPY --chown=node src/ src/
COPY --chown=node tools/jena tools/jena
COPY --chown=node tsconfig.build.json tsconfig.build.json
COPY --chown=node tsconfig.json tsconfig.json
COPY --chown=node nest-cli.json nest-cli.json

RUN BASE_URL=$BASE_URL npm run build

FROM node:20-alpine AS production-build-stage
ENV NODE_ENV=production

RUN apk upgrade --no-cache

RUN apk update && apk add openjdk17

USER node
WORKDIR /usr/src/app


COPY --chown=node package*.json ./

RUN npm ci --only=production

COPY --from=development-build-stage /usr/src/app/dist ./dist

CMD ["node", "dist/src/main"]
