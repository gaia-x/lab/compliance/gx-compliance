# Labelling Criteria

The Gaia-X Compliance engine is an implementation of
the [Gaia-X Compliance Document criteria](https://docs.gaia-x.eu/policy-rules-committee/compliance-document/24.11/criteria_cloud_services/),
this page documents each criterion to explain what it means and how it is implemented.

Criteria are implemented as individual filters that divide responsibilities between multiple classes for ease of use
and
maintenance.

---
> **_NOTE:_**
:no_entry: This document is no specification nor standard and only represents the current state of the implementation of
> the Compliance Document.
---

[[_TOC_]]

## Contractual Framework

### Contractual Governance

#### Criterion P1.1.1

> The Provider shall offer the ability to establish a legally binding act. This legally binding act shall be documented.

| Standard Compliance | 	Label L1        | 	Label L2        | 	Label L3        |
|---------------------|------------------|------------------|------------------|
| declaration         | 	declaration     | 	declaration     | 	declaration     |
| **implemented**     | 	**implemented** | 	**implemented** | 	**implemented** |

[View in Compliance Document](https://docs.gaia-x.eu/policy-rules-committee/compliance-document/24.11/criteria_cloud_services/#P1.1.1)

---

Checks that the `ServiceOffering` has at least one `LegallyBindingAct` in its `legalDocuments`.

Implemented
by [ServiceOfferingHasLegallyBindingActFilter](../src/vp-validation/filter/service-offering-has-legally-binding-act.filter.ts)

---

#### Criterion P1.1.2

> The Provider shall have an option for each legally binding act to be governed by EU/EEA/Member State law.

| Standard Compliance | 	Label L1        | 	Label L2        | 	Label L3        |
|---------------------|------------------|------------------|------------------|
| declaration         | 	declaration     | 	declaration     | 	declaration     |
| **implemented**     | 	**implemented** | 	**implemented** | 	**implemented** |

[View in Compliance Document](https://docs.gaia-x.eu/policy-rules-committee/compliance-document/24.11/criteria_cloud_services/#P1.1.2)

---

Checks that the `ServiceOffering` has at least one `LegallyBindingAct` in its `legalDocuments` that is governed by an
EAA country referenced in its `governingLawCountries`.

Implemented
by [ServiceOfferingLegallyBindingActsHaveGoverningLawCountry](../src/vp-validation/filter/service-offering-legally-binding-acts-have-governing-law.country.ts)

---

#### Criterion P1.1.3

> The Provider shall clearly identify for which parties the legal act is binding.

| Standard Compliance | 	Label L1        | 	Label L2        | 	Label L3        |
|---------------------|------------------|------------------|------------------|
| declaration         | 	declaration     | 	declaration     | 	declaration     |
| **implemented**     | 	**implemented** | 	**implemented** | 	**implemented** |

[View in Compliance Document](https://docs.gaia-x.eu/policy-rules-committee/compliance-document/24.11/criteria_cloud_services/#P1.1.3)

---

Collects all the `LegallyBindingAct`s in each `ServiceOffering`'s `legalDocuments` attribute.

Then checks that every `LegallyBindingAct` involved party (`involvedParties` attribute) is declared in a `LegalPerson`
entity.

Implemented
by [LegalDocumentHasInvolvedParties](../src/vp-validation/filter/legal-document-has-involved-parties.filter.ts)

---

#### Criterion P1.1.4

> The Provider shall ensure that the legally binding act covers the entire provision of the Service Offering.

| Standard Compliance | 	Label L1        | 	Label L2        | 	Label L3        |
|---------------------|------------------|------------------|------------------|
| declaration         | 	declaration     | 	declaration     | 	declaration     |
| **implemented**     | 	**implemented** | 	**implemented** | 	**implemented** |

[View in Compliance Document](https://docs.gaia-x.eu/policy-rules-committee/compliance-document/24.11/criteria_cloud_services/#P1.1.4)

---

Verifies that each `ServiceOffering` provides either:

- a `serviceScope` attribute
- an `aggregationOfResources` or a `dependsOn` attribute

Implemented
by [LegallyBindingActsCoverEntireServiceOfferingFilter](../src/vp-validation/filter/legally-binding-acts-cover-entire-service-offering.filter.ts)

---

#### Criterion P1.1.5

> The Provider shall clearly identify in each legally binding act the applicable governing law.

| Standard Compliance | 	Label L1        | 	Label L2        | 	Label L3        |
|---------------------|------------------|------------------|------------------|
| declaration         | 	declaration     | 	declaration     | 	declaration     |
| **implemented**     | 	**implemented** | 	**implemented** | 	**implemented** |

[View in Compliance Document](https://docs.gaia-x.eu/policy-rules-committee/compliance-document/24.11/criteria_cloud_services/#P1.1.5)

---

Verifies that every `LegallyBindingAct` of `ServiceOffering`s have at least one country code in `governingLawCountries`.

Implemented
by [ServiceOfferingLegallyBindingActsHaveGoverningLawCountry](../src/vp-validation/filter/service-offering-legally-binding-acts-have-governing-law.country.ts)

---

### General Material Requirements and Transparency

#### Criterion P1.2.1

> The Provider shall ensure there are specific provisions regarding service interruptions and business continuity (e.g.,
> by means of a service level agreement), Provider’s bankruptcy or any other reason by which the Provider may cease to
> exist in law.

| Standard Compliance | 	Label L1        | 	Label L2        | 	Label L3        |
|---------------------|------------------|------------------|------------------|
| declaration         | 	declaration     | 	declaration     | 	declaration     |
| **implemented**     | 	**implemented** | 	**implemented** | 	**implemented** |

[View in Compliance Document](https://docs.gaia-x.eu/policy-rules-committee/compliance-document/24.11/criteria_cloud_services/#P1.2.1)

---

Please see [Criterion P3.1.16](#criterion-p3116)

---

#### Criterion P1.2.2

> The Provider shall ensure there are provisions governing the rights of the parties to use the service and any Customer
> Data therein.

| Standard Compliance | 	Label L1        | 	Label L2        | 	Label L3        |
|---------------------|------------------|------------------|------------------|
| declaration         | 	declaration     | 	declaration     | 	declaration     |
| **implemented**     | 	**implemented** | 	**implemented** | 	**implemented** |

[View in Compliance Document](https://docs.gaia-x.eu/policy-rules-committee/compliance-document/24.11/criteria_cloud_services/#P1.2.2)

---

Verifies that each `ServiceOffering` has all of these legal document types in its `legalDocuments`attribute:

- `LegallyBindingAct`
- `CustomerDataProcessingTerms`
- `CustomerDataAccessTerms`

Implemented
by [ServiceOfferingHasDataUsageRightsForPartiesFilter](../src/vp-validation/filter/service-offering-has-data-usage-rights-for-parties.filter.ts)

---

#### Criterion P1.2.3

> The Provider shall ensure there are provisions governing changes, regardless of their kind.

| Standard Compliance | 	Label L1        | 	Label L2        | 	Label L3        |
|---------------------|------------------|------------------|------------------|
| declaration         | 	declaration     | 	declaration     | 	declaration     |
| **implemented**     | 	**implemented** | 	**implemented** | 	**implemented** |

[View in Compliance Document](https://docs.gaia-x.eu/policy-rules-committee/compliance-document/24.11/criteria_cloud_services/#P1.2.3)

---

Checks that the `ServiceOffering` has at least one `DocumentChangeProcedures` in its `legalDocuments`.

Implemented
by [ServiceOfferingHasDocumentedChangeProceduresFilter](../src/vp-validation/filter/service-offering-has-documented-change-procedures.filter.ts)

---

#### Criterion P1.2.4

> The Provider shall ensure there are provisions governing aspects regarding copyright or any other intellectual
> property rights.

| Standard Compliance | 	Label L1        | 	Label L2        | 	Label L3        |
|---------------------|------------------|------------------|------------------|
| declaration         | 	declaration     | 	declaration     | 	declaration     |
| **implemented**     | 	**implemented** | 	**implemented** | 	**implemented** |

[View in Compliance Document](https://docs.gaia-x.eu/policy-rules-committee/compliance-document/24.11/criteria_cloud_services/#P1.2.4)

---

Checks that the `ServiceOffering` has at least one `CopyrightAndIntellectualPropertyDocument` in its `legalDocuments`.

Implemented
by [ServiceOfferingHasDocumentedCopyrightAndIntellectualPropertyFilter](../src/vp-validation/filter/service-offering-has-documented-copyright-and-intellectual-property.filter.ts)

---

#### Criterion P1.2.5

> The Provider shall declare the general location of any processing of Customer Data, allowing the Customer to determine
> the applicable jurisdiction and to comply with Customer’s requirements in the context of its business and operational
> context.

| Standard Compliance | 	Label L1        | 	Label L2        | 	Label L3        |
|---------------------|------------------|------------------|------------------|
| declaration         | 	declaration     | 	declaration     | 	declaration     |
| **implemented**     | 	**implemented** | 	**implemented** | 	**implemented** |

[View in Compliance Document](https://docs.gaia-x.eu/policy-rules-committee/compliance-document/24.11/criteria_cloud_services/#P1.2.5)

---

Checks that the `ServiceOffering` has at least one `Resource` containing a `location` with an `administrativeAreaCode`.

Implemented
by [ServiceOfferingHasAResourceWithAddressFilter](../src/vp-validation/filter/service-offering-has-a-resource-with-address.filter.ts)

---

#### Criterion P1.2.6

> The Provider shall explain how information about subcontractors and related Customer Data localization will be
> communicated.

| Standard Compliance | 	Label L1        | 	Label L2        | 	Label L3        |
|---------------------|------------------|------------------|------------------|
| declaration         | 	declaration     | 	declaration     | 	declaration     |
| **implemented**     | 	**implemented** | 	**implemented** | 	**implemented** |

[View in Compliance Document](https://docs.gaia-x.eu/policy-rules-committee/compliance-document/24.11/criteria_cloud_services/#P1.2.6)

---

Checks that the `ServiceOffering` has at least one `SubContractor` in its `subContractors` attribute containing at least
one element in its:

- `communicationMethods` attribute
- `informationDocuments` attribute

Implemented
by [ServiceOfferingHasSubContractorDetailsFilter](../src/vp-validation/filter/service-offering-has-subcontractor-details.filter.ts)

---

#### Criterion P1.2.7

> The Provider shall communicate to the Customer where the applicable jurisdiction(s) of subcontractors will be.

| Standard Compliance | 	Label L1        | 	Label L2        | 	Label L3        |
|---------------------|------------------|------------------|------------------|
| declaration         | 	declaration     | 	declaration     | 	declaration     |
| **implemented**     | 	**implemented** | 	**implemented** | 	**implemented** |

[View in Compliance Document](https://docs.gaia-x.eu/policy-rules-committee/compliance-document/24.11/criteria_cloud_services/#P1.2.7)

---

Checks that the `ServiceOffering` has at least one `SubContractor` in its `subContractors` attribute containing at least
one element in its:

- `applicableJurisdiction` attribute
- `legalName` attribute

Implemented
by [ServiceOfferingHasSubContractorDetailsFilter](../src/vp-validation/filter/service-offering-has-subcontractor-details.filter.ts)

---

#### Criterion P1.2.8

> The Provider shall include in the contract the contact details where Customer may address any queries regarding the
> Service Offering and the contract.

| Standard Compliance | 	Label L1        | 	Label L2        | 	Label L3        |
|---------------------|------------------|------------------|------------------|
| declaration         | 	declaration     | 	declaration     | 	declaration     |
| **implemented**     | 	**implemented** | 	**implemented** | 	**implemented** |

[View in Compliance Document](https://docs.gaia-x.eu/policy-rules-committee/compliance-document/24.11/criteria_cloud_services/#P1.2.8)

---

Checks that the `ServiceOffering` has `ContactInformation` in its `providerContactInformation` attribute.

Implemented
by [ServiceOfferingHasProviderContactInformationFilter](../src/vp-validation/filter/service-offering-has-provider-contact-information.filter.ts)

---

#### Criterion P1.2.9

> The Provider shall declare the mandatory service and resource attributes in the self-description of each Service
> Offering.

| Standard Compliance | 	Label L1        | 	Label L2        | 	Label L3        |
|---------------------|------------------|------------------|------------------|
| declaration         | 	declaration     | 	declaration     | 	declaration     |
| **implemented**     | 	**implemented** | 	**implemented** | 	**implemented** |

[View in Compliance Document](https://docs.gaia-x.eu/policy-rules-committee/compliance-document/24.11/criteria_cloud_services/#P1.2.9)

---

This is checked through the SHACL shape validation.

Implemented
by [VpRespectsShaclShapeFilter](../src/vp-validation/filter/vp-respects-shacl-shape.filter.ts)

---

### Technical Compliance Requirements

#### Criterion P1.3.1

> The Provider shall describe the Permissions, Requirements and Constraints of the Service Offering using a common
> Domain-Specific Language (DSL) in the self-description.

| Standard Compliance | 	Label L1        | 	Label L2        | 	Label L3        |
|---------------------|------------------|------------------|------------------|
| N/A                 | 	declaration     | 	declaration     | 	declaration     |
| N/A                 | 	**implemented** | 	**implemented** | 	**implemented** |

[View in Compliance Document](https://docs.gaia-x.eu/policy-rules-committee/compliance-document/24.11/criteria_cloud_services/#P1.3.1)

---

Verifies that each `ServiceOffering` contains at least one `servicePolicy` of type `gx:AccessUsagePolicy`

Implemented by
[ServiceOfferingHasServicePolicyFilter](../src/vp-validation/filter/service-offering-has-service-policy.filter.ts)

---

#### Criterion P1.3.2

> The Provider shall ensure that the Service Offering is operated by a Gaia-X participant defined by a verified
> credential.

| Standard Compliance | 	Label L1        | 	Label L2        | 	Label L3        |
|---------------------|------------------|------------------|------------------|
| declaration         | 	declaration     | 	declaration     | 	declaration     |
| **implemented**     | 	**implemented** | 	**implemented** | 	**implemented** |

[View in Compliance Document](https://docs.gaia-x.eu/policy-rules-committee/compliance-document/24.11/criteria_cloud_services/#P1.3.2)

---

Verifies that the `ServiceOffering` is provided by (`providedBy` attribute) a `LegalPerson` with a Gaia-X trusted notary
signed `RegistrationNumber` in its `registrationNumber` attribute.

Implemented
by [ServiceOfferingIsOperatedByGaiaXParticipantFilter](../src/vp-validation/filter/service-offering-is-operated-by-gaia-x-participant.filter.ts)

---

#### Criterion P1.3.2

> The Provider shall ensure that the Service Offering is operated by a Gaia-X participant defined by a verified
> credential.

| Standard Compliance | 	Label L1        | 	Label L2        | 	Label L3        |
|---------------------|------------------|------------------|------------------|
| declaration         | 	declaration     | 	declaration     | 	declaration     |
| **implemented**     | 	**implemented** | 	**implemented** | 	**implemented** |

[View in Compliance Document](https://docs.gaia-x.eu/policy-rules-committee/compliance-document/24.11/criteria_cloud_services/#P1.3.2)

---

Verifies that the `ServiceOffering` is provided by (`providedBy` attribute) a `LegalPerson` with a Gaia-X trusted notary
signed `RegistrationNumber` in its `registrationNumber` attribute.

Implemented
by [ServiceOfferingIsOperatedByGaiaXParticipantFilter](../src/vp-validation/filter/service-offering-is-operated-by-gaia-x-participant.filter.ts)

---

## Data Protection

### General

#### Criterion P2.1.1

> The Provider shall offer the ability to establish a contract under Union or EU/EEA/Member State law and specifically
> addressing GDPR requirements.

| Standard Compliance | 	Label L1        | 	Label L2          | 	Label L3          |
|---------------------|------------------|--------------------|--------------------|
| N/A                 | 	declaration     | 	certification     | 	certification     |
| N/A                 | 	**implemented** | 	`not implemented` | 	`not implemented` |

[View in Compliance Document](https://docs.gaia-x.eu/policy-rules-committee/compliance-document/24.11/criteria_cloud_services/#P2.1.1)

---

Checks that `ServiceOffering` has at least one `DataProtectionRegulationMeasures` and at least
one `ServiceAgreementOffer` in its `legalDocuments`.

Implemented
by [ServiceOfferingHasDataProtectionAndServiceAgreementFilter](../src/vp-validation/filter/service-offering-has-data-protection-and-service-agreement.filter.ts)

---

#### Criterion P2.1.2

> The Provider shall define the roles and responsibilities of each party.

| Standard Compliance | 	Label L1        | 	Label L2          | 	Label L3          |
|---------------------|------------------|--------------------|--------------------|
| declaration         | 	declaration     | 	certification     | 	certification     |
| **implemented**     | 	**implemented** | 	`not implemented` | 	`not implemented` |

[View in Compliance Document](https://docs.gaia-x.eu/policy-rules-committee/compliance-document/24.11/criteria_cloud_services/#P2.1.2)

---

Checks that `ServiceOffering` has at least one `RoleAndResponsibilities` in its `legalDocuments`.

Implemented
by [ServiceOfferingHasRoleAndResponsibilitiesFilter](../src/vp-validation/filter/service-offering-has-role-and-responsibilities.filter.ts)

---

#### Criterion P2.1.3

> The Provider shall clearly define the technical and organizational measures in accordance with the roles and
> responsibilities of the parties, including an adequate level of detail.

| Standard Compliance | 	Label L1        | 	Label L2        | 	Label L3        |
|---------------------|------------------|------------------|------------------|
| declaration         | 	declaration     | 	declaration     | 	declaration     |
| **implemented**     | 	**implemented** | 	**implemented** | 	**implemented** |

[View in Compliance Document](https://docs.gaia-x.eu/policy-rules-committee/compliance-document/24.11/criteria_cloud_services/#P2.1.3)

---

Checks that `ServiceOffering` has `Measure`s in its `requiredMeasures` attribute.

Implemented
by [ServiceOfferingHasRequiredMeasuresFilter](../src/vp-validation/filter/service-offering-has-required-measures.filter.ts)

---

### GDPR Article 28

#### Criterion P2.2.1

> The Provider shall be ultimately bound to instructions of the Customer.

| Standard Compliance | 	Label L1        | 	Label L2          | 	Label L3         |
|---------------------|------------------|--------------------|-------------------|
| declaration         | 	declaration     | 	certification     | 	certification    |
| **implemented**     | 	**implemented** | 	`not implemented` | `not implemented` |

[View in Compliance Document](https://docs.gaia-x.eu/policy-rules-committee/compliance-document/24.11/criteria_cloud_services/#P2.2.1)

---

Checks that `ServiceOffering` has `CustomerInstructions` in its `customerInstructions` attribute and that the latter has
`terms`.

Implemented
by [ServiceOfferingHasCustomerInstructionsFilter](../src/vp-validation/filter/service-offering-has-customer-instructions.filter.ts)

---

#### Criterion P2.2.2

> The Provider shall clearly define how Customer may instruct, including by electronic means such as configuration tools
> or APIs.

| Standard Compliance | 	Label L1        | 	Label L2          | 	Label L3         |
|---------------------|------------------|--------------------|-------------------|
| declaration         | 	declaration     | 	certification     | 	certification    |
| **implemented**     | 	**implemented** | 	`not implemented` | not	implementable |

[View in Compliance Document](https://docs.gaia-x.eu/policy-rules-committee/compliance-document/24.11/criteria_cloud_services/#P2.2.2)

---

Checks that `ServiceOffering` has `CustomerInstructions` in its `customerInstructions` attribute and that the latter has
`means`.

Implemented
by [ServiceOfferingHasCustomerInstructionsFilter](../src/vp-validation/filter/service-offering-has-customer-instructions.filter.ts)

---

#### Criterion P2.2.3

> The Provider shall clearly define if and to which extent third country transfer will take place.

| Standard Compliance | 	Label L1        | 	Label L2          | 	Label L3 |
|---------------------|------------------|--------------------|-----------|
| declaration         | 	declaration     | 	certification     | 	N/A      |
| **implemented**     | 	**implemented** | 	`not implemented` | 	N/A      |

[View in Compliance Document](https://docs.gaia-x.eu/policy-rules-committee/compliance-document/24.11/criteria_cloud_services/#P2.2.3)

---

Verified by the SHACL shape which allows the `ServiceOffering` to have an empty `possiblePersonalDataTransfers`
attribute but also will check that the `DataTransfer` entities are valid if ever this attribute is provided.

Implemented
by [VpRespectsShaclShapeFilter](../src/vp-validation/filter/vp-respects-shacl-shape.filter.ts)

---

#### Criterion P2.2.4

> The Provider shall clearly define if and to which extent third country transfer will take place.

| Standard Compliance | 	Label L1        | 	Label L2          | 	Label L3 |
|---------------------|------------------|--------------------|-----------|
| N/A                 | 	declaration     | 	certification     | 	N/A      |
| N/A                 | 	**implemented** | 	`not implemented` | N/A       |

[View in Compliance Document](https://docs.gaia-x.eu/policy-rules-committee/compliance-document/24.11/criteria_cloud_services/#P2.2.4)

---

In the case where a `ServiceOffering` has provided the `possiblePersonalDataTransfers` attribute, the `DataTransfer`
entities with a `ThirdCountryDataTransfer` type provided in that attribute are collected.

If one of those `ThirdCountryDataTransfer` entities contains an unsecure third country in its `countries` attribute,
this criterion will not be conformant.

Implemented
by [ServiceOfferingHasDescribedDataTransfersFilter](../src/vp-validation/filter/service-offering-has-described-data-transfers.filter.ts)

---

#### Criterion P2.2.5

> The Provider shall clearly define if and to which extent sub-processors will be involved.

| Standard Compliance | 	Label L1        | 	Label L2          | 	Label L3          |
|---------------------|------------------|--------------------|--------------------|
| declaration         | 	declaration     | 	certification     | 	certification     |
| **implemented**     | 	**implemented** | 	`not implemented` | 	`not implemented` |

[View in Compliance Document](https://docs.gaia-x.eu/policy-rules-committee/compliance-document/24.11/criteria_cloud_services/#P2.2.5)

---

Please see [Criterion P2.2.3](#criterion-p223)

Implemented
by [VpRespectsShaclShapeFilter](../src/vp-validation/filter/vp-respects-shacl-shape.filter.ts)

---

#### Criterion P2.2.6

> The Provider shall clearly define if and to which extent sub-processors will be involved.

| Standard Compliance | 	Label L1        | 	Label L2          | 	Label L3          |
|---------------------|------------------|--------------------|--------------------|
| declaration         | 	declaration     | 	certification     | 	certification     |
| **implemented**     | 	**implemented** | 	`not implemented` | 	`not implemented` |

[View in Compliance Document](https://docs.gaia-x.eu/policy-rules-committee/compliance-document/24.11/criteria_cloud_services/#P2.2.6)

---

Please see [Criterion P2.2.3](#criterion-p223)

Implemented
by [VpRespectsShaclShapeFilter](../src/vp-validation/filter/vp-respects-shacl-shape.filter.ts)

---

#### Criterion P2.2.7

> The Provider shall define the audit rights for the Customer.

| Standard Compliance | 	Label L1        | 	Label L2          | 	Label L3          |
|---------------------|------------------|--------------------|--------------------|
| declaration         | 	declaration     | 	certification     | 	certification     |
| **implemented**     | 	**implemented** | 	`not implemented` | 	`not implemented` |

[View in Compliance Document](https://docs.gaia-x.eu/policy-rules-committee/compliance-document/24.11/criteria_cloud_services/#P2.2.7)

---

Checks that the `ServiceOffering` has at least one `CustomerAuditingRights` in its `legalDocuments`.

Implemented
by [ServiceOfferingHasCustomerAuditingRightsFilter](../src/vp-validation/filter/service-offering-has-customer-auditing-rights.filter.ts)

---

## Cybersecurity

### Criterion P3.1.1

> Organization of information security: Plan, implement, maintain and continuously improve the information security
> framework within the organisation.

| Standard Compliance | 	Label L1        | 	Label L2          | 	Label L3          |
|---------------------|------------------|--------------------|--------------------|
| declaration         | 	declaration     | 	certification     | 	certification     |
| **implemented**     | 	**implemented** | 	`not implemented` | 	`not implemented` |

[View in Compliance Document](https://docs.gaia-x.eu/policy-rules-committee/compliance-document/24.11/criteria_cloud_services/#P3.1.1)

---

Checks that the `ServiceOffering` has at least one `InformationSecurityOrganization` in its `legalDocuments`.

Implemented
by [ServiceOfferingHasInformationSecurityOrganizationFilter](../src/vp-validation/filter/service-offering-has-information-security-organization.filter.ts)

---

### Criterion P3.1.2

> Information Security Policies: Provide a global information security policy, derived into policies and procedures
> regarding security requirements and to support business requirements.

| Standard Compliance | 	Label L1        | 	Label L2          | 	Label L3          |
|---------------------|------------------|--------------------|--------------------|
| declaration         | 	declaration     | 	certification     | 	certification     |
| **implemented**     | 	**implemented** | 	`not implemented` | 	`not implemented` |

[View in Compliance Document](https://docs.gaia-x.eu/policy-rules-committee/compliance-document/24.11/criteria_cloud_services/#P3.1.2)

---

Checks that the `ServiceOffering` has at least one `InformationSecurityPolicies` in its `legalDocuments`.

Implemented
by [ServiceOfferingHasInformationSecurityPoliciesFilter](../src/vp-validation/filter/service-offering-has-information-security-policies.filter.ts)

---

### Criterion P3.1.3

> Risk Management: Ensure that risks related to information security are properly identified, assessed, and treated, and
> that the residual risk is acceptable to the CSP.

| Standard Compliance | 	Label L1        | 	Label L2          | 	Label L3          |
|---------------------|------------------|--------------------|--------------------|
| declaration         | 	declaration     | 	certification     | 	certification     |
| **implemented**     | 	**implemented** | 	`not implemented` | 	`not implemented` |

[View in Compliance Document](https://docs.gaia-x.eu/policy-rules-committee/compliance-document/24.11/criteria_cloud_services/#P3.1.3)

---

Checks that the `ServiceOffering` has at least one `InformationSecurityRiskManagement` in its `legalDocuments`.

Implemented
by [ServiceOfferingHasInformationSecurityRiskManagementFilter](../src/vp-validation/filter/service-offering-has-information-security-risk-management.filter.ts)

---

### Criterion P3.1.4

> Human Resources: Ensure that employees understand their responsibilities, are aware of their responsibilities with
> regard to information security, and that the organisation’s assets are protected in the event of changes in
> responsibilities or termination.

| Standard Compliance | 	Label L1        | 	Label L2          | 	Label L3          |
|---------------------|------------------|--------------------|--------------------|
| declaration         | 	declaration     | 	certification     | 	certification     |
| **implemented**     | 	**implemented** | 	`not implemented` | 	`not implemented` |

[View in Compliance Document](https://docs.gaia-x.eu/policy-rules-committee/compliance-document/24.11/criteria_cloud_services/#P3.1.4)

---

Checks that the `ServiceOffering` has at least one `EmployeeResponsibilities` in its `legalDocuments`.

Implemented
by [ServiceOfferingHasEmployeeResponsibilitiesFilter](../src/vp-validation/filter/service-offering-has-employee-responsibilities.filter.ts)

---

### Criterion P3.1.5

> Asset Management: Identify the organisation’s own assets and ensure an appropriate level of protection throughout
> their lifecycle.

| Standard Compliance | 	Label L1        | 	Label L2          | 	Label L3          |
|---------------------|------------------|--------------------|--------------------|
| declaration         | 	declaration     | 	certification     | 	certification     |
| **implemented**     | 	**implemented** | 	`not implemented` | 	`not implemented` |

[View in Compliance Document](https://docs.gaia-x.eu/policy-rules-committee/compliance-document/24.11/criteria_cloud_services/#P3.1.5)

---

Checks that the `ServiceOffering` has at least one `AssetsManagement` in its `legalDocuments`.

Implemented
by [ServiceOfferingHasAssetsManagementFilter](../src/vp-validation/filter/service-offering-has-assets-management.filter.ts)

---

### Criterion P3.1.6

> Physical Security: Prevent unauthorised physical access and protect against theft, damage, loss and outage of
> operations.

| Standard Compliance | 	Label L1        | 	Label L2          | 	Label L3          |
|---------------------|------------------|--------------------|--------------------|
| declaration         | 	declaration     | 	certification     | 	certification     |
| **implemented**     | 	**implemented** | 	`not implemented` | 	`not implemented` |

[View in Compliance Document](https://docs.gaia-x.eu/policy-rules-committee/compliance-document/24.11/criteria_cloud_services/#P3.1.6)

---

Checks that the `ServiceOffering` has at least one `PhysicalSecurity` in its `legalDocuments`.

Implemented
by [ServiceOfferingHasPhysicalSecurityFilter](../src/vp-validation/filter/service-offering-has-physical-security.filter.ts)

---

### Criterion P3.1.7

> Operational Security: Ensure proper and regular operation, including appropriate measures for planning and monitoring
> capacity, protection against malware, logging and monitoring events, and dealing with vulnerabilities, malfunctions
> and
> failures.

| Standard Compliance | 	Label L1        | 	Label L2          | 	Label L3          |
|---------------------|------------------|--------------------|--------------------|
| declaration         | 	declaration     | 	certification     | 	certification     |
| **implemented**     | 	**implemented** | 	`not implemented` | 	`not implemented` |

[View in Compliance Document](https://docs.gaia-x.eu/policy-rules-committee/compliance-document/24.11/criteria_cloud_services/#P3.1.7)

---

Checks that the `ServiceOffering` has at least one `OperationalSecurity` in its `legalDocuments`.

Implemented
by [ServiceOfferingHasOperationalSecurityFilter](../src/vp-validation/filter/service-offering-has-operational-security.filter.ts)

---

### Criterion P3.1.8

> Identity, Authentication and access control management: Limit access to information and information processing
> facilities.

| Standard Compliance | 	Label L1        | 	Label L2          | 	Label L3          |
|---------------------|------------------|--------------------|--------------------|
| declaration         | 	declaration     | 	certification     | 	certification     |
| **implemented**     | 	**implemented** | 	`not implemented` | 	`not implemented` |

[View in Compliance Document](https://docs.gaia-x.eu/policy-rules-committee/compliance-document/24.11/criteria_cloud_services/#P3.1.8)

---

Checks that the `ServiceOffering` has at least one `AccessControlManagement` in its `legalDocuments`.

Implemented
by [ServiceOfferingHasAccessControlManagementFilter](../src/vp-validation/filter/service-offering-has-access-control-management.filter.ts)

---

### Criterion P3.1.9

> Cryptography and Key management: Ensure appropriate and effective use of cryptography to protect the confidentiality,
> authenticity or integrity of information.

| Standard Compliance | 	Label L1        | 	Label L2          | 	Label L3          |
|---------------------|------------------|--------------------|--------------------|
| declaration         | 	declaration     | 	certification     | 	certification     |
| **implemented**     | 	**implemented** | 	`not implemented` | 	`not implemented` |

[View in Compliance Document](https://docs.gaia-x.eu/policy-rules-committee/compliance-document/24.11/criteria_cloud_services/#P3.1.9)

---

Checks that the `ServiceOffering` has at least one `cryptographicSecurityStandards` defined.

Implemented
by [ServiceOfferingHasCryptographicSecurityStandards](../src/vp-validation/filter/service-offering-has-cryptographic-security-standards.filter.ts)

---

### Criterion P3.1.10

> Communication Security: Ensure the protection of information in networks and the corresponding information processing
> systems.

| Standard Compliance | 	Label L1        | 	Label L2          | 	Label L3          |
|---------------------|------------------|--------------------|--------------------|
| declaration         | 	declaration     | 	certification     | 	certification     |
| **implemented**     | 	**implemented** | 	`not implemented` | 	`not implemented` |

[View in Compliance Document](https://docs.gaia-x.eu/policy-rules-committee/compliance-document/24.11/criteria_cloud_services/#P3.1.10)

---

Checks that each `ServiceOffering` has a `PointOfPresence` and a `Datacenter` described in its resources. Children of 
those classes also satisfy this criteria. The location of these instances need to be specified, this is checked during 
the SHACL shape validation phase.

---

### Criterion P3.1.11

> Portability and Interoperability: The CSP shall provide a means by which a customer can obtain their stored customer
> data, and provide documentation on how (where appropriate, through documented API’s) the CSC can obtain the stored
> data
> at the end of the contractual relationship and shall document how the data will be securely deleted from the Cloud
> Service Provider in what timeframe.

| Standard Compliance | 	Label L1        | 	Label L2          | 	Label L3          |
|---------------------|------------------|--------------------|--------------------|
| declaration         | 	declaration     | 	certification     | 	certification     |
| **implemented**     | 	**implemented** | 	`not implemented` | 	`not implemented` |

[View in Compliance Document](https://docs.gaia-x.eu/policy-rules-committee/compliance-document/24.11/criteria_cloud_services/#P3.1.11)

---

Checks that the `ServiceOffering` has at least one `dataPortability` defined.

Implemented
by [ServiceOfferingHasDataPortabilityFilter](../src/vp-validation/filter/service-offering-has-data-portability.filter.ts)

---

### Criterion P3.1.12

> Change and Configuration Management: Ensure that changes and configuration actions to information systems guarantee
> the security of the delivered cloud service.

| Standard Compliance | 	Label L1        | 	Label L2          | 	Label L3          |
|---------------------|------------------|--------------------|--------------------|
| declaration         | 	declaration     | 	certification     | 	certification     |
| **implemented**     | 	**implemented** | 	`not implemented` | 	`not implemented` |

[View in Compliance Document](https://docs.gaia-x.eu/policy-rules-committee/compliance-document/24.11/criteria_cloud_services/#P3.1.12)

---

Checks that the `ServiceOffering` has at least one `ChangeAndConfigurationManagement` in its `legalDocuments`.

Implemented
by [ServiceOfferingHasChangeAndConfigurationManagementFilter](../src/vp-validation/filter/service-offering-has-change-and-configuration-management.filter.ts)

---

### Criterion P3.1.13

> Development of Information systems: Ensure information security in the development cycle of information systems.

| Standard Compliance | 	Label L1        | 	Label L2          | 	Label L3          |
|---------------------|------------------|--------------------|--------------------|
| declaration         | 	declaration     | 	certification     | 	certification     |
| **implemented**     | 	**implemented** | 	`not implemented` | 	`not implemented` |

[View in Compliance Document](https://docs.gaia-x.eu/policy-rules-committee/compliance-document/24.11/criteria_cloud_services/#P3.1.13)

---

Checks that the `ServiceOffering` has at least one `DevelopmentCycleSecurity` in its `legalDocuments`.

Implemented
by [ServiceOfferingHasDevelopmentCycleSecurityFilter](../src/vp-validation/filter/service-offering-has-development-cycle-security.filter.ts)

---

### Criterion P3.1.14

> Procurement Management: Ensure the protection of information that suppliers of the CSP can access and monitor the
> agreed services and security requirements.

| Standard Compliance | 	Label L1        | 	Label L2          | 	Label L3          |
|---------------------|------------------|--------------------|--------------------|
| declaration         | 	declaration     | 	certification     | 	certification     |
| **implemented**     | 	**implemented** | 	`not implemented` | 	`not implemented` |

[View in Compliance Document](https://docs.gaia-x.eu/policy-rules-committee/compliance-document/24.11/criteria_cloud_services/#P3.1.14)

---

Checks that the `ServiceOffering` has at least one `ProcurementManagementSecurity` in its `legalDocuments`.

Implemented
by [ServiceOfferingHasProcurementManagementSecurityFilter](../src/vp-validation/filter/service-offering-has-procurement-management-security.filter.ts)

---

### Criterion P3.1.15

> Incident Management: Ensure a consistent and comprehensive approach to the capture, assessment, communication and
> escalation of security incidents.

| Standard Compliance | 	Label L1        | 	Label L2          | 	Label L3          |
|---------------------|------------------|--------------------|--------------------|
| declaration         | 	declaration     | 	certification     | 	certification     |
| **implemented**     | 	**implemented** | 	`not implemented` | 	`not implemented` |

[View in Compliance Document](https://docs.gaia-x.eu/policy-rules-committee/compliance-document/24.11/criteria_cloud_services/#P3.1.15)

---

Checks that the `ServiceOffering` has at least one `SecurityIncidentManagement` in its `legalDocuments`.

Implemented
by [ServiceOfferingHasSecurityIncidentManagementFilter](../src/vp-validation/filter/service-offering-has-security-incident-management.filter.ts)

---

### Criterion P3.1.16

> Business Continuity: Plan, implement, maintain and test procedures and measures for business continuity and emergency
> management.

| Standard Compliance | 	Label L1        | 	Label L2          | 	Label L3          |
|---------------------|------------------|--------------------|--------------------|
| declaration         | 	declaration     | 	certification     | 	certification     |
| **implemented**     | 	**implemented** | 	`not implemented` | 	`not implemented` |

[View in Compliance Document](https://docs.gaia-x.eu/policy-rules-committee/compliance-document/24.11/criteria_cloud_services/#P3.1.16)

---

Checks that the `ServiceOffering` has at least one `BusinessContinuityMeasures` in its `legalDocuments`.

Implemented
by [ServiceOfferingHasBusinessContinuityMeasuresFilter](../src/vp-validation/filter/service-offering-has-business-continuity-measures.filter.ts)

---

### Criterion P3.1.17

> Compliance: Avoid non-compliance with legal, regulatory, self-imposed or contractual information security and
> compliance requirements.

| Standard Compliance | 	Label L1        | 	Label L2          | 	Label L3          |
|---------------------|------------------|--------------------|--------------------|
| declaration         | 	declaration     | 	certification     | 	certification     |
| **implemented**     | 	**implemented** | 	`not implemented` | 	`not implemented` |

[View in Compliance Document](https://docs.gaia-x.eu/policy-rules-committee/compliance-document/24.11/criteria_cloud_services/#P3.1.17)

---

Checks that the `ServiceOffering` has at least one `ComplianceAssurance` in its `legalDocuments`.

Implemented
by [ServiceOfferingHasComplianceAssuranceFilter](../src/vp-validation/filter/service-offering-has-compliance-assurance.filter.ts)

---

### Criterion P3.1.18

> User documentation: Provide up-to-date information on the secure configuration and known vulnerabilities of the cloud
> service for cloud customers.

| Standard Compliance | 	Label L1        | 	Label L2          | 	Label L3          |
|---------------------|------------------|--------------------|--------------------|
| declaration         | 	declaration     | 	certification     | 	certification     |
| **implemented**     | 	**implemented** | 	`not implemented` | 	`not implemented` |

[View in Compliance Document](https://docs.gaia-x.eu/policy-rules-committee/compliance-document/24.11/criteria_cloud_services/#P3.1.18)

---

Checks that the `ServiceOffering` has at least one `UserDocumentationMaintenance` in its `legalDocuments`.

Implemented
by [ServiceOfferingHasUserDocumentationMaintenanceFilter](../src/vp-validation/filter/service-offering-has-user-documentation-maintenance.filter.ts)

---

### Criterion P3.1.19

> Dealing with information requests from government agencies: Ensure appropriate handling of government investigation
> requests for legal review, information to cloud customers, and limitation of access to or disclosure of Customer Data.

| Standard Compliance | 	Label L1        | 	Label L2          | 	Label L3          |
|---------------------|------------------|--------------------|--------------------|
| declaration         | 	declaration     | 	certification     | 	certification     |
| **implemented**     | 	**implemented** | 	`not implemented` | 	`not implemented` |

[View in Compliance Document](https://docs.gaia-x.eu/policy-rules-committee/compliance-document/24.11/criteria_cloud_services/#P3.1.19)

---

Checks that the `ServiceOffering` has at least one `GovernmentInvestigationManagement` in its `legalDocuments`.

Implemented
by [ServiceOfferingHasGovernmentInvestigationManagementFilter](../src/vp-validation/filter/service-offering-has-government-investigation-management.filter.ts)

---

### Criterion P3.1.20

> Product security: Provide appropriate mechanisms for cloud customers to enable product security.

| Standard Compliance | 	Label L1        | 	Label L2          | 	Label L3          |
|---------------------|------------------|--------------------|--------------------|
| declaration         | 	declaration     | 	certification     | 	certification     |
| **implemented**     | 	**implemented** | 	`not implemented` | 	`not implemented` |

[View in Compliance Document](https://docs.gaia-x.eu/policy-rules-committee/compliance-document/24.11/criteria_cloud_services/#P3.1.20)

---

Checks that the `ServiceOffering` has at least one `ProductSecurity` in its `legalDocuments`.

Implemented
by [ServiceOfferingHasProductSecurityFilter](../src/vp-validation/filter/service-offering-has-product-security.filter.ts)

---

## Portability

### Switching and porting of Customer Data

#### Criterion P4.1.1

> The Provider shall implement practices for facilitating the switching of Providers and the porting of Customer Data in
> a structured, commonly used and machine-readable format including open standard formats where required or requested by
> the Customer.

| Standard Compliance | 	Label L1        | 	Label L2        | 	Label L3          |
|---------------------|------------------|------------------|--------------------|
| declaration         | 	declaration     | 	declaration     | 	certification     |
| **implemented**     | 	**implemented** | 	**implemented** | 	`not implemented` |

[View in Compliance Document](https://docs.gaia-x.eu/policy-rules-committee/compliance-document/24.11/criteria_cloud_services/#P4.1.1)

---

Checks that `DataPortability`s provided by `ServiceOffering`s have the required following attributes:

- `means`
- `legalDocument` which is also checked to make sure it's a valid `LegalDocument`
- `formats`
- `documentation` which must be a valid URI

Implemented
by [ServiceOfferingHasDataPortabilityFilter](../src/vp-validation/filter/service-offering-has-data-portability.filter.ts)

---

#### Criterion P4.1.2

> The Provider shall ensure pre-contractual information exists, with sufficiently detailed, clear and transparent
> information regarding the processes of Customer Data portability, technical requirements, timeframes and charges that
> apply in case a professional user wants to switch to another Provider or port Customer Data back to its own IT
> systems.

| Standard Compliance | 	Label L1        | 	Label L2        | 	Label L3          |
|---------------------|------------------|------------------|--------------------|
| declaration         | 	declaration     | 	declaration     | 	certification     |
| **implemented**     | 	**implemented** | 	**implemented** | 	`not implemented` |

[View in Compliance Document](https://docs.gaia-x.eu/policy-rules-committee/compliance-document/24.11/criteria_cloud_services/#P4.1.2)

---

Checks that `DataPortability`s provided by `ServiceOffering`s have the required `pricing` attribute

Implemented
by [ServiceOfferingHasDataPortabilityFilter](../src/vp-validation/filter/service-offering-has-data-portability.filter.ts)

---

## European Control

### Processing and storing of the Customer Data in EU/EEA

#### Criterion P5.1.1

> For Label Level 2, the Provider shall provide the option that all Customer Data are processed and stored exclusively
> in EU/EEA.

| Standard Compliance | 	Label L1 | 	Label L2          | 	Label L3 |
|---------------------|-----------|--------------------|-----------|
| N/A                 | 	N/A      | 	certification     | 	N/A      |
| N/A                 | 	N/A      | 	`not implemented` | 	N/A      |

[View in Compliance Document](https://docs.gaia-x.eu/policy-rules-committee/compliance-document/24.11/criteria_cloud_services/#P5.1.1)

---

Not implemented.

---

#### Criterion P5.1.2

> For Label Level 3, the Provider shall process and store all Customer Data exclusively in the EU/EEA.

| Standard Compliance | 	Label L1 | 	Label L2 | 	Label L3          |
|---------------------|-----------|-----------|--------------------|
| N/A                 | 	N/A      | 	N/A      | 	certification     |
| N/A                 | 	N/A      | 	N/A      | 	`not implemented` |

[View in Compliance Document](https://docs.gaia-x.eu/policy-rules-committee/compliance-document/24.11/criteria_cloud_services/#P5.1.2)

---

Not implemented.

---

#### Criterion P5.1.3

> For Label Level 3, where the Provider or subcontractor is subject to legal obligations to transmit or disclose
> Customer Data on the basis of a non-EU/EEA statutory order, the Provider shall have verified safeguards in place to
> ensure that any access request is compliant with EU/EEA/Member State law.

| Standard Compliance | 	Label L1 | 	Label L2 | 	Label L3          |
|---------------------|-----------|-----------|--------------------|
| N/A                 | 	N/A      | 	N/A      | 	certification     |
| N/A                 | 	N/A      | 	N/A      | 	`not implemented` |

[View in Compliance Document](https://docs.gaia-x.eu/policy-rules-committee/compliance-document/24.11/criteria_cloud_services/#P5.1.3)

---

Not implemented.

---

#### Criterion P5.1.4

> For Label Level 3, the Provider’s registered head office, headquarters and main establishment shall be established in
> a Member State of the EU/EEA.

| Standard Compliance | 	Label L1 | 	Label L2 | 	Label L3          |
|---------------------|-----------|-----------|--------------------|
| N/A                 | 	N/A      | 	N/A      | 	certification     |
| N/A                 | 	N/A      | 	N/A      | 	`not implemented` |

[View in Compliance Document](https://docs.gaia-x.eu/policy-rules-committee/compliance-document/24.11/criteria_cloud_services/#P5.1.4)

---

Not implemented.

---

#### Criterion P5.1.5

> For Label Level 3, Shareholders in the Provider, whose registered head office, headquarters and main establishment are
> not established in a Member State of the EU/EEA shall not, directly or indirectly, individually or jointly, hold
> control
> of the CSP. Control is defined as the ability of a natural or legal person to exercise decisive influence directly or
> indirectly on the CSP through one or more intermediate entities, de jure or de facto. (cf. Council Regulation No
> 139/2004 and Commission Consolidated Jurisdictional Notice under Council Regulation (EC) No 139/2004 for illustrations
> of decisive control).

| Standard Compliance | 	Label L1 | 	Label L2 | 	Label L3          |
|---------------------|-----------|-----------|--------------------|
| N/A                 | 	N/A      | 	N/A      | 	certification     |
| N/A                 | 	N/A      | 	N/A      | 	`not implemented` |

[View in Compliance Document](https://docs.gaia-x.eu/policy-rules-committee/compliance-document/24.11/criteria_cloud_services/#P5.1.5)

---

Not implemented.

---

#### Criterion P5.1.6

> For Label Level 3, in the event of recourse by the Provider, in the context of the services provided to the Customer,
> to the services of a third-party company - including a subcontractor - whose registered head office, headquarters and
> main establishment is outside of the European Union or who is owned or controlled directly or indirectly by another
> third-party company registered outside the EU/EEA, the third-party company shall have no access over the Customer Data
> nor access and identity management for the services provided to the Customer. The Provider, including any of its
> sub-processors, shall push back any request received from non-European authorities to obtain communication of Customer
> Data relating to European Customers, except if request is made in execution of a court judgment or order that is valid
> and compliant under Union law and applicable Member States law as provided by Article 48 GDPR.

| Standard Compliance | 	Label L1 | 	Label L2 | 	Label L3          |
|---------------------|-----------|-----------|--------------------|
| N/A                 | 	N/A      | 	N/A      | 	certification     |
| N/A                 | 	N/A      | 	N/A      | 	`not implemented` |

[View in Compliance Document](https://docs.gaia-x.eu/policy-rules-committee/compliance-document/24.11/criteria_cloud_services/#P5.1.6)

---

Not implemented.

---

#### Criterion P5.1.7

> For Label Level 3, the Provider must maintain continuous operating autonomy for all or part of the services it
> provides. The concept of operating autonomy shall be understood as the ability to maintain the provision of the cloud
> computing service by drawing on the provider’s own skills or by using adequate alternatives

| Standard Compliance | 	Label L1 | 	Label L2 | 	Label L3          |
|---------------------|-----------|-----------|--------------------|
| N/A                 | 	N/A      | 	N/A      | 	certification     |
| N/A                 | 	N/A      | 	N/A      | 	`not implemented` |

[View in Compliance Document](https://docs.gaia-x.eu/policy-rules-committee/compliance-document/24.11/criteria_cloud_services/#P5.1.7)

---

Not implemented.

---

### Access to Customer Data

#### Criterion P5.2.1

> The Provider shall not access Customer Data unless authorized by the Customer or when the access is in accordance with
> applicable laws in scope of the legally binding act.

| Standard Compliance | 	Label L1        | 	Label L2          | 	Label L3          |
|---------------------|------------------|--------------------|--------------------|
| declaration         | 	declaration     | 	certification     | 	certification     |
| **implemented**     | 	**implemented** | 	`not implemented` | 	`not implemented` |

[View in Compliance Document](https://docs.gaia-x.eu/policy-rules-committee/compliance-document/24.11/criteria_cloud_services/#P5.2.1)

---

Verify that at least one `CustomerDataAccessTerms` is present in the `ServiceOffering`'s `legalDocuments`.

Implemented
by [ServiceOfferingHasCustomerDataAccessTermsFilter](../src/vp-validation/filter/service-offering-has-customer-data-access-terms.filter.ts)

---

## Sustainability

### Criterion P6.1.1

> The Provider shall provide transparency on the environmental impact of the Service Offering provided

| Standard Compliance | 	Label L1          | 	Label L2          | 	Label L3          |
|---------------------|--------------------|--------------------|--------------------|
| declaration         | 	declaration       | 	declaration       | 	declaration       |
| `not implemented`   | 	`not implemented` | 	`not implemented` | 	`not implemented` |

[View in Compliance Document](https://docs.gaia-x.eu/policy-rules-committee/compliance-document/24.11/criteria_cloud_services/#P6.1.1)

---

Verify that at least one `EnvironmentalImpactReport` is present in the `ServiceOffering`'s `legalDocuments`.

Not implemented.

---

### Criterion P6.1.2

> The Provider shall ensure that the Service Offering meets or relies on an infrastructure Services Offering which meets
> a high standard in energy efficiency, meeting an annual target of PUE of 1.3 in cool climates and 1.4 in warm climates

| Standard Compliance | 	Label L1          | 	Label L2          | 	Label L3          |
|---------------------|--------------------|--------------------|--------------------|
| N/A                 | 	declaration       | 	certification     | 	certification     |
| N/A                 | 	`not implemented` | 	`not implemented` | 	`not implemented` |

[View in Compliance Document](https://docs.gaia-x.eu/policy-rules-committee/compliance-document/24.11/criteria_cloud_services/#P6.1.2)

---

Not implemented.

---

### Criterion P6.1.3

> The Provider shall ensure that the Service Offering meets or relies on an infrastructure Services Offering for which
> electricity demand will be matched by 75% renewable energy or hourly carbon-free energy by 31st December 2025, and
> 100%
> by 31st December 2030.

| Standard Compliance | 	Label L1          | 	Label L2          | 	Label L3          |
|---------------------|--------------------|--------------------|--------------------|
| N/A                 | 	declaration       | 	certification     | 	certification     |
| N/A                 | 	`not implemented` | 	`not implemented` | 	`not implemented` |

[View in Compliance Document](https://docs.gaia-x.eu/policy-rules-committee/compliance-document/24.11/criteria_cloud_services/#P6.1.3)

---

Not implemented.

---

### Criterion P6.1.4

> The Provider shall ensure that the Service Offering meets or relies on an infrastructure Services Offering that will
> meet a high standard for water conservation demonstrated through the application of a location and source sensitive
> water usage effectiveness (WUE)target of 0.4 L/kWh in areas with water stress.

| Standard Compliance | 	Label L1          | 	Label L2          | 	Label L3          |
|---------------------|--------------------|--------------------|--------------------|
| N/A                 | 	declaration       | 	certification     | 	certification     |
| N/A                 | 	`not implemented` | 	`not implemented` | 	`not implemented` |

[View in Compliance Document](https://docs.gaia-x.eu/policy-rules-committee/compliance-document/24.11/criteria_cloud_services/#P6.1.4)

---

Not implemented.

---